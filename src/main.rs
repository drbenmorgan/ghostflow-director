// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The Kitware robot
//!
//! This program watches a directory for JSON jobs in order to manage the Kitware workflow for
//! repositories.

#![warn(missing_docs)]

#[macro_use]
extern crate clap;

#[macro_use]
extern crate graphql_client;

#[cfg(test)]
#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;

extern crate serde_derive;

#[macro_use]
extern crate serde_json;

mod crates {
    pub extern crate boxfnonce;
    pub extern crate chrono;
    pub extern crate clap;
    pub extern crate either;
    pub extern crate env_logger;
    pub extern crate erased_serde;
    pub extern crate failure;
    pub extern crate ghostflow;
    pub extern crate ghostflow_github;
    pub extern crate ghostflow_gitlab;
    pub extern crate git_checks;
    pub extern crate git_checks_config;
    pub extern crate git_checks_core;
    pub extern crate git_topic_stage;
    pub extern crate git_workarea;
    pub extern crate graphql_client;
    pub extern crate inventory;
    pub extern crate itertools;
    pub extern crate json_job_dispatch;
    pub extern crate lazy_init;
    // pub extern crate lazy_static;
    pub extern crate log;
    pub extern crate rayon;
    pub extern crate regex;
    pub extern crate serde;
    pub extern crate serde_json;
    pub extern crate serde_yaml;
    pub extern crate thiserror;
    pub extern crate topological_sort;
    pub extern crate ttl_cache;
    pub extern crate yaml_merge_keys;

    #[cfg(feature = "systemd")]
    pub extern crate systemd;

    #[cfg(test)]
    pub extern crate futures;
    #[cfg(test)]
    pub extern crate serial_test;
    #[cfg(test)]
    pub extern crate tempdir;
    #[cfg(test)]
    pub extern crate tokio;
    #[cfg(test)]
    pub extern crate tokio_uds;
}

use std::error::Error;
use std::fmt;
use std::io::{self, Write};
use std::num::ParseIntError;
use std::path::{Path, PathBuf};

use crates::clap::{App, Arg};
use crates::env_logger;
use crates::json_job_dispatch::{
    self, Director, DirectorError, DirectorWatchdog, Handler, RunResult,
};
use crates::log::LevelFilter;
#[cfg(feature = "systemd")]
use crates::log::SetLoggerError;
use crates::rayon::ThreadPoolBuilder;
#[cfg(feature = "systemd")]
use crates::systemd::journal::JournalLog;
use crates::thiserror::Error;

#[macro_use]
pub mod config;
use config::{Config, ConfigRead};

mod ghostflow_ext;

pub mod actions;
pub mod handlers;

#[cfg(test)]
mod tests;

// Include git information from `build.rs`.
include!(concat!(env!("OUT_DIR"), "/git_info.rs"));

#[derive(Debug, Error)]
enum RunError {
    #[error("config error: {}", source)]
    Config {
        #[from]
        source: config::ConfigError,
    },
    #[error("failed to create director in {}: {}", path.display(), source)]
    CreateDirector {
        path: PathBuf,
        #[source]
        source: json_job_dispatch::DirectorError,
    },
    #[error("failed to add watchdog to the director: {}", source)]
    AddWatchdog {
        #[source]
        source: Box<dyn Error + Send + Sync>,
    },
    #[error("failed to add handler to the director: {}", source)]
    AddHandler {
        #[source]
        source: Box<dyn Error + Send + Sync>,
    },
    #[error("failed to run the director in {}: {}", path.display(), source)]
    RunDirector {
        path: PathBuf,
        #[source]
        source: DirectorError,
    },
}

impl RunError {
    fn create_director(path: PathBuf, source: json_job_dispatch::DirectorError) -> Self {
        RunError::CreateDirector {
            path,
            source,
        }
    }

    fn add_watchdog(source: Box<dyn Error + Send + Sync>) -> Self {
        RunError::AddWatchdog {
            source,
        }
    }

    fn add_handler(source: Box<dyn Error + Send + Sync>) -> Self {
        RunError::AddHandler {
            source,
        }
    }

    fn run_director(path: PathBuf, source: DirectorError) -> Self {
        RunError::RunDirector {
            path,
            source,
        }
    }
}

/// Run the director with the configuration file's path.
fn run_director(config_path: &Path) -> Result<RunResult, RunError> {
    let watchdog = DirectorWatchdog {};

    let config = Config::from_path(&config_path)?;

    let mut director = Director::new(&config.archive_dir)
        .map_err(|err| RunError::create_director(config.archive_dir.clone(), err))?;

    watchdog
        .add_to_director(&mut director)
        .map_err(RunError::add_watchdog)?;
    for handler in &config.handlers {
        handler
            .add_to_director(&mut director)
            .map_err(RunError::add_handler)?;
    }

    director
        .watch_directory(&config.queue_dir)
        .map_err(|err| RunError::run_director(config.queue_dir, err))
}

fn version_string() -> String {
    format!(
        "{}{}",
        crate_version!(),
        if let Some(hash) = COMMIT_HASH {
            format!(
                " ({}{})",
                hash,
                if let Some(false) = WORKTREE_CLEAN {
                    " worktree dirty"
                } else {
                    ""
                }
            )
        } else {
            String::new()
        },
    )
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Job {
    Exit,
    Restart,
    ResetFailedProjects,
}

impl fmt::Display for Job {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let desc = match self {
            Job::Exit => "exit",
            Job::Restart => "restart",
            Job::ResetFailedProjects => "reset failed projects",
        };

        write!(f, "{}", desc)
    }
}

#[derive(Debug, Error)]
enum SetupError {
    #[cfg(not(feature = "systemd"))]
    #[error("unavailable logger: {}", name)]
    UnavailableLogger { name: &'static str },
    #[cfg(feature = "systemd")]
    #[error("failed to initialize the journal: {}", source)]
    InitializeJournal {
        #[from]
        source: SetLoggerError,
    },
    #[error("unrecognized logger: {}", name)]
    UnrecognizedLogger { name: String },
    #[error("non-integer thread count {}: {}", count, source)]
    NonIntegerThreadCount {
        count: String,
        #[source]
        source: ParseIntError,
    },
    #[error("failed to initialize the global rayon thread pool: {}", source)]
    RayonThreadPoolInit {
        #[from]
        source: rayon::ThreadPoolBuildError,
    },
    #[error("failed to handle the {} job: {}", job, source)]
    JobError {
        job: Job,
        #[source]
        source: json_job_dispatch::utils::JobError,
    },
    #[error("failed to archive: {}", source)]
    Archive {
        #[from]
        source: json_job_dispatch::utils::ArchiveQueueError,
    },
}

impl SetupError {
    #[cfg(not(feature = "systemd"))]
    fn unavailable_logger(name: &'static str) -> Self {
        SetupError::UnavailableLogger {
            name,
        }
    }

    fn unrecognized_logger(name: String) -> Self {
        SetupError::UnrecognizedLogger {
            name,
        }
    }

    fn non_integer_thread_count(count: String, source: ParseIntError) -> Self {
        SetupError::NonIntegerThreadCount {
            count,
            source,
        }
    }

    fn job_error(job: Job, source: json_job_dispatch::utils::JobError) -> Self {
        SetupError::JobError {
            job,
            source,
        }
    }
}

enum Logger {
    #[cfg(feature = "systemd")]
    Systemd,
    Env,
}

/// A `main` function which supports `try!`.
fn try_main() -> Result<(), Box<dyn Error>> {
    let version = version_string();
    let matches = App::new("ghostflow-director")
        .version(version.as_str())
        .author("Ben Boeckel <ben.boeckel@kitware.com>")
        .about("Watch a directory for project events and perform workflow actions on them")
        .arg(
            Arg::with_name("CONFIG")
                .short("c")
                .long("config")
                .help("Path to the configuration file")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("DEBUG")
                .short("d")
                .long("debug")
                .help("Increase verbosity")
                .multiple(true),
        )
        .arg(
            Arg::with_name("LOGGER")
                .short("l")
                .long("logger")
                .default_value("env")
                .possible_values(&[
                    "env",
                    #[cfg(feature = "systemd")]
                    "systemd",
                ])
                .help("Logging backend")
                .value_name("LOGGER")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("VERIFY")
                .short("v")
                .long("verify")
                .help("Check the configuration file and exit"),
        )
        .arg(
            Arg::with_name("DUMP_CONFIG")
                .long("dump-config")
                .help("Read the configuration file, dump it as JSON, and exit"),
        )
        .arg(
            Arg::with_name("RESET_FAILED_PROJECTS")
                .long("reset-failed-projects")
                .help("Reset failed projects so that they are attempted again")
                .value_name("HOST")
                .takes_value(true)
                .number_of_values(1)
                .multiple(true),
        )
        .arg(
            Arg::with_name("THREAD_COUNT")
                .short("j")
                .long("threads")
                .help("Number of threads to use in the Rayon thread pool")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("RELOAD")
                .long("reload")
                .help("Reload the configuration"),
        )
        .arg(
            Arg::with_name("EXIT")
                .long("exit")
                .help("Cause the director to exit"),
        )
        .arg(
            Arg::with_name("ARCHIVE")
                .long("archive")
                .help("Archive jobs into tarballs in an archive directory")
                .takes_value(true),
        )
        .get_matches();

    let log_level = match matches.occurrences_of("DEBUG") {
        0 => LevelFilter::Error,
        1 => LevelFilter::Warn,
        2 => LevelFilter::Info,
        3 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let _logger = match matches
        .value_of("LOGGER")
        .expect("logger should have a value")
    {
        "env" => {
            env_logger::Builder::new().filter(None, log_level).init();
            Logger::Env
        },

        #[cfg(feature = "systemd")]
        "systemd" => {
            JournalLog::init()?;
            Logger::Systemd
        },
        #[cfg(not(feature = "systemd"))]
        "systemd" => {
            return Err(SetupError::unavailable_logger("systemd").into());
        },

        logger => {
            return Err(SetupError::unrecognized_logger(logger.into()).into());
        },
    };

    log::set_max_level(log_level);

    if let Some(count) = matches.value_of("THREAD_COUNT") {
        let count = count
            .parse()
            .map_err(|err| SetupError::non_integer_thread_count(count.into(), err))?;

        ThreadPoolBuilder::new().num_threads(count).build_global()?;
    }

    let config_path = Path::new(
        matches
            .value_of("CONFIG")
            .expect("the configuration option is required"),
    );

    if matches.is_present("VERIFY") {
        let config = ConfigRead::from_path(&config_path)?;
        config.verify_all_check_configs()?;
    } else if matches.is_present("DUMP_CONFIG") {
        let config = ConfigRead::from_path(&config_path)?;
        let stdout = io::stdout();
        let mut out = stdout.lock();
        serde_json::to_writer_pretty(&mut out, &config)?;
        out.write_all(b"\n")?;
    } else if matches.is_present("EXIT") {
        let config = ConfigRead::from_path(&config_path)?;
        json_job_dispatch::utils::exit(config.queue_dir())
            .map_err(|err| SetupError::job_error(Job::Exit, err))?;
    } else if matches.is_present("RELOAD") {
        let config = ConfigRead::from_path(&config_path)?;
        json_job_dispatch::utils::restart(config.queue_dir())
            .map_err(|err| SetupError::job_error(Job::Restart, err))?;
    } else if let Some(hosts) = matches.values_of("RESET_FAILED_PROJECTS") {
        let config = ConfigRead::from_path(&config_path)?;
        let queue_dir = config.queue_dir();
        hosts
            .map(|host| {
                let kind = format!("{}:reset_failed_projects", host);
                json_job_dispatch::utils::drop_job(queue_dir, kind, json!({}))
                    .map_err(|err| SetupError::job_error(Job::ResetFailedProjects, err))
            })
            .collect::<Result<Vec<_>, _>>()?;
    } else if matches.is_present("ARCHIVE") {
        let config = ConfigRead::from_path(&config_path)?;
        let archive_path = matches
            .value_of("ARCHIVE")
            .expect("the archive option requires a value");
        json_job_dispatch::utils::archive_queue(config.archive_dir(), archive_path)?;
    } else {
        while let RunResult::Restart = run_director(config_path)? {}
    }

    Ok(())
}

/// The entry point.
///
/// Wraps around `try_main` to panic on errors.
fn main() {
    if let Err(err) = try_main() {
        panic!("{:?}", err);
    }
}
