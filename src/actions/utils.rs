// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::serde_json::Value;

/// Create a test job.
pub fn test_job<K>(kind: K, data: &Value) -> Value
where
    K: AsRef<str>,
{
    json!({
        "kind": kind.as_ref(),
        "data": data,
    })
}
