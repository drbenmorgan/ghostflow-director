// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use crates::clap::Arg;
use crates::git_workarea::CommitId;

use actions::merge_requests::utils;
use actions::merge_requests::{Action, ActionError, Data, Effect};

/// The reformat action
///
/// Rewrites a merge request topic's commits to conform to code formatting guidelines. The topology
/// of the merge request topic is unchanged.
#[derive(Debug, Default, Clone, Copy)]
pub struct Reformat;

impl Action for Reformat {
    fn help(&self, data: &Data) -> Option<Cow<'static, str>> {
        let _ = data.main_branch.reformat()?;

        let msg =
            "Rewrites the topic for the merge request according to the project coding guidelines. \
             By default, every commit will have its content changed so that it follows the coding \
             guidelines. With the `--whole-tree` option, all files in the repository as of the \
             last commit in the topic will be rewritten to follow the coding guidelines. Any \
             commit which is empty after the reformatting will be dropped from the topic. After \
             performing a reformat, the resulting topic is force-pushed as the source topic of \
             the merge request. In order to use the reformatted branch, it must be fetched and \
             updated locally, otherwise the reformatting will need to be redone.";

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let action = if let Some(action) = data.main_branch.reformat() {
            action
        } else {
            effects.push(ActionError::not_supported().into());
            return effects;
        };
        if !data.action_data.cause.is_submitter && data.action_data.cause.how < action.access_level
        {
            effects.push(ActionError::insufficient_permissions(action.access_level).into());
            return effects;
        }

        let reformat = action.reformat();

        let matches = utils::command_app("reformat")
            .arg(
                Arg::with_name("WHOLE_TREE")
                    .long("whole-tree")
                    .takes_value(false),
            )
            .get_matches_from_safe(arguments);
        let matches = match matches {
            Ok(matches) => matches,
            Err(err) => {
                let msg = err.message.lines().next().unwrap_or("<unknown error>");
                effects.push(ActionError::unrecognized_arguments(msg).into());
                return effects;
            },
        };

        let mr = &data.info.merge_request;

        let res = if matches.is_present("WHOLE_TREE") {
            reformat.reformat_repo(mr)
        } else {
            reformat.reformat_mr(&CommitId::new(&data.main_branch.name), mr)
        };

        if let Err(err) = res {
            error!(
                target: "ghostflow-director/handler",
                "failed during the reformat action on {}: {:?}",
                mr.url,
                err,
            );

            let msg = format!(
                "Error occurred during reformat action (@{}): {}",
                data.action_data.project.maintainers().join(" @"),
                err,
            );
            effects.push(Effect::error(msg));
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use std::env;
    use std::fs::File;
    use std::thread;

    use crates::git_workarea::GitContext;

    use handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";
    const REFORMAT_OK_TOPIC: &str = "5df8609482f91d9aea2f665877efa6a343ca4480";
    const REFORMAT_UPDATED_TOPIC: &str = "ffbe4260803165707759c7e3625d0ca3113c8104";
    const REFORMAT_UPDATED_TOPIC_AFTER_COMMITS: &str = "24efcd08dded7b698b42bc9b304ec29d469c40f5";
    const REFORMAT_UPDATED_TOPIC_AFTER_REPO: &str = "31a46f20249546d99032a7aaea737b2a368dde63";
    const REFORMAT_UPDATED_WITH_ARG_TOPIC: &str = "34ec0f5f3b3dda108bf3f4fb3603fd5fd7ddcd06";
    const REFORMAT_UPDATED_WITH_ARG_TOPIC_AFTER_COMMITS: &str =
        "0c2b2342ae4fe1f56b2a99b3a1a7648125c5991e";
    const REFORMAT_UPDATED_WITH_ARG_TOPIC_AFTER_REPO: &str =
        "432fd1cfacbe9cdfa3a213018e01462a9f55fafa";

    lazy_static! {
        static ref REFORMAT_TIMEOUT: usize = {
            env::var("GHOSTFLOW_TEST_REFORMAT_TIMEOUT")
                .map_err(|err| {
                    if let env::VarError::NotUnicode(val) = err {
                        error!("non-unicode GHOSTFLOW_TEST_REFORMAT_TIMEOUT given; ignoring: {:?}", val);
                    }
                })
                // Ignore environment variable errors.
                .ok()
                .and_then(|val| {
                    val.parse()
                        .map_err(|err| {
                            error!("failed to parse GHOSTFLOW_TEST_REFORMAT_TIMEOUT; ignoring: {}", err);
                        })
                        .ok()
                })
                .unwrap_or(1)
        };
    }

    fn reformat_config() -> serde_json::Value {
        json!({
            "required_access_level": "developer",
            "formatters": [
                {
                    "kind": "simple",
                    "formatter": concat!(env!("CARGO_MANIFEST_DIR"), "/test/format.simple"),
                    "config_files": [
                        "format-config",
                    ],
                    "timeout": *REFORMAT_TIMEOUT,
                },
                {
                    "kind": "with_arg",
                    "formatter": concat!(env!("CARGO_MANIFEST_DIR"), "/test/format.with_arg"),
                    "timeout": *REFORMAT_TIMEOUT,
                },
            ],
        })
    }

    fn check_reformat(ctx: &GitContext, topic: &str, commit: &str) {
        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg(format!("refs/heads/{}", topic))
            .output()
            .unwrap();
        assert!(rev_parse.status.success());
        let rev = String::from_utf8_lossy(&rev_parse.stdout);

        assert_eq!(rev.trim_end(), commit);
    }

    #[test]
    fn test_command_reformat_refuse_not_configured() {
        let mut service = TestService::new(
            "test_command_reformat_refuse_not_configured",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: reformat"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `reformat` command: the command is not supported"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_reformat_disallowed() {
        let mut service = TestService::new(
            "test_command_reformat_disallowed",
            vec![
                Action::create_user("fork"),
                Action::create_user("unrelated"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("unrelated", "Do: reformat"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `reformat` command: permission denied: insufficient \
                   access level (Developer)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_reformat_submitter_allowed() {
        let mut service = TestService::new(
            "test_command_reformat_submitter_allowed",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("fork", "Do: reformat"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "This topic is clean and required no reformatting.".into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_reformat_bad_arguments() {
        let mut service = TestService::new(
            "test_command_reformat_bad_arguments",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: reformat --unrecognized"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `reformat` command: unrecognized arguments: error: Found \
                   argument '--unrecognized' which wasn't expected, or isn't valid in this context"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_reformat_ok() {
        let mut service = TestService::new(
            "test_command_reformat_ok",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", REFORMAT_OK_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: reformat"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "This topic is clean and required no reformatting.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/fork/example.git");
        let ctx = GitContext::new(project_dir);

        check_reformat(&ctx, "mr-source", REFORMAT_OK_TOPIC);
    }

    #[test]
    fn test_command_reformat_updated() {
        let mut service = TestService::new(
            "test_command_reformat_updated",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", REFORMAT_UPDATED_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: reformat"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This topic has been reformatted and pushed; please fetch from the source \
                 repository and reset your local branch to continue with further development on \
                 the reformatted commits."
                .into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/fork/example.git");
        let ctx = GitContext::new(project_dir);

        check_reformat(&ctx, "mr-source", REFORMAT_UPDATED_TOPIC_AFTER_COMMITS);
    }

    #[test]
    fn test_command_reformat_updated_with_arg() {
        let mut service = TestService::new(
            "test_command_reformat_updated_with_arg",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push(
                    "fork",
                    "example",
                    "mr-source",
                    REFORMAT_UPDATED_WITH_ARG_TOPIC,
                ),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: reformat"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This topic has been reformatted and pushed; please fetch from the source \
                 repository and reset your local branch to continue with further development on \
                 the reformatted commits."
                .into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/fork/example.git");
        let ctx = GitContext::new(project_dir);

        check_reformat(
            &ctx,
            "mr-source",
            REFORMAT_UPDATED_WITH_ARG_TOPIC_AFTER_COMMITS,
        );
    }

    #[test]
    fn test_command_reformat_push_fail() {
        let mut service = TestService::new(
            "test_command_reformat_push_fail",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source-stage", REFORMAT_OK_TOPIC),
                Action::push("fork", "example", "mr-source", REFORMAT_UPDATED_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::delay_director("update-mr-source", 500),
                Action::mr_comment("ghostflow", "Do: reformat"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "Failed to push the reformatted branch.".into(),
        };

        let project_subdir = "projects/ghostflow/example.git";
        let fork_subdir = "projects/fork/example.git";
        let fork_dir = service.root().join(fork_subdir);
        let refpath = "refs/mr/0/heads/0";
        let clone_indicator = service
            .root()
            .join("director")
            .join(project_subdir)
            .join(refpath);
        let delay_path = service.root().join("delay").join("update-mr-source");
        let ctx = GitContext::new(fork_dir);

        let update_ref_thread = thread::spawn(move || {
            // Wait for the project to be cloned.
            loop {
                if clone_indicator.exists() {
                    break;
                }
            }
            info!("updating mr head ref");

            // Emulate a push that has happened to the user that has not made a notification yet.
            let update_ref = ctx
                .git()
                .arg("update-ref")
                .arg("refs/heads/mr-source")
                .arg(REFORMAT_OK_TOPIC)
                .output()
                .map_err(|_| "failed to create update-ref command")?;
            if !update_ref.status.success() {
                return Err(format!(
                    "failed to update master to point to next: {}",
                    String::from_utf8_lossy(&update_ref.stderr),
                ));
            }

            info!("making the delay signal");
            File::create(delay_path).map_err(|_| "failed to create the delay file")?;

            Ok(())
        });

        service.launch(config, end).unwrap();
        update_ref_thread.join().unwrap().unwrap();

        let project_dir = service.root().join("projects/fork/example.git");
        let ctx = GitContext::new(project_dir);

        check_reformat(&ctx, "mr-source", REFORMAT_OK_TOPIC);
    }

    #[test]
    fn test_command_reformat_repo_ok() {
        let mut service = TestService::new(
            "test_command_reformat_repo_ok",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", REFORMAT_OK_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: reformat --whole-tree"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "This topic is clean and required no reformatting.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/fork/example.git");
        let ctx = GitContext::new(project_dir);

        check_reformat(&ctx, "mr-source", REFORMAT_OK_TOPIC);
    }

    #[test]
    fn test_command_reformat_repo_updated() {
        let mut service = TestService::new(
            "test_command_reformat_repo_updated",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", REFORMAT_UPDATED_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: reformat --whole-tree"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This topic has been reformatted and pushed; please fetch from the source \
                 repository and reset your local branch to continue with further development on \
                 the reformatted commits."
                .into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/fork/example.git");
        let ctx = GitContext::new(project_dir);

        check_reformat(&ctx, "mr-source", REFORMAT_UPDATED_TOPIC_AFTER_REPO);
    }

    #[test]
    fn test_command_reformat_repo_updated_with_arg() {
        let mut service = TestService::new(
            "test_command_reformat_repo_updated_with_arg",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push(
                    "fork",
                    "example",
                    "mr-source",
                    REFORMAT_UPDATED_WITH_ARG_TOPIC,
                ),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: reformat --whole-tree"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 This topic has been reformatted and pushed; please fetch from the source \
                 repository and reset your local branch to continue with further development on \
                 the reformatted commits."
                .into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/fork/example.git");
        let ctx = GitContext::new(project_dir);

        check_reformat(
            &ctx,
            "mr-source",
            REFORMAT_UPDATED_WITH_ARG_TOPIC_AFTER_REPO,
        );
    }

    #[test]
    fn test_command_reformat_repo_push_fail() {
        let mut service = TestService::new(
            "test_command_reformat_repo_push_fail",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source-stage", REFORMAT_OK_TOPIC),
                Action::push("fork", "example", "mr-source", REFORMAT_UPDATED_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::delay_director("update-mr-source", 500),
                Action::mr_comment("ghostflow", "Do: reformat --whole-tree"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": reformat_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "Failed to push the reformatted branch.".into(),
        };

        let project_subdir = "projects/ghostflow/example.git";
        let fork_subdir = "projects/fork/example.git";
        let fork_dir = service.root().join(fork_subdir);
        let refpath = "refs/mr/0/heads/0";
        let clone_indicator = service
            .root()
            .join("director")
            .join(project_subdir)
            .join(refpath);
        let delay_path = service.root().join("delay").join("update-mr-source");
        let ctx = GitContext::new(fork_dir);

        let update_ref_thread = thread::spawn(move || {
            // Wait for the project to be cloned.
            loop {
                if clone_indicator.exists() {
                    break;
                }
            }

            // Emulate a push that has happened to the user that has not made a notification yet.
            let update_ref = ctx
                .git()
                .arg("update-ref")
                .arg("refs/heads/mr-source")
                .arg(REFORMAT_OK_TOPIC)
                .output()
                .map_err(|_| "failed to create update-ref command")?;
            if !update_ref.status.success() {
                return Err(format!(
                    "failed to update master to point to next: {}",
                    String::from_utf8_lossy(&update_ref.stderr),
                ));
            }

            File::create(delay_path).map_err(|_| "failed to create the delay file")?;

            Ok(())
        });

        service.launch(config, end).unwrap();
        update_ref_thread.join().unwrap().unwrap();

        let project_dir = service.root().join("projects/fork/example.git");
        let ctx = GitContext::new(project_dir);

        check_reformat(&ctx, "mr-source", REFORMAT_OK_TOPIC);
    }
}
