// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use actions::merge_requests::utils;
use actions::merge_requests::{Action, ActionError, Data, Effect};

/// The check action
///
/// An action to check a merge request for consistency against the project's configured set of
/// content checks for the branch.
#[derive(Debug, Default, Clone, Copy)]
pub struct Check;

impl Action for Check {
    fn help(&self, _: &Data) -> Option<Cow<'static, str>> {
        let msg =
            "Performs checks on the content and structure of the merge request according to the \
             project's settings. Generally, all messages should be addressed. Those which mention \
             a specific commit require that the commit itself be fixed. That is, an additional \
             commit which fixes the issue will not address the problem with the original commit.";

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let matches = utils::command_app("check").get_matches_from_safe(arguments);
        let _ = match matches {
            Ok(matches) => matches,
            Err(err) => {
                let msg = err.message.lines().next().unwrap_or("<unknown error>");
                effects.push(ActionError::unrecognized_arguments(msg).into());
                return effects;
            },
        };

        match utils::check_mr(data.action_data.project, data.main_branch, &data.info) {
            Ok(utils::CheckResult::Pass) => (),
            Ok(utils::CheckResult::Fail) => {
                effects.push(Effect::stop_processing());
            },
            Err(err) => {
                error!(
                    target: "ghostflow-director/handler",
                    "failed to run checks on {}: {:?}",
                    data.info.merge_request.url,
                    err,
                );

                let msg = format!("failed to run checks: `{}`", err);
                effects.push(Effect::error(msg));
            },
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use crates::git_workarea::CommitId;
    use crates::serde_json::Value;

    use handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    fn test_check_config(project_config: &Value) -> Value {
        json!({
            "ghostflow/example": project_config,
        })
    }

    fn warning_check(path: &str) -> Value {
        json!({
            "kind": "changelog/topic",
            "config": {
                "style": "file",
                "path": path,
                "required": false,
            },
        })
    }

    fn failing_check() -> Value {
        json!({
            "kind": "bad_commits",
            "config": {
                "bad_commits": [
                    MERGE_REQUEST_TOPIC,
                ],
            },
        })
    }

    #[test]
    fn test_command_check_is_automatic() {
        let mut service = TestService::new(
            "test_command_check_is_automatic",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "branches": {
                "master": {},
            },
        }));
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "ghostflow-check-master".into(),
            state: CommitStatusState::Success,
            description: None,
            target_url: None,
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_project_pre_checks() {
        let mut service = TestService::new(
            "test_command_check_project_pre_checks",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "pre_checks": [
                failing_check(),
            ],
            "checks": {
                "project_changelog": warning_check("CHANGELOG.project.md"),
            },
            "branches": {
                "master": {},
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - commit 6f781d4d4d85dfd5a609532a26bcc6fd63fcef51 is a known-bad commit that was \
                   removed from the server.\n\n\
                 Alerts:\n\n  \
                 - commit 6f781d4d4d85dfd5a609532a26bcc6fd63fcef51 was pushed to the server.\n\n\
                 Please rewrite commits to fix the errors listed above (adding fixup commits will \
                 not resolve the errors) and force-push the branch again to update the merge \
                 request.\n\n\
                 Alert: @ghostflow."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_project_pre_checks_allowed() {
        let mut service = TestService::new(
            "test_command_check_project_pre_checks_allowed",
            vec![
                Action::create_user("allowed"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "allowed"),
                Action::push("allowed", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "allowed", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "pre_checks": [
                failing_check(),
                {
                    "kind": "allow_robot",
                    "config": {
                        "name": "allowed",
                        "email": "allowed@example.org",
                    },
                },
            ],
            "checks": {
                "project_changelog": warning_check("CHANGELOG.project.md"),
            },
            "branches": {
                "master": {},
            },
        }));
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "ghostflow-check-master".into(),
            state: CommitStatusState::Success,
            description: None,
            target_url: None,
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_branch_pre_checks() {
        let mut service = TestService::new(
            "test_command_check_branch_pre_checks",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "branches": {
                "master": {
                    "pre_checks": [
                        failing_check(),
                    ],
                    "checks": {
                        "branch_changelog": warning_check("CHANGELOG.branch.md"),
                    },
                },
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - commit 6f781d4d4d85dfd5a609532a26bcc6fd63fcef51 is a known-bad commit that was \
                   removed from the server.\n\n\
                 Alerts:\n\n  \
                 - commit 6f781d4d4d85dfd5a609532a26bcc6fd63fcef51 was pushed to the server.\n\n\
                 Please rewrite commits to fix the errors listed above (adding fixup commits will \
                 not resolve the errors) and force-push the branch again to update the merge \
                 request.\n\n\
                 Alert: @ghostflow."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_both_pre_checks() {
        let mut service = TestService::new(
            "test_command_check_both_pre_checks",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "pre_checks": [
                warning_check("CHANGELOG.project.md"),
            ],
            "branches": {
                "master": {
                    "pre_checks": [
                        failing_check(),
                    ],
                    "checks": {
                        "branch_changelog": warning_check("CHANGELOG.branch.md"),
                    },
                },
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - commit 6f781d4d4d85dfd5a609532a26bcc6fd63fcef51 is a known-bad commit that was \
                   removed from the server.\n\n\
                 Warnings:\n\n  \
                 - please consider adding a changelog entry in the `CHANGELOG.project.md` \
                   file.\n\n\
                 Alerts:\n\n  \
                 - commit 6f781d4d4d85dfd5a609532a26bcc6fd63fcef51 was pushed to the server.\n\n\
                 The warnings do not need to be fixed, but it is recommended to do so.\n\n\
                 Please rewrite commits to fix the errors listed above (adding fixup commits will \
                 not resolve the errors) and force-push the branch again to update the merge \
                 request.\n\n\
                 Alert: @ghostflow."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_project_checks() {
        let mut service = TestService::new(
            "test_command_check_project_checks",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "checks": {
                "project_changelog": warning_check("CHANGELOG.project.md"),
            },
            "branches": {
                "master": {},
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Warnings:\n\n  \
                 - please consider adding a changelog entry in the `CHANGELOG.project.md` \
                   file.\n\n\
                 The warnings do not need to be fixed, but it is recommended to do so."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_branch_checks() {
        let mut service = TestService::new(
            "test_command_check_branch_checks",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "branches": {
                "master": {
                    "checks": {
                        "branch_changelog": warning_check("CHANGELOG.branch.md"),
                    },
                },
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Warnings:\n\n  \
                 - please consider adding a changelog entry in the `CHANGELOG.branch.md` \
                   file.\n\n\
                 The warnings do not need to be fixed, but it is recommended to do so."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_both_checks() {
        let mut service = TestService::new(
            "test_command_check_both_checks",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "checks": {
                "project_changelog": warning_check("CHANGELOG.project.md"),
            },
            "branches": {
                "master": {
                    "checks": {
                        "branch_changelog": warning_check("CHANGELOG.branch.md"),
                    },
                },
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Warnings:\n\n  \
                 - please consider adding a changelog entry in the `CHANGELOG.branch.md` file.\n  \
                 - please consider adding a changelog entry in the `CHANGELOG.project.md` \
                 file.\n\n\
                 The warnings do not need to be fixed, but it is recommended to do so."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_override_checks() {
        let mut service = TestService::new(
            "test_command_check_override_checks",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "checks": {
                "changelog": warning_check("CHANGELOG.project.md"),
            },
            "branches": {
                "master": {
                    "checks": {
                        "changelog": warning_check("CHANGELOG.branch.md"),
                    },
                },
            },
        }));
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Warnings:\n\n  \
                 - please consider adding a changelog entry in the `CHANGELOG.branch.md` file.\n\n\
                 The warnings do not need to be fixed, but it is recommended to do so."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_check_mask_checks() {
        let mut service = TestService::new(
            "test_command_check_mask_checks",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
            ],
        )
        .unwrap();
        let config = test_check_config(&json!({
            "checks": {
                "changelog": warning_check("CHANGELOG.project.md"),
            },
            "branches": {
                "master": {
                    "checks": {
                        "changelog": {
                            "kind": null,
                        },
                    },
                },
            },
        }));
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(MERGE_REQUEST_TOPIC),
            name: "ghostflow-check-master".into(),
            state: CommitStatusState::Success,
            description: None,
            target_url: None,
        };

        service.launch(config, end).unwrap();
    }
}
