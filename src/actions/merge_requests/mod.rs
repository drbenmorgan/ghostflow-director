// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Actions which may be performed on merge requests.

use std::borrow::Cow;

use crates::ghostflow::host::CheckStatus;
use crates::thiserror::Error;

use actions;
use config::Branch;
use ghostflow_ext::AccessLevel;

mod info;
pub use self::info::Backport;
pub use self::info::BackportError;
pub use self::info::Info;

mod utils;

/// An error which may be raised from an action.
pub trait InnerActionError: std::error::Error {}

/// Errors which may occur in the course of a merge request action.
#[derive(Debug, Error)]
pub enum ActionError {
    /// The action is not supported..
    #[error("the command is not supported")]
    NotSupported,
    /// The action arguments did not parse successfully.
    #[error("unrecognized arguments: {}", message)]
    UnrecognizedArguments {
        /// A description of the error message.
        message: String,
    },
    /// The cause of the action lacks permissions to perform the action.
    // TODO: `required` is formatted using `Debug` here.
    #[error("permission denied: insufficient access level ({:?})", required)]
    InsufficientPermissions {
        /// The required access level to perform the action.
        required: AccessLevel,
    },
    /// An error internal to the action occurred.
    #[error("{}", inner)]
    Inner {
        /// The source of the error.
        // TODO: Make this the `source` of the error.
        inner: Box<dyn InnerActionError + 'static>,
    },
}

impl ActionError {
    fn not_supported() -> Self {
        Self::NotSupported
    }

    fn unrecognized_arguments<M>(message: M) -> Self
    where
        M: Into<String>,
    {
        Self::UnrecognizedArguments {
            message: message.into(),
        }
    }

    fn insufficient_permissions(required: AccessLevel) -> Self {
        Self::InsufficientPermissions {
            required,
        }
    }
}

impl<E> From<E> for ActionError
where
    E: InnerActionError + Sized + 'static,
{
    fn from(error: E) -> Self {
        Self::Inner {
            inner: Box::new(error),
        }
    }
}

/// Effects actions may have once done.
#[derive(Debug)]
pub enum Effect {
    /// A message to add to the command sequence's resulting comment.
    Message {
        /// The message to add.
        message: String,
    },
    /// An error message to add to the command sequence's resulting comment.
    Error {
        /// The message to add.
        message: String,
    },
    /// An error to add to the command sequence's resulting comment.
    ActionError {
        /// The error which occurred.
        error: ActionError,
    },
    /// Defer handling of the merge request until the future.
    DeferHandling,
    /// Stop processing of any further commands.
    StopProcessing,
}

impl Effect {
    fn message<M>(message: M) -> Self
    where
        M: Into<String>,
    {
        Effect::Message {
            message: message.into(),
        }
    }

    fn error<E>(error: E) -> Self
    where
        E: Into<String>,
    {
        Effect::Error {
            message: error.into(),
        }
    }

    fn defer_handling() -> Self {
        Effect::DeferHandling
    }

    fn stop_processing() -> Self {
        Effect::StopProcessing
    }
}

impl From<ActionError> for Effect {
    fn from(error: ActionError) -> Self {
        Self::ActionError {
            error,
        }
    }
}

/// Data for actions on merge requests.
pub struct Data<'a> {
    /// Common data to all actions.
    pub action_data: actions::Data<'a>,
    /// The main branch for which the action is being performed.
    pub main_branch: &'a Branch,
    /// The merge request which triggered the action.
    pub info: Info<'a>,
}

impl<'a> Data<'a> {
    /// Check the status of a merge request.
    ///
    /// This is a read-only operation on the MR (rather than triggering a check).
    pub fn check_status(&self) -> (Vec<Effect>, Option<CheckStatus>) {
        let mut effects = Vec::new();

        let project = &self.action_data.project;
        let mr_info = &self.info;
        let status = match mr_info.check_status(
            project.service.clone().as_hosting_service().as_ref(),
            &project.context,
            |name| project.status_name(name),
        ) {
            Ok(status) => Some(status),
            Err(err) => {
                error!(
                    target: "ghostflow-director/handler",
                    "failed to determine the check status of {}: {:?}",
                    mr_info.merge_request.url,
                    err,
                );

                let msg = format!("failed to determine the check status: `{}`", err);
                effects.push(Effect::error(msg));

                None
            },
        };

        (effects, status)
    }
}

/// A trait for representing an action on a merge request.
pub trait Action {
    /// Help text for the action.
    fn help(&self, data: &Data) -> Option<Cow<'static, str>>;

    /// Perform the action with the given arguments and the data provided.
    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect>;
}

mod check;
pub use self::check::Check;

mod help;
pub use self::help::Help;

mod merge;
pub use self::merge::Merge;

mod reformat;
pub use self::reformat::Reformat;

mod stage;
pub use self::stage::Stage;

mod test;
pub use self::test::Test;

mod unstage;
pub use self::unstage::Unstage;
