// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use crates::itertools::Itertools;

use actions::merge_requests::{Action, ActionError, Data, Effect};
use config::TestBackend;

mod utils;

mod jobs;
mod pipelines;
mod refs;

/// The test action
///
/// Tests a merge request topic. Testing is not performed directly, but is instead directed via
/// triggers or notifications to other testing platforms.
#[derive(Debug, Default, Clone, Copy)]
pub struct Test;

impl Action for Test {
    fn help(&self, data: &Data) -> Option<Cow<'static, str>> {
        let all_tests = data
            .main_branch
            .tests()
            .map(|(backend, action)| (Some(backend), action))
            .chain(
                data.main_branch
                    .test()
                    .map(|action| (None, action))
                    .into_iter(),
            );

        let backend_help = all_tests
            .map(|(backend, test)| {
                let inner_help = match test.test() {
                    TestBackend::Jobs {
                        action: ref test_jobs,
                        ref help,
                    } => {
                        let backend_action = jobs::TestJobsAction::new(test_jobs, help);
                        backend_action.help(data)
                    },
                    TestBackend::Refs(ref test_refs) => {
                        let backend_action = refs::TestRefsAction::new(test_refs);
                        backend_action.help(data)
                    },
                    TestBackend::Pipelines {
                        action: ref test_pipelines,
                        ref actor,
                    } => {
                        let backend_action =
                            pipelines::TestPipelinesAction::new(test_pipelines, actor);
                        backend_action.help(data)
                    },
                };
                let inner_help = inner_help.unwrap_or_else(|| "<unknown>".into());

                (backend, test.explicit_only, inner_help)
            })
            .collect::<Vec<_>>();

        if backend_help.is_empty() {
            return None;
        }
        let multiple_backends = backend_help.len() > 1;

        let backend_msg = backend_help
            .into_iter()
            .map(|(backend, explicit_only, inner_help)| {
                let explicit_msg = if explicit_only {
                    " (requires an explicit `-X` argument)"
                } else {
                    ""
                };

                if multiple_backends {
                    if let Some(backend) = backend {
                        format!(
                            "  * Backend `{}`{}: {}  ",
                            backend, explicit_msg, inner_help,
                        )
                        .into()
                    } else {
                        inner_help
                    }
                } else {
                    inner_help
                }
            })
            .join("\n");

        let msg = if multiple_backends {
            format!(
                "Multiple backends are supported. In order to restrict communication to a given \
                 backend, the first argument may be in the form of `-Xbackend` (without a space).\n\n{}",
                backend_msg,
            )
        } else {
            backend_msg
        };

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let all_tests = data
            .main_branch
            .tests()
            .map(|(backend, action)| (Some(backend), action))
            .chain(
                data.main_branch
                    .test()
                    .map(|action| (None, action))
                    .into_iter(),
            );

        let (requested_backend, args) = if data.main_branch.test().is_some() {
            (None, arguments)
        } else if let Some(backend) = arguments.get(0).and_then(|arg| arg.strip_prefix("-X")) {
            (Some(backend), &arguments[1..])
        } else {
            (None, arguments)
        };

        let mut supported = false;
        for (backend, test) in all_tests {
            supported = true;

            // Check if the backend should be considered.
            if test.explicit_only && requested_backend.is_none() {
                continue;
            }

            // Check if the requested backend excludes the current backend.
            if let Some(backend) = backend {
                if requested_backend.map(|b| b != backend).unwrap_or(false) {
                    continue;
                }
            }

            if data.action_data.cause.how < test.access_level {
                effects.push(ActionError::insufficient_permissions(test.access_level).into());
                continue;
            }

            let test_effects = match test.test() {
                TestBackend::Jobs {
                    action: ref test_jobs,
                    ref help,
                } => {
                    let backend_action = jobs::TestJobsAction::new(test_jobs, help);
                    backend_action.perform(args, data)
                },
                TestBackend::Refs(ref test_refs) => {
                    let backend_action = refs::TestRefsAction::new(test_refs);
                    backend_action.perform(args, data)
                },
                TestBackend::Pipelines {
                    action: ref test_pipelines,
                    ref actor,
                } => {
                    let backend_action = pipelines::TestPipelinesAction::new(test_pipelines, actor);
                    backend_action.perform(args, data)
                },
            };

            effects.extend(test_effects);
        }

        if !supported {
            effects.push(ActionError::not_supported().into());
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::Path;

    use crates::ghostflow::host::PipelineState;
    use crates::git_workarea::{CommitId, GitContext};
    use crates::serde_json::Value;

    use actions::merge_requests::test::utils;
    use handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    #[test]
    fn test_command_test_refuse_not_configured() {
        let mut service = TestService::new(
            "test_command_test_refuse_not_configured",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: the command is not supported"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    fn test_config() -> Value {
        json!({
            "test": {
                "required_access_level": "developer",
                "backend": "refs",
            },
        })
    }

    #[test]
    fn test_command_test_disallowed() {
        let mut service = TestService::new(
            "test_command_test_disallowed",
            vec![
                Action::create_user("fork"),
                Action::create_user("unrelated"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("unrelated", "Do: test"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": test_config(),
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: permission denied: insufficient access \
                   level (Developer)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_submitter_disallowed() {
        let mut service = TestService::new(
            "test_command_test_submitter_disallowed",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("fork", "Do: test"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": test_config(),
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: permission denied: insufficient access \
                   level (Developer)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    fn test_multiple_backends_config(test_job_dir: &Path) -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "tests": {
                            "required_access_level": "maintainer",
                            "backends": {
                                "jobs": {
                                    "backend": "jobs",
                                    "config": {
                                        "queue": test_job_dir,
                                        "help_string": "Example test job backend.",
                                    },
                                },
                                "refs": {
                                    "backend": "refs",
                                },
                                "pipelines": {
                                    "backend": "pipelines",
                                },
                            },
                        },
                    },
                },
            },
        })
    }

    #[test]
    fn test_command_test_multiple_backends() {
        let mut service = TestService::new(
            "test_command_test_multiple_backends",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_multiple_backends_config(&test_job_dir);
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!([]);
        let backports_obj = json!({});
        let merges = &[];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg("refs/test-topics/0")
            .output()
            .unwrap();
        assert!(rev_parse.status.success());
        let rev = String::from_utf8_lossy(&rev_parse.stdout);

        assert_eq!(rev.trim_end(), MERGE_REQUEST_TOPIC);
    }

    #[test]
    fn test_command_test_multiple_backends_only_jobs() {
        let mut service = TestService::new(
            "test_command_test_multiple_backends_only_jobs",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -Xjobs"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_multiple_backends_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "This topic has been queued for testing.".into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!([]);
        let backports_obj = json!({});
        let merges = &[];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg("refs/test-topics/0")
            .output()
            .unwrap();
        assert!(!rev_parse.status.success());
    }

    #[test]
    fn test_command_test_multiple_backends_only_refs() {
        let mut service = TestService::new(
            "test_command_test_multiple_backends_only_refs",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -Xrefs"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_multiple_backends_config(&test_job_dir);
        let end = EndSignal::Push {
            project: "ghostflow/example".into(),
            ref_: "refs/test-topics/0".into(),
            sha1: CommitId::new(MERGE_REQUEST_TOPIC),
        };

        service.launch(config, end).unwrap();

        assert_eq!(fs::read_dir(&test_job_dir).unwrap().count(), 0);

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg("refs/test-topics/0")
            .output()
            .unwrap();
        assert!(rev_parse.status.success());
        let rev = String::from_utf8_lossy(&rev_parse.stdout);

        assert_eq!(rev.trim_end(), MERGE_REQUEST_TOPIC);
    }

    #[test]
    fn test_command_test_multiple_backends_only_pipelines() {
        let mut service = TestService::new(
            "test_command_test_multiple_backends_only_pipelines",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -Xpipelines"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_multiple_backends_config(&test_job_dir);
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch(config, end).unwrap();

        assert_eq!(fs::read_dir(&test_job_dir).unwrap().count(), 0);

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg("refs/test-topics/0")
            .output()
            .unwrap();
        assert!(!rev_parse.status.success());
    }

    fn test_explicit_backends_config(test_job_dir: &Path) -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "tests": {
                            "required_access_level": "maintainer",
                            "backends": {
                                "jobs": {
                                    "backend": "jobs",
                                    "explicit_only": true,
                                    "config": {
                                        "queue": test_job_dir,
                                        "help_string": "Example test job backend.",
                                    },
                                },
                                "refs": {
                                    "backend": "refs",
                                    "explicit_only": true,
                                },
                                "pipelines": {
                                    "backend": "pipelines",
                                    "explicit_only": true,
                                },
                            },
                        },
                    },
                },
            },
        })
    }

    #[test]
    fn test_command_test_explicit_backend() {
        let mut service = TestService::new(
            "test_command_test_explicit_backend",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_explicit_backends_config(&test_job_dir);
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch_timeout(config, end).unwrap();

        assert_eq!(fs::read_dir(&test_job_dir).unwrap().count(), 0);

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg("refs/test-topics/0")
            .output()
            .unwrap();
        assert!(!rev_parse.status.success());
    }

    #[test]
    fn test_command_test_explicit_backend_jobs() {
        let mut service = TestService::new(
            "test_command_test_multiple_backends_only_pipelines",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -Xjobs"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_explicit_backends_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "This topic has been queued for testing.".into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!([]);
        let backports_obj = json!({});
        let merges = &[];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg("refs/test-topics/0")
            .output()
            .unwrap();
        assert!(!rev_parse.status.success());
    }

    #[test]
    fn test_command_test_explicit_backend_refs() {
        let mut service = TestService::new(
            "test_command_test_multiple_backends_only_pipelines",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -Xrefs"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_explicit_backends_config(&test_job_dir);
        let end = EndSignal::Push {
            project: "ghostflow/example".into(),
            ref_: "refs/test-topics/0".into(),
            sha1: CommitId::new(MERGE_REQUEST_TOPIC),
        };

        service.launch(config, end).unwrap();

        assert_eq!(fs::read_dir(&test_job_dir).unwrap().count(), 0);

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg("refs/test-topics/0")
            .output()
            .unwrap();
        assert!(rev_parse.status.success());
        let rev = String::from_utf8_lossy(&rev_parse.stdout);

        assert_eq!(rev.trim_end(), MERGE_REQUEST_TOPIC);
    }

    #[test]
    fn test_command_test_explicit_backend_pipelines() {
        let mut service = TestService::new(
            "test_command_test_multiple_backends_only_pipelines",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -Xpipelines"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_explicit_backends_config(&test_job_dir);
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch(config, end).unwrap();

        assert_eq!(fs::read_dir(&test_job_dir).unwrap().count(), 0);

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        let rev_parse = ctx
            .git()
            .arg("rev-parse")
            .arg("refs/test-topics/0")
            .output()
            .unwrap();
        assert!(!rev_parse.status.success());
    }
}
