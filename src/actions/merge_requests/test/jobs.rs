// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::iter;

use crates::ghostflow::actions::test::jobs::{TestJobs, TestJobsError};
use crates::serde_json::Value;

use actions;
use actions::merge_requests::test::utils;
use actions::merge_requests::{Action, ActionError, Backport, Data, Effect, InnerActionError};

impl InnerActionError for TestJobsError {}

#[derive(Debug, Clone)]
pub struct TestJobsAction<'a> {
    test_jobs: &'a TestJobs,
    help: &'a str,
}

impl<'a> TestJobsAction<'a> {
    pub fn new(test_jobs: &'a TestJobs, help: &'a str) -> Self {
        Self {
            test_jobs,
            help,
        }
    }
}

impl<'a> Action for TestJobsAction<'a> {
    fn help(&self, _: &Data) -> Option<Cow<'static, str>> {
        let msg = self.help.to_string();

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let merged = arguments.iter().any(|&arg| arg == "--merged");

        let merge_data = if merged {
            let backport_merges = data
                .info
                .backports()
                .into_iter()
                .chain(iter::once(Backport::main_target(
                    &data.info.merge_request.target_branch,
                )))
                .filter_map(|backport| {
                    let (merge_effects, backport_merge) = utils::merge_for_testing(data, &backport);
                    effects.extend(merge_effects);
                    backport_merge
                })
                .collect::<Vec<_>>();
            let push_result = if backport_merges.is_empty() {
                let msg = "Failed to merge any branches; ignoring the request to test.";
                effects.push(Effect::error(msg));
                return effects;
            } else {
                let (push_effects, push_result) = utils::push_merges(data, &backport_merges);
                effects.extend(push_effects);
                push_result
            };

            if matches!(push_result, utils::PushStatus::Success) {
                let merge_info = backport_merges
                    .into_iter()
                    .map(|test_merge| {
                        let json = json!({
                            "refname": test_merge.refname,
                            "commit": test_merge.commit.as_str(),
                        });
                        (test_merge.branch, json)
                    })
                    .collect();
                Some(Value::Object(merge_info))
            } else {
                None
            }
        } else {
            Some(json!({}))
        };

        if let Some(merge_obj) = merge_data {
            let args = Value::Array(
                arguments
                    .iter()
                    .map(|&arg| Value::String(arg.into()))
                    .collect(),
            );
            let job_data = json!({
                "arguments": args,
                "backports": data.info.backports_json(&data.action_data.project.context),
                "merges": merge_obj,
                "merge_request": data.action_data.job_data,
            });
            let job_object = actions::utils::test_job("test_action", &job_data);
            if let Err(err) = self.test_jobs.test_mr(&data.info.merge_request, job_object) {
                effects.push(ActionError::from(err).into());
            }
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::path::Path;

    use crates::serde_json::Value;

    use actions::merge_requests::test::utils;
    use handlers::test::*;

    const MERGE_REQUEST_PARENT: &str = "910392b73b31e766e155a032b3a565a4d0b3bfc9";
    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";
    const MERGE_REQUEST_TOPIC_CONFLICT: &str = "4c470b2665edb1c41aa09a8b54aaea21347a0093";
    const MERGE_REQUEST_TOPIC_BACKPORT_CONFLICT: &str = "82f91eb38ebe6717a52760328ee29f7560e43873";
    const MAINLINE_COMMIT: &str = "cb8272d9698f22c43ba879f8d95cdf3e2c85a68b";
    const BRANCH_NEXT: &str = "27485b5013bf65f5bccc17de921f67c069d43423";

    fn test_jobs_config(test_job_dir: &Path) -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "test": {
                            "required_access_level": "maintainer",
                            "backend": "jobs",
                            "config": {
                                "queue": test_job_dir,
                                "help_string": "Example test job backend.",
                            },
                        },
                    },
                },
            },
        })
    }

    fn test_jobs_backport_config(test_job_dir: &Path) -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "test": {
                            "required_access_level": "maintainer",
                            "backend": "jobs",
                            "config": {
                                "queue": test_job_dir,
                                "help_string": "Example test job backend.",
                            },
                        },
                    },
                    "next": {},
                },
            },
        })
    }

    #[test]
    fn test_command_test_jobs() {
        let mut service = TestService::new(
            "test_command_test_jobs",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --arg1 --arg2"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "This topic has been queued for testing.".into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--arg1", "--arg2"]);
        let backports_obj = json!({});
        let merges = &[];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_multiple() {
        let mut service = TestService::new(
            "test_command_test_jobs_multiple",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment(
                    "ghostflow",
                    "Do: test --arg1 --arg2\nDo: test --arg3 --arg4",
                ),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "This topic has been queued for testing.".into(),
        };

        service.launch(config, end).unwrap();

        let arguments1_obj = json!(["--arg1", "--arg2"]);
        let arguments2_obj = json!(["--arg3", "--arg4"]);
        let arguments_obj = vec![arguments1_obj, arguments2_obj];
        let backports_obj = json!({});
        let merges = &[];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "");

        utils::tests::check_test_job_dir_multi(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_merged() {
        let mut service = TestService::new(
            "test_command_test_jobs_merged",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "This topic has been queued for testing.".into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--merged"]);
        let backports_obj = json!({});
        let merges = &["master"];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_merged_existing_refs() {
        let mut service = TestService::new(
            "test_command_test_jobs_merged_existing_refs",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::push(
                    "ghostflow",
                    "example",
                    "refs/test/master/0/heads/0",
                    BRANCH_NEXT,
                ),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "This topic has been queued for testing.".into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--merged"]);
        let backports_obj = json!({});
        let merges = &["master"];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "");

        utils::tests::check_test_job_dir_count(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
            1,
        );
    }

    #[test]
    fn test_command_test_jobs_merged_no_common_history() {
        let mut service = TestService::new(
            "test_command_test_jobs_merged_no_common_history",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "mainline", MAINLINE_COMMIT),
                Action::push("fork", "example", "mr-source", MAINLINE_COMMIT),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Messages:\n\n  \
                 - While processing the `test` command: Failed to merge for testing into `master` \
                   (ignoring testing for this branch): no common history.\n\n\
                 Errors: \n\n  \
                 - While processing the `test` command: Failed to merge any branches; ignoring \
                   the request to test."
                .into(),
        };

        service.launch(config, end).unwrap();

        // These are empty because only the `mr_update` job should have been dropped.
        let arguments_obj = json!([]);
        let backports_obj = json!({});
        let merges = &[];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MAINLINE_COMMIT, "");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_merged_already_merged() {
        let mut service = TestService::new(
            "test_command_test_jobs_merged_already_merged",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_PARENT),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Messages:\n\n  \
                 - While processing the `test` command: Failed to merge for testing into `master` \
                   (ignoring testing for this branch): the topic has already been merged.\n\n\
                 Errors: \n\n  \
                 - While processing the `test` command: Failed to merge any branches; ignoring \
                   the request to test."
                .into(),
        };

        service.launch(config, end).unwrap();

        // These are empty because only the `mr_update` job should have been dropped.
        let arguments_obj = json!([]);
        let backports_obj = json!({});
        let merges = &[];
        let mr_obj = utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_PARENT, "");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_merged_conflicts() {
        let mut service = TestService::new(
            "test_command_test_jobs_merged_conflicts",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC_CONFLICT),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Messages:\n\n  \
                 - While processing the `test` command: Failed to merge for testing into `master` \
                   (ignoring testing for this branch): conflicts in `master`.\n\n\
                 Errors: \n\n  \
                 - While processing the `test` command: Failed to merge any branches; ignoring \
                   the request to test."
                .into(),
        };

        service.launch(config, end).unwrap();

        // These are empty because only the `mr_update` job should have been dropped.
        let arguments_obj = json!([]);
        let backports_obj = json!({});
        let merges = &[];
        let mr_obj =
            utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC_CONFLICT, "");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_backports() {
        let mut service = TestService::new(
            "test_command_test_jobs_backports",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_backport_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "This topic has been queued for testing.".into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!([]);
        let backports_obj = json!({
            "next": MERGE_REQUEST_TOPIC,
        });
        let merges = &[];
        let mr_obj =
            utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "Backport: next");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_backports_merged() {
        let mut service = TestService::new(
            "test_command_test_jobs_backports_merged",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_backport_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "This topic has been queued for testing.".into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--merged"]);
        let backports_obj = json!({
            "next": MERGE_REQUEST_TOPIC,
        });
        let merges = &["master", "next"];
        let mr_obj =
            utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "Backport: next");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_backports_merged_no_target_branch() {
        let mut service = TestService::new(
            "test_command_test_jobs_backports_merged_no_target_branch",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "Backport: next", false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Messages:\n\n  \
                 - While processing the `test` command: Failed to merge for testing into `next` \
                   (ignoring testing for this branch): no such branch."
                .into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--merged"]);
        let backports_obj = json!({
            "next": MERGE_REQUEST_TOPIC,
        });
        let merges = &["master"];
        let mr_obj =
            utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, "Backport: next");

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_backports_merged_no_backport_commit() {
        let description = "Backport: next:0000000000000000000000000000000000000000";
        let mut service = TestService::new(
            "test_command_test_jobs_backports_merged_no_backport_commit",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", description, false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_backport_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Messages:\n\n  \
                 - While processing the `test` command: Failed to merge for testing into `next` \
                   (ignoring testing for this branch): failed to determine the commit for \
                   backport."
                .into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--merged"]);
        let backports_obj = json!({
            "next": (),
        });
        let merges = &["master"];
        let mr_obj =
            utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, description);

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_backports_merged_no_common_history() {
        let description = format!("Backport: next:{}", MAINLINE_COMMIT);
        let mut service = TestService::new(
            "test_command_test_jobs_backports_merged_no_common_history",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("ghostflow", "example", "mainline", MAINLINE_COMMIT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", &description, false),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_backport_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Messages:\n\n  \
                 - While processing the `test` command: Failed to merge for testing into `next` \
                   (ignoring testing for this branch): no common history."
                .into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--merged"]);
        let backports_obj = json!({
            "next": MAINLINE_COMMIT,
        });
        let merges = &["master"];
        let mr_obj =
            utils::tests::expected_mr_object(service.root(), MERGE_REQUEST_TOPIC, &description);

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_backports_merged_already_merged() {
        let mut service = TestService::new(
            "test_command_test_jobs_backports_merged_already_merged",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr(
                    "ghostflow",
                    "example",
                    "fork",
                    "Backport: next:HEAD~",
                    false,
                ),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_backport_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Messages:\n\n  \
                 - While processing the `test` command: Failed to merge for testing into `next` \
                   (ignoring testing for this branch): the topic has already been merged."
                .into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--merged"]);
        let backports_obj = json!({
            "next": MERGE_REQUEST_PARENT,
        });
        let merges = &["master"];
        let mr_obj = utils::tests::expected_mr_object(
            service.root(),
            MERGE_REQUEST_TOPIC,
            "Backport: next:HEAD~",
        );

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }

    #[test]
    fn test_command_test_jobs_backports_merged_conflicts() {
        let mut service = TestService::new(
            "test_command_test_jobs_backports_merged_conflicts",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
                Action::push(
                    "fork",
                    "example",
                    "mr-source",
                    MERGE_REQUEST_TOPIC_BACKPORT_CONFLICT,
                ),
                Action::create_mr(
                    "ghostflow",
                    "example",
                    "fork",
                    "Backport: next:HEAD^2",
                    false,
                ),
                Action::mr_comment("ghostflow", "Do: test --merged"),
            ],
        )
        .unwrap();
        let test_job_dir = service.root().join("test-jobs");
        fs::create_dir_all(&test_job_dir).unwrap();
        let config = test_jobs_config(&test_job_dir);
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Messages:\n\n  \
                 - While processing the `test` command: Failed to merge for testing into `next` \
                   (ignoring testing for this branch): conflicts in `master`."
                .into(),
        };

        service.launch(config, end).unwrap();

        let arguments_obj = json!(["--merged"]);
        let backports_obj = json!({
            "next": MERGE_REQUEST_TOPIC_CONFLICT,
        });
        let merges = &["master"];
        let mr_obj = utils::tests::expected_mr_object(
            service.root(),
            MERGE_REQUEST_TOPIC_BACKPORT_CONFLICT,
            "Backport: next:HEAD^2",
        );

        utils::tests::check_test_job_dir(
            &test_job_dir,
            &arguments_obj,
            &backports_obj,
            merges,
            &mr_obj,
        );
    }
}
