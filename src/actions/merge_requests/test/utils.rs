// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_workarea::{CommitId, MergeResult, MergeStatus};
use crates::itertools::Itertools;

use actions::merge_requests::Backport;
use actions::merge_requests::{Data, Effect};

macro_rules! merge_bail {
    ($data:expr, $effects:expr, $err:expr, $backport:expr, $format:expr ) => {
        merge_bail!($data, $effects, $err, $backport, $format,)
    };
    ($data:expr, $effects:expr, $err:expr, $backport:expr, $format:expr, $( $arg:expr ),* $(,)? ) => {
        let msg = format!(
            concat!(
                "Failed to merge for testing into `{}` (ignoring testing for this branch): ",
                $format,
                ".",
            ),
            $backport.branch(),
            $($arg,)*
        );
        error!(
            target: "ghostflow-director/handler",
            "{}: {:?}",
            msg,
            $err,
        );

        $effects.push(Effect::message(msg));

        return ($effects, None);
    };
}

pub struct BackportTestMerge {
    pub branch: String,
    pub refname: String,
    pub commit: CommitId,
}

pub fn merge_for_testing(
    data: &Data,
    backport: &Backport,
) -> (Vec<Effect>, Option<BackportTestMerge>) {
    let ctx = &data.action_data.project.context;
    let mr = &data.info.merge_request;
    let cause = &data.action_data.cause;

    let mut effects = Vec::new();

    let refnames = format!("refs/test/{}/{}/heads/*", backport.branch(), mr.id);
    if let Err(err) = ctx.force_fetch_into("origin", &refnames, &refnames) {
        merge_bail!(
            data,
            effects,
            err,
            backport,
            "failed to fetch existing test refs",
        );
    }

    let target = CommitId::new(backport.branch());
    let workarea = match ctx.prepare(&target) {
        Ok(workarea) => workarea,
        Err(err) => {
            merge_bail!(data, effects, err, backport, "no such branch");
        },
    };

    let topic = match backport.commit(mr, ctx) {
        Ok(commit) => commit,
        Err(err) => {
            merge_bail!(
                data,
                effects,
                err,
                backport,
                "failed to determine the commit for backport",
            );
        },
    };
    let bases = match ctx.mergeable(&target, &topic) {
        Ok(status) => {
            match status {
                MergeStatus::Mergeable(bases) => bases,
                MergeStatus::NoCommonHistory => {
                    merge_bail!(data, effects, status, backport, "no common history");
                },
                MergeStatus::AlreadyMerged => {
                    merge_bail!(
                        data,
                        effects,
                        status,
                        backport,
                        "the topic has already been merged",
                    );
                },
            }
        },
        Err(err) => {
            merge_bail!(
                data,
                effects,
                err,
                backport,
                "failed to determine if the topic is mergeable",
            );
        },
    };
    let merge = match workarea.setup_merge(&bases, &target, &topic) {
        Ok(MergeResult::Ready(mut command)) => {
            command
                .author(&cause.who.identity())
                .author_date(cause.when);
            let commit_msg = format!(
                "WIP: Merge topic '{}' into '{}' for testing",
                mr.source_branch,
                backport.branch(),
            );
            match command.commit(commit_msg) {
                Ok(commit) => commit,
                Err(err) => {
                    merge_bail!(data, effects, err, backport, "failed to perform the merge");
                },
            }
        },
        Ok(MergeResult::Conflict(conflicts)) => {
            merge_bail!(
                data,
                effects,
                (),
                backport,
                "conflicts in `{}`",
                conflicts
                    .iter()
                    .map(|conflict| conflict.path().to_string_lossy())
                    .dedup()
                    .format("`, `"),
            );
        },
        Err(err) => {
            merge_bail!(data, effects, err, backport, "failed to setup the merge");
        },
    };
    let ref_prefix = format!("test/{}/{}", backport.branch(), mr.id);
    let refname = match ctx.reserve_ref(ref_prefix, &merge) {
        Ok((refname, _)) => refname,
        Err(err) => {
            merge_bail!(
                data,
                effects,
                err,
                backport,
                "failed to reserve a reference to store the merge",
            );
        },
    };

    (
        effects,
        Some(BackportTestMerge {
            branch: backport.branch().into(),
            refname,
            commit: merge,
        }),
    )
}

pub enum PushStatus {
    Success,
    Failure,
}

pub fn push_merges(data: &Data, test_merges: &[BackportTestMerge]) -> (Vec<Effect>, PushStatus) {
    let push_specs = test_merges.iter().map(|test_merge| &test_merge.refname);
    let push_cmd = data
        .action_data
        .project
        .context
        .git()
        .arg("push")
        .arg("--atomic")
        .arg("origin")
        .args(push_specs)
        .output();

    let push_status = match push_cmd {
        Ok(push) => {
            if push.status.success() {
                PushStatus::Success
            } else {
                error!(
                    target: "ghostflow-director/handler",
                    "Failed to push test merge commits: {:?}",
                    String::from_utf8_lossy(&push.stderr),
                );

                PushStatus::Failure
            }
        },
        Err(err) => {
            error!(
                target: "ghostflow-director/handler",
                "Failed to construct the push command: {:?}",
                err,
            );

            PushStatus::Failure
        },
    };

    let mut effects = Vec::new();

    if matches!(push_status, PushStatus::Failure) {
        let msg = "Failed to push test merge commits; ignoring the request to test.";
        effects.push(Effect::error(msg));
    }

    (effects, push_status)
}

#[cfg(test)]
pub mod tests {
    use std::fs::{self, File};
    use std::path::Path;

    use crates::itertools::Itertools;
    use crates::serde_json::Value;

    pub fn expected_mr_object(root: &Path, commit: &str, desc: &str) -> Value {
        let source_repo_path = root.join("projects/fork/example.git");
        let target_repo_path = root.join("projects/ghostflow/example.git");

        let target_repo = json!({
            "forked_from": (),
            "id": 0,
            "name": "ghostflow/example",
            "url": target_repo_path.to_string_lossy(),
        });
        let source_repo = json!({
            "forked_from": target_repo,
            "id": 1,
            "name": "fork/example",
            "url": source_repo_path.to_string_lossy(),
        });

        json!({
            "author": {
                "email": "fork@example.org",
                "handle": "fork",
                "id": 1,
                "name": "fork",
            },
            "commit": {
                "id": commit,
                "refname": "mr-source",
                "repo": source_repo,
                "last_pipeline": null,
            },
            "description": desc,
            "id": 0,
            "old_commit": (),
            "reference": "!0",
            "remove_source_branch": false,
            "source_branch": "mr-source",
            "source_repo": source_repo,
            "target_branch": "master",
            "target_repo": target_repo,
            "url": "0/merge_requests/0",
            "work_in_progress": false,
        })
    }

    pub fn check_test_job_dir_count(
        job_dir: &Path,
        arguments: &Value,
        backports: &Value,
        merges: &[&str],
        mr: &Value,
        count: u64,
    ) {
        for dirent in fs::read_dir(job_dir).unwrap() {
            let entry = dirent.unwrap();
            assert!(entry.file_name().to_string_lossy().ends_with(".json"));
            let mut fin = File::open(entry.path()).unwrap();
            let job_obj: Value = serde_json::from_reader(&mut fin).unwrap();
            let kind = job_obj
                .pointer("/kind")
                .expect("no kind value")
                .as_str()
                .unwrap();

            if kind == "test_action" {
                assert_eq!(
                    job_obj
                        .pointer("/data/arguments")
                        .expect("no arguments value"),
                    arguments,
                );
                assert_eq!(
                    job_obj
                        .pointer("/data/backports")
                        .expect("no backports value"),
                    backports,
                );
                for merge in merges {
                    let pointer = format!("/data/merges/{}", merge);
                    let refname = format!("refs/test/{}/0/heads/{}", merge, count);
                    let merge_obj = job_obj.pointer(&pointer).unwrap();
                    assert_eq!(
                        merge_obj
                            .pointer("/refname")
                            .expect("no refname value")
                            .as_str()
                            .unwrap(),
                        refname,
                    );
                    assert!(merge_obj
                        .pointer("/commit")
                        .expect("no commit value")
                        .as_str()
                        .is_some());
                }
                assert_eq!(
                    job_obj
                        .pointer("/data/merge_request/merge_request")
                        .expect("no merge_request value"),
                    mr,
                );
                assert_eq!(
                    job_obj
                        .pointer("/data/merge_request/action")
                        .expect("no action value"),
                    "comment",
                );
                assert!(job_obj
                    .pointer("/data/merge_request/note")
                    .expect("no note value")
                    .as_object()
                    .is_some());
            } else if kind == "mr_update" {
                assert_eq!(
                    job_obj
                        .pointer("/data/backports")
                        .expect("no backports value"),
                    backports,
                );
                assert_eq!(
                    job_obj
                        .pointer("/data/merge_request/merge_request")
                        .expect("no merge_request value"),
                    mr,
                );
                assert_eq!(
                    job_obj
                        .pointer("/data/merge_request/action")
                        .expect("no action value"),
                    "open",
                );
            } else {
                panic!("unrecognized test job kind: {}", kind);
            }
        }
    }

    pub fn check_test_job_dir_multi(
        job_dir: &Path,
        arguments: &[Value],
        backports: &Value,
        merges: &[&str],
        mr: &Value,
    ) {
        let files = fs::read_dir(job_dir)
            .unwrap()
            .map(|dirent| dirent.unwrap())
            .sorted_by_key(|entry| entry.file_name());
        let mut args = arguments.iter();

        for entry in files {
            assert!(entry.file_name().to_string_lossy().ends_with(".json"));
            let mut fin = File::open(entry.path()).unwrap();
            let job_obj: Value = serde_json::from_reader(&mut fin).unwrap();
            let kind = job_obj
                .pointer("/kind")
                .expect("no kind value")
                .as_str()
                .unwrap();

            if kind == "test_action" {
                assert_eq!(
                    job_obj
                        .pointer("/data/arguments")
                        .expect("no arguments value"),
                    args.next().unwrap(),
                );
                assert_eq!(
                    job_obj
                        .pointer("/data/backports")
                        .expect("no backports value"),
                    backports,
                );
                for merge in merges {
                    let pointer = format!("/data/merges/{}", merge);
                    let refname = format!("refs/test/{}/0/heads/0", merge);
                    let merge_obj = job_obj.pointer(&pointer).unwrap();
                    assert_eq!(
                        merge_obj
                            .pointer("/refname")
                            .expect("no refname value")
                            .as_str()
                            .unwrap(),
                        refname,
                    );
                    assert!(merge_obj
                        .pointer("/commit")
                        .expect("no commit value")
                        .as_str()
                        .is_some());
                }
                assert_eq!(
                    job_obj
                        .pointer("/data/merge_request/merge_request")
                        .expect("no merge_request value"),
                    mr,
                );
                assert_eq!(
                    job_obj
                        .pointer("/data/merge_request/action")
                        .expect("no action value"),
                    "comment",
                );
                assert!(job_obj
                    .pointer("/data/merge_request/note")
                    .expect("no note value")
                    .as_object()
                    .is_some());
            } else if kind == "mr_update" {
                assert_eq!(
                    job_obj
                        .pointer("/data/backports")
                        .expect("no backports value"),
                    backports,
                );
                assert_eq!(
                    job_obj
                        .pointer("/data/merge_request/merge_request")
                        .expect("no merge_request value"),
                    mr,
                );
                assert_eq!(
                    job_obj
                        .pointer("/data/merge_request/action")
                        .expect("no action value"),
                    "open",
                );
            } else {
                panic!("unrecognized test job kind: {}", kind);
            }
        }

        assert_eq!(args.next(), None, "Arguments for a test command not found")
    }

    pub fn check_test_job_dir(
        job_dir: &Path,
        arguments: &Value,
        backports: &Value,
        merges: &[&str],
        mr: &Value,
    ) {
        check_test_job_dir_count(job_dir, arguments, backports, merges, mr, 0)
    }
}
