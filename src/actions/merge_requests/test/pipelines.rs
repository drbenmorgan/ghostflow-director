// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use crates::clap::Arg;
use crates::ghostflow::actions::test::pipelines::{self, TestPipelines, TestPipelinesError};
use crates::regex::Regex;

use actions::merge_requests::utils;
use actions::merge_requests::{Action, ActionError, Data, Effect, InnerActionError};
use config::io::TestPipelinesActor;

impl InnerActionError for TestPipelinesError {}

pub struct TestPipelinesAction<'a> {
    test_pipelines: &'a TestPipelines,
    actor: &'a TestPipelinesActor,
}

impl<'a> TestPipelinesAction<'a> {
    pub fn new(test_pipelines: &'a TestPipelines, actor: &'a TestPipelinesActor) -> Self {
        Self {
            test_pipelines,
            actor,
        }
    }
}

impl<'a> Action for TestPipelinesAction<'a> {
    fn help(&self, _: &Data) -> Option<Cow<'static, str>> {
        let msg =
            "Triggers the service's CI APIs. It can start or restart CI jobs within the merge \
             request's latest pipeline. It supports the `--action` (or `-a`) flag to select the \
             action to perform. Supported actions are:\n\n  \
             - `manual`: Start jobs awaiting manual action\n  \
             - `unsuccessful`: Restart jobs which completed without success (including skipped or \
               canceled jobs)\n  \
             - `failed`: Restart jobs which completed with failure\n  \
             - `completed`: Restart all completed jobs\n\n\
             Jobs affected by the action may also be filtered. The `--stage` (or `-s`) flag may \
             be used to give the name of a stage to perform the action on. The `--named` (or \
             `-n`) flag may be used multiple times to only affect jobs which match the argument. \
             When given multiple `--named` arguments, a job may match any of the arguments to be \
             affected.";

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let matches = utils::command_app("test")
            .arg(
                Arg::with_name("ACTION")
                    .long("action")
                    .short("a")
                    .takes_value(true)
                    .possible_values(&["manual", "unsuccessful", "failed", "completed"]),
            )
            .arg(
                Arg::with_name("STAGE")
                    .long("stage")
                    .short("s")
                    .takes_value(true),
            )
            .arg(
                Arg::with_name("NAMED")
                    .long("named")
                    .short("n")
                    .takes_value(true)
                    .multiple(true)
                    .number_of_values(1),
            )
            // TODO: Support testing merged branches. This requires support for creating new
            // pipelines and attaching them to a merge request.
            // .arg(Arg::with_name("MERGED").long("merged").short("m").takes_value(false))
            .get_matches_from_safe(arguments);

        // TODO: Support testing backport branches. As with testing merged branches, this
        // requires support for creating new pipelines and attaching them to a merge request.

        let matches = match matches {
            Ok(matches) => matches,
            Err(err) => {
                let msg = err.message.lines().next().unwrap_or("<unknown error>");
                effects.push(ActionError::unrecognized_arguments(msg).into());
                return effects;
            },
        };

        let mut builder = pipelines::TestPipelinesOptions::builder();
        let mut err_occurred = false;

        let action = matches.value_of("ACTION").map(|action| {
            match action {
                "manual" => pipelines::TestPipelinesAction::StartManual,
                "unsuccessful" => pipelines::TestPipelinesAction::RestartUnsuccessful,
                "failed" => pipelines::TestPipelinesAction::RestartFailed,
                "completed" => pipelines::TestPipelinesAction::RestartAll,
                action => {
                    unreachable!(
                        "the argument parser should have rejected the '{}' action",
                        action,
                    )
                },
            }
        });
        if let Some(action) = action {
            builder.action(action);
        }

        if let Some(stage) = matches.value_of("STAGE") {
            builder.stage(stage);
        }

        if let Some(jobs) = matches.values_of("NAMED") {
            for job in jobs {
                match Regex::new(job) {
                    Ok(re) => {
                        builder.jobs_matching(re);
                    },
                    Err(_) => {
                        err_occurred = true;
                        let msg = format!("failed to compile `{}` as a regular expression", job);
                        effects.push(Effect::error(msg));
                    },
                }
            }
        }

        let user = match self.actor {
            TestPipelinesActor::Self_ => None,
            TestPipelinesActor::Author => Some(&data.info.merge_request.author.handle),
            TestPipelinesActor::Requester => Some(&data.action_data.cause.who.handle),
            TestPipelinesActor::User(user) => Some(user),
        };
        if let Some(user) = user {
            builder.user(user);
        }

        if !err_occurred {
            let options = builder.build().unwrap();
            let res = self
                .test_pipelines
                .test_mr(&data.info.merge_request, &options);

            if let Err(err) = res {
                match err {
                    TestPipelinesError::NoPipelinesAvailable => {
                        let msg = "Pipelines seem to be disabled for this merge request.";
                        effects.push(Effect::error(msg));
                    },
                    TestPipelinesError::NoPipelines => {
                        let msg = "No pipelines found for this merge request.";
                        effects.push(Effect::error(msg));
                    },
                    err => {
                        effects.push(ActionError::from(err).into());
                    },
                }
            }
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use crates::ghostflow::host::PipelineState;
    use crates::serde_json::Value;

    use handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    fn test_pipelines_config(actor: &str) -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "test": {
                            "required_access_level": "maintainer",
                            "backend": "pipelines",
                            "config": {
                                "actor": actor,
                            },
                        },
                    },
                },
            },
        })
    }

    fn test_pipelines_config_custom(user: &str) -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "test": {
                            "required_access_level": "maintainer",
                            "backend": "pipelines",
                            "config": {
                                "actor": {
                                    "user": user,
                                },
                            },
                        },
                    },
                },
            },
        })
    }

    #[test]
    fn test_command_test_pipelines_bad_arguments() {
        let mut service = TestService::new(
            "test_command_test_pipelines_bad_arguments",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::EnablePipelines,
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --unrecognized"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: Found \
                   argument '--unrecognized' which wasn't expected, or isn't valid in this \
                   context"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_no_pipelines() {
        let mut service = TestService::new(
            "test_command_test_pipelines_no_pipelines",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::EnablePipelines,
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: Pipelines seem to be disabled for this \
                   merge request."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_no_pipelines_found() {
        let mut service = TestService::new(
            "test_command_test_pipelines_no_pipelines_found",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: No pipelines found for this merge \
                   request."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_a_missing_argument() {
        let mut service = TestService::new(
            "test_command_test_pipelines_a_missing_argument",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -a"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: The \
                   argument '--action <ACTION>' requires a value but none was supplied"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_action_missing_argument() {
        let mut service = TestService::new(
            "test_command_test_pipelines_action_missing_argument",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --action"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: The \
                   argument '--action <ACTION>' requires a value but none was supplied"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_a_invalid_argument() {
        let mut service = TestService::new(
            "test_command_test_pipelines_a_invalid_argument",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -a invalid"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: 'invalid' \
                   isn't a valid value for '--action <ACTION>'"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_action_invalid_argument() {
        let mut service = TestService::new(
            "test_command_test_pipelines_action_invalid_argument",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --action invalid"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: 'invalid' \
                   isn't a valid value for '--action <ACTION>'"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_s_missing_argument() {
        let mut service = TestService::new(
            "test_command_test_pipelines_s_missing_argument",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -s"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: The \
                   argument '--stage <STAGE>' requires a value but none was supplied"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_stage_missing_argument() {
        let mut service = TestService::new(
            "test_command_test_pipelines_stage_missing_argument",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --stage"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: The \
                   argument '--stage <STAGE>' requires a value but none was supplied"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_n_missing_argument() {
        let mut service = TestService::new(
            "test_command_test_pipelines_n_missing_argument",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test -n"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: The \
                   argument '--named <NAMED>...' requires a value but none was supplied"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_test_pipelines_named_missing_argument() {
        let mut service = TestService::new(
            "test_command_test_pipelines_named_missing_argument",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --named"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: unrecognized arguments: error: The \
                   argument '--named <NAMED>...' requires a value but none was supplied"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    fn test_command_test_pipelines_action_state(
        test: &str,
        action: &str,
        state: PipelineState,
        should_run: bool,
    ) {
        let mut service = TestService::new(
            test,
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(state, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", &format!("Do: test --action {}", action)),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        let service = if should_run {
            service.launch(config, end)
        } else {
            service.launch_timeout(config, end)
        };
        service.unwrap()
    }

    #[test]
    fn test_command_test_pipelines_action_manual_state_manual() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_manual_state_manual",
            "manual",
            PipelineState::Manual,
            true,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_manual_state_in_progress() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_manual_state_in_progress",
            "manual",
            PipelineState::InProgress,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_manual_state_canceled() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_manual_state_canceled",
            "manual",
            PipelineState::Canceled,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_manual_state_failed() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_manual_state_failed",
            "manual",
            PipelineState::Failed,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_manual_state_success() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_manual_state_success",
            "manual",
            PipelineState::Success,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_unsuccessful_state_manual() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_unsuccessful_state_manual",
            "unsuccessful",
            PipelineState::Manual,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_unsuccessful_state_in_progress() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_unsuccessful_state_in_progress",
            "unsuccessful",
            PipelineState::InProgress,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_unsuccessful_state_canceled() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_unsuccessful_state_canceled",
            "unsuccessful",
            PipelineState::Canceled,
            true,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_unsuccessful_state_failed() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_unsuccessful_state_failed",
            "unsuccessful",
            PipelineState::Failed,
            true,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_unsuccessful_state_success() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_unsuccessful_state_success",
            "unsuccessful",
            PipelineState::Success,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_failed_state_manual() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_failed_state_manual",
            "failed",
            PipelineState::Manual,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_failed_state_in_progress() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_failed_state_in_progress",
            "failed",
            PipelineState::InProgress,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_failed_state_canceled() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_failed_state_canceled",
            "failed",
            PipelineState::Canceled,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_failed_state_failed() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_failed_state_failed",
            "failed",
            PipelineState::Failed,
            true,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_failed_state_success() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_failed_state_success",
            "failed",
            PipelineState::Success,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_completed_state_manual() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_completed_state_manual",
            "completed",
            PipelineState::Manual,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_completed_state_in_progress() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_completed_state_in_progress",
            "completed",
            PipelineState::InProgress,
            false,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_completed_state_canceled() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_completed_state_canceled",
            "completed",
            PipelineState::Canceled,
            true,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_completed_state_failed() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_completed_state_failed",
            "completed",
            PipelineState::Failed,
            true,
        )
    }

    #[test]
    fn test_command_test_pipelines_action_completed_state_success() {
        test_command_test_pipelines_action_state(
            "test_command_test_pipelines_action_completed_state_success",
            "completed",
            PipelineState::Success,
            true,
        )
    }

    #[test]
    fn test_command_test_pipelines_stage_matches() {
        let mut service = TestService::new(
            "test_command_test_pipelines_stage_matches",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, "stage", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --stage stage"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_stage_no_match() {
        let mut service = TestService::new(
            "test_command_test_pipelines_stage_no_match",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, "stage", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --stage not-stage"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch_timeout(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_stage_no_match_unknown() {
        let mut service = TestService::new(
            "test_command_test_pipelines_stage_no_match_unknown",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --stage not-stage"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch_timeout(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_name_matches() {
        let mut service = TestService::new(
            "test_command_test_pipelines_name_matches",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --named j"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_name_no_match() {
        let mut service = TestService::new(
            "test_command_test_pipelines_name_no_match",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --named unknown"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch_timeout(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_name_matches_many() {
        let mut service = TestService::new(
            "test_command_test_pipelines_name_matches_many",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --named unknown --named j"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_name_invalid_regex_comment() {
        let mut service = TestService::new(
            "test_command_test_pipelines_name_invalid_regex_comment",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --named ["),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: failed to compile `[` as a regular \
                   expression"
                .into(),
        };

        service.launch(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_name_invalid_regex_no_run() {
        let mut service = TestService::new(
            "test_command_test_pipelines_name_invalid_regex_no_run",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test --named ["),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("self");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: None,
        };

        service.launch_timeout(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_actor_author() {
        let mut service = TestService::new(
            "test_command_test_pipelines_actor_author",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("author");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: Some("fork".into()),
        };

        service.launch(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_actor_requester() {
        let mut service = TestService::new(
            "test_command_test_pipelines_actor_requester",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config("requester");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: Some("ghostflow".into()),
        };

        service.launch(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_actor_custom() {
        let mut service = TestService::new(
            "test_command_test_pipelines_actor_custom",
            vec![
                Action::create_user("builder"),
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config_custom("builder");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: Some("builder".into()),
        };

        service.launch(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_actor_custom_unknown() {
        let mut service = TestService::new(
            "test_command_test_pipelines_actor_custom_unknown",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config_custom("builder");
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `test` command: hosting service error: host error: \
                   missing user by name: builder"
                .into(),
        };

        service.launch(config, end).unwrap()
    }

    #[test]
    fn test_command_test_pipelines_multiple() {
        let mut service = TestService::new(
            "test_command_test_pipelines_multiple",
            vec![
                Action::create_user("builder"),
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Manual, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: test\nDo: test -s stage"),
            ],
        )
        .unwrap();
        let config = test_pipelines_config_custom("builder");
        let end = EndSignal::TriggerJob {
            project: "ghostflow/example".into(),
            job: "job".into(),
            user: Some("builder".into()),
        };

        service.launch(config, end).unwrap()
    }
}
