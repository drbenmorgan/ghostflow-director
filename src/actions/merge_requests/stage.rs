// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use actions::merge_requests::utils;
use actions::merge_requests::{Action, ActionError, Data, Effect};
use config::StageUpdatePolicy;

/// The stage action
///
/// Stages the merge request topic onto the main target branch's stage.
#[derive(Debug, Default, Clone, Copy)]
pub struct Stage;

impl Action for Stage {
    fn help(&self, data: &Data) -> Option<Cow<'static, str>> {
        let action = data.main_branch.stage()?;

        let policy_desc = match action.policy {
            StageUpdatePolicy::Ignore => "left as-is on the stage",
            StageUpdatePolicy::Restage => "automatically restaged",
            StageUpdatePolicy::Unstage => "removed from the stage",
        };

        let msg = format!(
            "Takes the topic from the merge request and adds it to the \"stage\". This is a \
             branch which contains all of the staged topics merged in on top of the target \
             branch. Conflicts between topics on the stage are handled in a \"first-come, \
             first-served\" fashion, so if a topic conflicts with another, waiting until it is \
             either merged into the target branch or removed from the stage is best. When \
             updating a merge request with new code, a topic that has already been staged is {}.",
            policy_desc,
        );

        Some(msg.into())
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let action = if let Some(action) = data.main_branch.stage() {
            action
        } else {
            effects.push(ActionError::not_supported().into());
            return effects;
        };
        if data.action_data.cause.how < action.access_level {
            effects.push(ActionError::insufficient_permissions(action.access_level).into());
            return effects;
        }

        let matches = utils::command_app("stage").get_matches_from_safe(arguments);
        let _ = match matches {
            Ok(matches) => matches,
            Err(err) => {
                let msg = err.message.lines().next().unwrap_or("<unknown error>");
                effects.push(ActionError::unrecognized_arguments(msg).into());
                return effects;
            },
        };

        let (check_effects, check_status) = data.check_status();
        effects.extend(check_effects);
        let check_status = if let Some(check_status) = check_status {
            check_status
        } else {
            return effects;
        };

        let mr_info = &data.info;

        if !check_status.is_checked() {
            effects.push(Effect::error(
                "refusing to stage; topic is missing the checks",
            ));
        } else if !check_status.is_ok() {
            effects.push(Effect::error(
                "refusing to stage; topic is failing the checks",
            ));
        } else {
            let mut stage = action.stage();

            let res = stage.stage_merge_request_named(
                &mr_info.merge_request,
                mr_info.topic_name(),
                &data.action_data.cause.who.identity(),
                data.action_data.cause.when,
            );

            if let Err(err) = res {
                error!(
                    target: "ghostflow-director/handler",
                    "failed during the stage action on {}: {:?}",
                    mr_info.merge_request.url,
                    err,
                );

                let msg = format!(
                    "Error occurred during stage action (@{}): {}",
                    data.action_data.project.maintainers().join(" @"),
                    err,
                );
                effects.push(Effect::error(msg));
            }
        }

        effects
    }
}

#[cfg(test)]
mod tests {
    use handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    fn stage_config() -> serde_json::Value {
        json!({
            "required_access_level": "developer",
        })
    }

    #[test]
    fn test_command_stage_refuse_not_configured() {
        let mut service = TestService::new(
            "test_command_stage_refuse_not_configured",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: stage"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `stage` command: the command is not supported"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_stage_disallowed() {
        let mut service = TestService::new(
            "test_command_stage_disallowed",
            vec![
                Action::create_user("fork"),
                Action::create_user("unrelated"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("unrelated", "Do: stage"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "stage": stage_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `stage` command: permission denied: insufficient access \
                   level (Developer)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_stage_submitter_disallowed() {
        let mut service = TestService::new(
            "test_command_stage_submitter_disallowed",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("fork", "Do: stage"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "stage": stage_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `stage` command: permission denied: insufficient access \
                   level (Developer)"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_command_stage_bad_arguments() {
        let mut service = TestService::new(
            "test_command_stage_bad_arguments",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: stage --unrecognized"),
            ],
        )
        .unwrap();
        let config = json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "stage": stage_config(),
                    },
                },
            },
        });
        let end = EndSignal::MergeRequestComment {
            content: "\
                 Errors:\n\n  \
                 - While processing the `stage` command: unrecognized arguments: error: Found \
                 argument '--unrecognized' which wasn't expected, or isn't valid in this context"
                .into(),
        };

        service.launch(config, end).unwrap();
    }
}
