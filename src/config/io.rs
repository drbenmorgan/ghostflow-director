// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Configuration reader types.
//!
//! This module contains types which represent the configuration of the director.

use std::collections::hash_map::HashMap;
use std::fs::File;
use std::io;
use std::path::{Path, PathBuf};

use crates::serde::{Deserialize, Serialize};
use crates::serde_json::{self, Value};
use crates::serde_yaml;
use crates::thiserror::Error;
use crates::yaml_merge_keys;

use config::checks::{CheckError, Checks};
use config::defaults;

fn empty_object() -> Value {
    Value::Object(Default::default())
}

fn default_data_ref_namespace() -> String {
    "data".into()
}

/// Configuration for handling of data.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Data {
    /// Whether the keep data refs after handling data pushes.
    #[serde(default = "defaults::default_false")]
    pub keep_refs: bool,

    /// The namespace for data references.
    #[serde(default = "default_data_ref_namespace")]
    pub namespace: String,

    /// The destinations for the data.
    pub destinations: Vec<String>,
}

/// Configuration for a check.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct PreCheckConfig {
    /// The kind of check to perform.
    #[serde(default)]
    pub kind: String,
    /// Configuration object for the check `kind`.
    #[serde(default = "empty_object")]
    pub config: Value,
}

/// Configuration for a check.
///
/// If the `kind` is `None`, a check of the same name is masked out (see `Branch::checks`).
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct CheckConfig {
    /// The kind of check to perform.
    ///
    /// Skip to remove the check.
    #[serde(default)]
    pub kind: Option<String>,
    /// Configuration object for the check `kind`.
    #[serde(default = "empty_object")]
    pub config: Value,
}

impl From<PreCheckConfig> for CheckConfig {
    fn from(pre_check: PreCheckConfig) -> Self {
        CheckConfig {
            kind: Some(pre_check.kind),
            config: pre_check.config,
        }
    }
}

fn default_follow_ref_namespace() -> String {
    "follow".into()
}

/// Configuration for the follow action for a branch.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Follow {
    /// The namespace for follow references.
    #[serde(default = "default_follow_ref_namespace")]
    pub ref_namespace: String,
}

/// Trailers to add for a given stage status.
#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum CiReviewAction {
    /// Add an `Acked-by` trailer.
    #[serde(rename = "ack")]
    Ack,
    /// Add an `Reviewed-by` trailer.
    #[serde(rename = "review")]
    Review,
    /// Add an `Tested-by` trailer.
    #[serde(rename = "tested")]
    Tested,
    /// Add an `Rejected-by` trailer.
    #[serde(rename = "reject")]
    Reject,
    /// Ignore the status.
    #[serde(rename = "ignore")]
    Ignore,
}

/// Policies for collecting information from a merge request before merging.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct CiStagePolicy {
    /// The stage to apply the policy to.
    ///
    /// If empty, all stages are considered.
    #[serde(default)]
    pub stages: Vec<String>,
    /// The status to apply if passing.
    #[serde(default)]
    pub passing: Option<CiReviewAction>,
    /// The status to apply if incomplete.
    #[serde(default)]
    pub incomplete: Option<CiReviewAction>,
    /// The status to apply if missing.
    #[serde(default)]
    pub missing: Option<CiReviewAction>,
    /// The status to apply if failing.
    #[serde(default)]
    pub failing: Option<CiReviewAction>,
    /// The reason for the rejection (if rejecting).
    #[serde(default)]
    pub reject_reason: Option<String>,
}

/// Policies for collecting information from a merge request before merging.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct CiPipelinePolicy {
    /// The user to attribute pipeline status trailers to.
    pub ci_user: String,
    /// Stage policy settings.
    ///
    /// Each stage policy is applied in order and the first that contains a match to the complete
    /// status of the pipeline will determine the trailer to use.
    pub stage_policies: Vec<CiStagePolicy>,
}

/// Policies for collecting information from a merge request before merging.
#[derive(Deserialize, Serialize, Debug, Default, Clone)]
pub struct MergePolicy {
    /// The CI pipeline policy.
    #[serde(default)]
    pub ci_pipeline: Option<CiPipelinePolicy>,
}

/// Access level strings.
#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
pub enum AccessLevelIo {
    /// A user external to the project.
    #[serde(rename = "contributor")]
    Contributor,
    /// A user with permission to develop on the project directly.
    #[serde(rename = "developer")]
    Developer,
    /// A user with permission to maintain integration branches on the project.
    #[serde(rename = "maintainer")]
    Maintainer,
    /// A user with permission to administrate the project.
    #[serde(rename = "owner")]
    Owner,
}

/// Merge topology strings.
#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
pub enum MergeTopologyIo {
    /// A user external to the project.
    #[serde(rename = "no_fast_forward")]
    NoFastForward,
    /// A user with permission to develop on the project directly.
    #[serde(rename = "fast_forward_if_possible")]
    FastForwardIfPossible,
    /// A user with permission to maintain integration branches on the project.
    #[serde(rename = "fast_forward_only")]
    FastForwardOnly,
}

impl Default for MergeTopologyIo {
    fn default() -> Self {
        MergeTopologyIo::NoFastForward
    }
}

/// Configuration for the merge action for a branch.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Merge {
    /// If true, only errors and essential comments will be made to the service.
    #[serde(default = "defaults::default_false")]
    pub quiet: bool,
    /// The maximum number of commit summaries to show in the merge commit message.
    ///
    /// Set to `null` for `unlimited` and `0` for no logs.
    pub log_limit: Option<usize>,

    /// The access level required to be able to merge.
    pub required_access_level: AccessLevelIo,

    /// The access level required to perform a fast-forward merge.
    ///
    /// If not provided, fast-forward merges are not allowed.
    #[serde(default)]
    pub required_access_level_ff: Option<AccessLevelIo>,

    /// Merge the branch with a given name.
    #[serde(default)]
    pub named: Option<String>,

    /// The policy to use for merging.
    #[serde(default)]
    pub policy: MergePolicy,

    #[serde(default)]
    /// The topology to use for merges.
    pub merge_topology: MergeTopologyIo,

    /// Whether to elide the branch name or not when merging into it.
    ///
    /// Defaults to `true` for the `master` branch and `false` otherwise.
    #[serde(default)]
    pub elide: Option<bool>,
}

/// Configuration for a formatting tool on a project.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Formatter {
    /// The "kind" of formatter.
    ///
    /// Used for attribute lookup.
    pub kind: String,
    /// The path to the formatter.
    pub formatter: String,

    /// Paths within the repository which contain configuration for the formatter.
    #[serde(default)]
    pub config_files: Vec<String>,

    /// A timeout for running the formatter, in seconds.
    #[serde(default)]
    pub timeout: Option<u64>,
}

/// Configuration for the stage action for a branch.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Reformat {
    /// The access level required to be able to stage.
    pub required_access_level: AccessLevelIo,

    /// The formatters to use for the action.
    pub formatters: Vec<Formatter>,
}

/// Policies for updates to branches which have been merged into the stage.
#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
pub enum StageUpdatePolicy {
    /// Updates should be ignored; the previous merge should be used instead.
    #[serde(rename = "ignore")]
    Ignore,
    /// The updated branch should be added to the stage.
    #[serde(rename = "restage")]
    Restage,
    /// The branch should be removed from the stage when an update appears.
    #[serde(rename = "unstage")]
    Unstage,
}

impl Default for StageUpdatePolicy {
    fn default() -> Self {
        StageUpdatePolicy::Unstage
    }
}

/// Configuration for the stage action for a branch.
#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
pub struct Stage {
    /// If true, only errors and essential comments will be made to the service.
    #[serde(default = "defaults::default_false")]
    pub quiet: bool,

    /// The access level required to be able to stage.
    pub required_access_level: AccessLevelIo,

    /// The update policy for merge requests which receive new code.
    #[serde(default)]
    pub update_policy: StageUpdatePolicy,
}

/// Backends for performing tests for projects.
#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
pub enum TestBackend {
    /// Job files will be created in a directory for test commands.
    #[serde(rename = "jobs")]
    Jobs,
    /// Refs will be pushed to the remote to indicate topics to test.
    #[serde(rename = "refs")]
    Refs,
    /// The hosting service's CI support will be used to trigger testing.
    #[serde(rename = "pipelines")]
    Pipelines,
}

/// Configuration for the `jobs` test backend.
#[derive(Deserialize, Serialize, Debug)]
pub struct TestJobsConfig {
    /// If true, only errors and essential comments will be made to the service.
    #[serde(default = "defaults::default_false")]
    pub quiet: bool,

    /// The directory to drop job files into.
    pub queue: String,

    /// Whether to test updates to the branch itself or not.
    #[serde(default = "defaults::default_true")]
    pub test_updates: bool,

    /// The help string for usage of the backend.
    pub help_string: String,
}

fn default_test_ref_namespace() -> String {
    "test-topics".into()
}

/// Configuration for the `refs` test backend.
#[derive(Deserialize, Serialize, Debug)]
pub struct TestRefsConfig {
    /// If true, only errors and essential comments will be made to the service.
    #[serde(default = "defaults::default_false")]
    pub quiet: bool,

    /// The namespace for test references.
    #[serde(default = "default_test_ref_namespace")]
    pub namespace: String,
}

/// Pipeline actor possibilities.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum TestPipelinesActor {
    /// Start jobs as the director itself.
    ///
    /// Note that any other option may require the director user to have "sudo" privileges on the
    /// associated service.
    #[serde(rename = "self")]
    Self_,
    /// Start jobs as the author of the merge request.
    #[serde(rename = "author")]
    Author,
    /// Start jobs as the requester of the job.
    #[serde(rename = "requester")]
    Requester,
    /// A specific user.
    #[serde(rename = "user")]
    User(String),
}

fn default_test_pipelines_actor() -> TestPipelinesActor {
    TestPipelinesActor::Self_
}

/// Configuration for the `refs` test backend.
#[derive(Deserialize, Serialize, Debug)]
pub struct TestPipelinesConfig {
    /// The actor to trigger pipeline actions as.
    #[serde(default = "default_test_pipelines_actor")]
    pub actor: TestPipelinesActor,
}

/// Configuration for the test action for a branch.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Test {
    /// The access level required to be able to test.
    pub required_access_level: AccessLevelIo,

    /// The backend to use for testing topics for this branch.
    pub backend: TestBackend,

    /// Require explicit requests (ignored for standalone backends).
    #[serde(skip, default = "defaults::default_false")]
    pub explicit_only: bool,

    /// The configuration for the backend.
    #[serde(default = "empty_object")]
    pub config: Value,
}

/// Configuration for the test action for a branch.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SimpleTest {
    /// The backend to use for testing topics for this branch.
    pub backend: TestBackend,

    /// Require explicit requests.
    #[serde(default = "defaults::default_false")]
    pub explicit_only: bool,

    /// The configuration for the backend.
    #[serde(default = "empty_object")]
    pub config: Value,
}

impl SimpleTest {
    /// Create a full `Test` action with a given access level.
    pub fn with_access(self, access: AccessLevelIo) -> Test {
        Test {
            required_access_level: access,
            backend: self.backend,
            explicit_only: self.explicit_only,
            config: self.config,
        }
    }
}

/// The configuration structure for multiple test backends.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct MultipleTests {
    /// The access level required to be able to test.
    pub required_access_level: AccessLevelIo,

    /// The configuration for the multiple backends.
    ///
    /// Commands will be given to all backends unless the first argument is `-X<name>` where
    /// `<name>` is the key of this map.
    ///
    /// Note that the `required_access_level` for each backend will be ignored for the top-level
    /// setting.
    pub backends: HashMap<String, SimpleTest>,
}

/// Configuration for dashboard status actions.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Dashboard {
    /// The template for the status name.
    pub status_name: String,

    /// The template for the target URL.
    pub url: String,

    /// The template for the description of the status.
    pub description: String,
}

/// Configuration for wait-for-pipeline delays.
#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
pub struct WaitForPipeline {
    /// How long to wait after the first miss of a pipeline.
    pub initial_time: f64,
    /// The ratio to extend the deferral on each subsequent miss.
    pub ratio: f64,
    /// The maximum number of times to wait.
    pub max_waits: u64,
}

/// Configuration for a branch within a project.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Branch {
    /// The follow configuration.
    #[serde(default)]
    pub follow: Option<Follow>,
    /// The merge configuration.
    #[serde(default)]
    pub merge: Option<Merge>,
    /// The reformat configuration.
    #[serde(default)]
    pub reformat: Option<Reformat>,
    /// The stage configuration.
    #[serde(default)]
    pub stage: Option<Stage>,
    /// The test configuration.
    #[serde(default)]
    pub test: Option<Test>,
    /// The test configurations (if using multiple backends).
    #[serde(default)]
    pub tests: Option<MultipleTests>,

    /// Pre-check checks.
    ///
    /// These checks should be "fast" so that they can gate the running of other checks which may
    /// be expensive.
    ///
    /// These checks are independent of all other checks for the branch.
    #[serde(default)]
    pub pre_checks: Vec<PreCheckConfig>,

    /// Wait for a pipeline to be created on an MR commit for this branch.
    #[serde(default)]
    pub wait_for_pipeline: Option<WaitForPipeline>,

    /// Branch-specific checks.
    ///
    /// Checks which share the same name as the project override the project's check.
    #[serde(default)]
    pub checks: HashMap<String, CheckConfig>,

    /// Labels to add to issues which have been closed by merged merge requests.
    #[serde(default)]
    pub issue_labels: Vec<String>,

    /// Branches which should always contain this branch.
    #[serde(default)]
    pub into_branches: Vec<String>,
}

fn default_base_check_name() -> String {
    "ghostflow".into()
}

/// Configuration for a project.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Project {
    /// Project-wide pre-checks.
    ///
    /// These checks should be "fast" so that they can gate the running of other checks which may
    /// be expensive.
    #[serde(default)]
    pub pre_checks: Vec<PreCheckConfig>,
    /// Project-wide checks.
    #[serde(default)]
    pub checks: HashMap<String, CheckConfig>,
    /// The data configuration for the project.
    #[serde(default)]
    pub data: Option<Data>,

    /// The dashboard configuration for the project's commit CI.
    #[serde(default)]
    pub commit_dashboards: Vec<Dashboard>,
    /// The dashboard configuration for the project's MR CI.
    #[serde(default)]
    pub merge_request_dashboards: Vec<Dashboard>,

    /// The primary branch for the project.
    ///
    /// If given, sets the `init.defaultBranchName` in the clone of the project.
    pub primary_branch: Option<String>,
    /// The branches which should be watched.
    pub branches: HashMap<String, Branch>,

    /// The submodule git directories for the project.
    #[serde(default)]
    pub submodules: HashMap<String, String>,

    /// Project maintainers.
    #[serde(default)]
    pub maintainers: Vec<String>,

    /// The base name for checks on the project.
    #[serde(default = "default_base_check_name")]
    pub base_check_name: String,

    /// Whether to unprotect source branches of merge requests or not.
    ///
    /// Defaults to the host setting if unprovided.
    #[serde(default)]
    pub unprotect_source_branches: Option<bool>,
}

/// A user which should be added to all projects on the instance.
#[derive(Deserialize, Serialize, Debug)]
pub struct GlobalUser {
    /// The username of the user.
    pub username: String,
    /// The access level the user should be given.
    pub access_level: AccessLevelIo,
}

/// Configuration for a host.
#[derive(Deserialize, Serialize, Debug)]
pub struct Host {
    /// The API the host uses for communication.
    pub host_api: String,
    /// The URL to the host.
    pub host_url: Option<String>,
    /// The list of maintainers on the host to notify when errors occur.
    pub maintainers: Vec<String>,
    /// The path to a JSON file containing private information to contact this host.
    pub secrets_path: String,
    /// The webhook URL to register for new projects.
    pub webhook_url: Option<String>,
    /// The user to add to all projects on the instance.
    pub global_user: Option<GlobalUser>,

    /// The projects to handle on this host.
    ///
    /// All events for other projects are ignored.
    pub projects: HashMap<String, Project>,

    /// Whether to unprotect source branches of merge requests or not.
    #[serde(default = "defaults::default_false")]
    pub unprotect_source_branches: bool,
}

/// Errors for secrets files.
#[derive(Debug, Error)]
pub enum SecretsError {
    /// Failure to open a secrets file.
    #[error("failed to open secrets file `{}`: {}", path, source)]
    Open {
        /// The path to the secrets file.
        path: String,
        /// The source of the error.
        #[source]
        source: io::Error,
    },
    /// Failure to parse a secrets file.
    #[error("failed to parse secrets file `{}`: {}", path, source)]
    Parse {
        /// The path to the secrets file.
        path: String,
        /// The source of the error.
        #[source]
        source: serde_json::Error,
    },
}

impl SecretsError {
    fn open(path: String, source: io::Error) -> Self {
        SecretsError::Open {
            path,
            source,
        }
    }

    fn parse(path: String, source: serde_json::Error) -> Self {
        SecretsError::Parse {
            path,
            source,
        }
    }
}

impl Host {
    /// The secrets object.
    pub fn secrets(&self) -> Result<Value, SecretsError> {
        let fin = File::open(&self.secrets_path)
            .map_err(|err| SecretsError::open(self.secrets_path.clone(), err))?;
        serde_json::from_reader(fin)
            .map_err(|err| SecretsError::parse(self.secrets_path.clone(), err))
    }
}

/// Errors which can occur when reading in a configuration file.
#[derive(Debug, Error)]
pub enum ConfigError {
    /// Failure to open the configuration file.
    #[error("failed to open config file `{}`: {}", path.display(), source)]
    Open {
        /// The path to the configuration file.
        path: PathBuf,
        /// The source of the error.
        #[source]
        source: io::Error,
    },
    /// Failure to parse the file as a YAML document.
    #[error("failed to parse config file `{}`: {}", path.display(), source)]
    Parse {
        /// The path to the configuration file.
        path: PathBuf,
        /// The source of the error.
        #[source]
        source: serde_yaml::Error,
    },
    /// Failure to perform merge key operations on the YAML document.
    #[error("failed to handle merge keys from `{}`: {}", path.display(), source)]
    MergeKeys {
        /// The path to the configuration file.
        path: PathBuf,
        /// The source of the error.
        #[source]
        source: yaml_merge_keys::MergeKeyError,
    },
    /// Failure to deserialize the configuration structure.
    #[error("failed to deserialize config file `{}`: {}", path.display(), source)]
    Deserialize {
        /// The path to the configuration file.
        path: PathBuf,
        /// The source of the error.
        #[source]
        source: serde_yaml::Error,
    },
}

impl ConfigError {
    fn open(path: PathBuf, source: io::Error) -> Self {
        ConfigError::Open {
            path,
            source,
        }
    }

    fn parse(path: PathBuf, source: serde_yaml::Error) -> Self {
        ConfigError::Parse {
            path,
            source,
        }
    }

    fn merge_keys(path: PathBuf, source: yaml_merge_keys::MergeKeyError) -> Self {
        ConfigError::MergeKeys {
            path,
            source,
        }
    }

    fn deserialize(path: PathBuf, source: serde_yaml::Error) -> Self {
        ConfigError::Deserialize {
            path,
            source,
        }
    }
}

/// Main configuration object for the robot.
#[derive(Deserialize, Serialize, Debug)]
pub struct Config {
    /// A scratch space directory for the host.
    pub workdir: String,

    /// Host configuration.
    pub hosts: HashMap<String, Host>,

    archive: Option<String>,
    queue: String,
}

impl Config {
    /// Read the configuration from a path.
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, ConfigError> {
        Self::from_path_impl(path.as_ref())
    }

    fn from_path_impl(path: &Path) -> Result<Self, ConfigError> {
        File::open(path)
            .map_err(|err| ConfigError::open(path.into(), err))
            .and_then(|fin| {
                serde_yaml::from_reader(fin).map_err(|err| ConfigError::parse(path.into(), err))
            })
            .and_then(|doc| {
                yaml_merge_keys::merge_keys_serde(doc)
                    .map_err(|err| ConfigError::merge_keys(path.into(), err))
            })
            .and_then(|doc| {
                serde_yaml::from_value(doc)
                    .map_err(|err| ConfigError::deserialize(path.into(), err))
            })
    }

    /// The directory to read for input data.
    pub fn queue_dir(&self) -> &Path {
        Path::new(&self.queue)
    }

    /// The directory to place processed job files.
    pub fn archive_dir(&self) -> &Path {
        Path::new(self.archive.as_ref().unwrap_or(&self.queue))
    }

    /// Verify all of the check configuration blocks in the configuration file.
    pub fn verify_all_check_configs(self) -> Result<(), CheckError> {
        // Check the checks in each host.
        self.hosts
            .into_iter()
            .map(|(_, host)| {
                // And in each project.
                host.projects
                    .into_iter()
                    .map(|(_, project)| {
                        project
                            .checks
                            .into_iter()
                            .map(|(_, check)| check)
                            .chain(project.pre_checks.into_iter().map(Into::into))
                            .chain(
                                project
                                    .branches
                                    .into_iter()
                                    .map(|(_, branch)| {
                                        branch
                                            .checks
                                            .into_iter()
                                            .map(|(_, check)| check)
                                            .chain(branch.pre_checks.into_iter().map(Into::into))
                                    })
                                    .flatten(),
                            )
                    })
                    .flatten()
            })
            .flatten()
            // Split the check configuration into an owned kind and configuration block.
            .map(|check_config| (check_config.kind.clone(), check_config.config))
            // Parse each check configuration section out.
            .map(|(check_kind, config): (Option<String>, Value)| {
                check_kind.map(|kind| {
                    let mut checks = Checks::default();
                    checks.add_check(&kind, config)
                })
            })
            // Skip any branch checks which are deleting a project-level check.
            .flatten()
            // Collect them up as results.
            .collect::<Result<Vec<_>, _>>()
            // And drop the actual check results.
            .map(|_| ())
    }
}
