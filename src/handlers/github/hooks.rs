// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::{DateTime, Utc};
use crates::serde::Deserialize;

#[derive(Debug, Deserialize)]
pub enum CheckAction {
    #[serde(rename = "created")]
    Created,
    #[serde(rename = "requested")]
    Requested,
    #[serde(rename = "rerequested")]
    Rerequested,
    #[serde(rename = "requested_action")]
    RequestedAction,
    #[serde(rename = "completed")]
    Completed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum CheckStatus {
    #[serde(rename = "requested")]
    Requested,
    #[serde(rename = "queued")]
    Queued,
    #[serde(rename = "in_progress")]
    InProgress,
    #[serde(rename = "completed")]
    Completed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum CheckRunConclusion {
    #[serde(rename = "success")]
    Success,
    #[serde(rename = "failure")]
    Failure,
    #[serde(rename = "neutral")]
    Neutral,
    #[serde(rename = "cancelled")]
    Cancelled,
    #[serde(rename = "timed_out")]
    TimedOut,
    #[serde(rename = "stale")]
    Stale,
    #[serde(rename = "action_required")]
    ActionRequired,
}

#[derive(Debug, Deserialize)]
pub struct CheckRunHookAttrs {
    pub status: CheckStatus,
    pub head_sha: String,
    pub conclusion: Option<CheckRunConclusion>,
    pub name: String,
}

#[derive(Debug, Deserialize)]
pub struct CheckRunRequestedAction {
    pub identifier: String,
}

#[derive(Debug, Deserialize)]
pub struct BranchHookAttrs {
    #[serde(rename = "ref")]
    pub ref_: String,
    pub sha: String,
    pub repo: Option<RepositoryHookAttrs>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum PullRequestState {
    #[serde(rename = "open")]
    Open,
    #[serde(rename = "closed")]
    Closed,
    #[serde(rename = "merged")]
    Merged,
}

#[derive(Debug, Deserialize)]
pub struct PullRequestHookAttrs {
    pub number: u64,
    pub title: String,
    pub body: Option<String>,
    pub head: BranchHookAttrs,
    pub base: BranchHookAttrs,
    pub state: PullRequestState,
    pub merged: Option<bool>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Deserialize)]
pub struct CheckSuiteHookAttrs {
    pub head_branch: Option<String>,
    pub head_sha: String,
    pub status: CheckStatus,
    pub conclusion: Option<CheckRunConclusion>,
    // pub pull_requests: Vec<PullRequestHookAttrs>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum AppAction {
    #[serde(rename = "revoked")]
    Revoked,
    // TODO
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum InstallationAction {
    #[serde(rename = "created")]
    Created,
    #[serde(rename = "deleted")]
    Deleted,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum UserAccountType {
    Bot,
    User,
    Organization,
}

#[derive(Debug, Deserialize)]
pub struct UserHookAttrs {
    pub login: String,
    #[serde(rename = "type")]
    pub type_: UserAccountType,
}

#[derive(Debug, Deserialize)]
pub struct InstallationHookAttrs {
    pub id: u64,
    pub account: UserHookAttrs,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum InstallationRepositoriesAction {
    #[serde(rename = "added")]
    Added,
    #[serde(rename = "removed")]
    Removed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum RepositorySelection {
    #[serde(rename = "selected")]
    Selected,
    #[serde(rename = "all")]
    All,
}

#[derive(Debug, Deserialize)]
pub struct RepositoryHookAttrs {
    pub full_name: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum IssueCommentAction {
    #[serde(rename = "created")]
    Created,
    #[serde(rename = "edited")]
    Edited,
    #[serde(rename = "deleted")]
    Deleted,
}

#[derive(Debug, Deserialize)]
pub struct IssuePullRequestAttrs {
    pub url: String,
}

#[derive(Debug, Deserialize)]
pub struct IssueHookAttrs {
    pub url: String,
    pub number: u64,
    pub pull_request: Option<IssuePullRequestAttrs>,
}

#[derive(Debug, Deserialize)]
pub struct CommentHookAttrs {
    pub node_id: String,
    pub user: UserHookAttrs,
    pub body: String,
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum MemberAction {
    #[serde(rename = "added")]
    Added,
    #[serde(rename = "edited")]
    Edited,
    #[serde(rename = "deleted")]
    Deleted,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum Permission {
    #[serde(rename = "admin")]
    Admin,
    #[serde(rename = "write")]
    Write,
    #[serde(rename = "read")]
    Read,
    #[serde(rename = "maintain")]
    Maintain,
    // TODO
}

#[derive(Debug, Deserialize)]
pub struct MemberPermission {
    pub from: Option<Permission>,
}

#[derive(Debug, Deserialize)]
pub struct MemberChanges {
    pub permission: MemberPermission,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum MembershipAction {
    #[serde(rename = "added")]
    Added,
    #[serde(rename = "removed")]
    Removed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum MembershipScope {
    #[serde(rename = "team")]
    Team,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum TeamPermission {
    #[serde(rename = "admin")]
    Admin,
    #[serde(rename = "pull")]
    Pull,
    #[serde(rename = "push")]
    Push,
}

#[derive(Debug, Deserialize)]
pub struct TeamHookAttrs {
    pub name: String,
    pub permission: TeamPermission,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum OrganizationAction {
    #[serde(rename = "member_added")]
    MemberAdded,
    #[serde(rename = "member_removed")]
    MemberRemoved,
    #[serde(rename = "member_invited")]
    MemberInvited,
    #[serde(rename = "deleted")]
    Deleted,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum MembershipState {
    #[serde(rename = "active")]
    Active,
    #[serde(rename = "pending")]
    Pending,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum MembershipRole {
    #[serde(rename = "member")]
    Member,
    // TODO
}

#[derive(Debug, Deserialize)]
pub struct MembershipHookAttrs {
    pub state: MembershipState,
    pub role: MembershipRole,
    pub user: UserHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct OrganizationHookAttrs {
    pub login: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum PullRequestReviewAction {
    #[serde(rename = "submitted")]
    Submitted,
    #[serde(rename = "edited")]
    Edited,
    #[serde(rename = "dismissed")]
    Dismissed,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum PullRequestReviewState {
    #[serde(rename = "commented")]
    Commented,
    #[serde(rename = "approved")]
    Approved,
    #[serde(rename = "changes_requested")]
    ChangesRequested,
    #[serde(rename = "dismissed")]
    Dismissed,
}

#[derive(Debug, Deserialize)]
pub struct PullRequestReviewHookAttrs {
    pub body: Option<String>,
    pub submitted_at: DateTime<Utc>,
    pub state: PullRequestReviewState,
    pub user: UserHookAttrs,
    pub node_id: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum PullRequestAction {
    #[serde(rename = "assigned")]
    Assigned,
    #[serde(rename = "unassigned")]
    Unassigned,
    #[serde(rename = "converted_to_draft")]
    ConvertedToDraft,
    #[serde(rename = "review_requested")]
    ReviewRequested,
    #[serde(rename = "review_request_removed")]
    ReviewRequestRemoved,
    #[serde(rename = "labeled")]
    Labeled,
    #[serde(rename = "unlabeled")]
    Unlabeled,
    #[serde(rename = "opened")]
    Opened,
    #[serde(rename = "edited")]
    Edited,
    #[serde(rename = "closed")]
    Closed,
    #[serde(rename = "reopened")]
    Reopened,
    #[serde(rename = "synchronize")]
    Synchronize,
    #[serde(rename = "ready_for_review")]
    ReadyForReview,
}

#[derive(Debug, Deserialize)]
pub struct PusherHookAttrs {
    pub name: String,
    pub email: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
pub enum TeamAction {
    #[serde(rename = "created")]
    Created,
    #[serde(rename = "deleted")]
    Deleted,
    #[serde(rename = "edited")]
    Edited,
    #[serde(rename = "added_to_repository")]
    AddedToRepository,
    #[serde(rename = "removed_from_repository")]
    RemovedFromRepository,
}

#[derive(Debug, Deserialize)]
pub struct CheckRunEvent {
    pub action: CheckAction,
    pub check_run: CheckRunHookAttrs,
    pub pull_requests: Option<Vec<PullRequestHookAttrs>>,
    pub requested_action: Option<CheckRunRequestedAction>,
}

#[derive(Debug, Deserialize)]
pub struct CheckSuiteEvent {
    pub action: CheckAction,
    pub check_suite: CheckSuiteHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct GithubAppAuthorizationEvent {
    pub action: AppAction,
}

#[derive(Debug, Deserialize)]
pub struct InstallationEvent {
    pub action: InstallationAction,
    pub installation: InstallationHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct InstallationRepositoriesEvent {
    pub action: InstallationRepositoriesAction,
    pub installation: InstallationHookAttrs,
    pub repository_selection: RepositorySelection,
    pub repositories_added: Vec<RepositoryHookAttrs>,
    pub repositories_removed: Vec<RepositoryHookAttrs>,
}

#[derive(Debug, Deserialize)]
pub struct IssueCommentEvent {
    pub action: IssueCommentAction,
    pub issue: IssueHookAttrs,
    pub comment: CommentHookAttrs,
    pub repository: RepositoryHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct MemberEvent {
    pub action: MemberAction,
    pub member: UserHookAttrs,
    pub changes: Option<MemberChanges>,
    pub repository: RepositoryHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct MembershipEvent {
    pub action: MembershipAction,
    pub scope: MembershipScope,
    pub member: UserHookAttrs,
    pub changes: Option<MemberChanges>,
    pub team: TeamHookAttrs,
    pub organization: OrganizationHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct OrganizationEvent {
    pub action: OrganizationAction,
    pub membership: Option<MembershipHookAttrs>,
    pub organization: OrganizationHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct PullRequestReviewEvent {
    pub action: PullRequestReviewAction,
    pub pull_request: PullRequestHookAttrs,
    pub review: PullRequestReviewHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct PullRequestEvent {
    pub after: Option<String>,
    pub action: PullRequestAction,
    pub pull_request: PullRequestHookAttrs,
    pub number: u64,
}

#[derive(Debug, Deserialize)]
pub struct PushEvent {
    #[serde(rename = "ref")]
    pub ref_: String,
    pub after: String,
    pub repository: RepositoryHookAttrs,
    pub pusher: PusherHookAttrs,
    pub sender: UserHookAttrs,
}

#[derive(Debug, Deserialize)]
pub struct TeamEvent {
    pub action: TeamAction,
    pub team: TeamHookAttrs,
    pub repository: Option<RepositoryHookAttrs>,
    pub organization: Option<OrganizationHookAttrs>,
}

#[derive(Debug, Deserialize)]
pub struct TeamAddEvent {
    pub team: TeamHookAttrs,
    pub repository: RepositoryHookAttrs,
    pub organization: OrganizationHookAttrs,
}
