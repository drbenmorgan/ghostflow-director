// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::Arc;

use crates::ghostflow::host::{self, HostingService, HostingServiceError};
use crates::ghostflow_github::GithubService;
use crates::git_workarea::CommitId;
use crates::graphql_client::GraphQLQuery;
use crates::thiserror::Error;

use ghostflow_ext::{AccessLevel, DirectorHostingService, HostingServiceExt, Membership};
use handlers::github::queries;

#[derive(Debug, Error)]
enum GithubHostError {
    #[error("no repository named {}", project)]
    NoRepository { project: String },
    #[error("no such object {}@{}", project, object)]
    NoObject { object: CommitId, project: String },
    #[error("{}@{} is not a commit", project, object)]
    NotCommit { object: CommitId, project: String },
    #[error("no collaborators on {}", project)]
    NoCollaborators { project: String },
    #[error("no collaborator edges on {}", project)]
    NoCollaboratorEdges { project: String },
    #[error("Github does not support adding members via the API")]
    CannotAddMemberToProject {},
    #[error("Github does not support adding hooks via the API")]
    CannotAddHookToProject {},
}

impl GithubHostError {
    fn no_repository(project: String) -> Self {
        GithubHostError::NoRepository {
            project,
        }
    }

    fn no_object(object: CommitId, project: String) -> Self {
        GithubHostError::NoObject {
            object,
            project,
        }
    }

    fn not_commit(object: CommitId, project: String) -> Self {
        GithubHostError::NotCommit {
            object,
            project,
        }
    }

    fn no_collaborators(project: String) -> Self {
        GithubHostError::NoCollaborators {
            project,
        }
    }

    fn no_collaborator_edges(project: String) -> Self {
        GithubHostError::NoCollaboratorEdges {
            project,
        }
    }

    fn cannot_add_member_to_project() -> Self {
        GithubHostError::CannotAddMemberToProject {}
    }

    fn cannot_add_hook_to_project() -> Self {
        GithubHostError::CannotAddHookToProject {}
    }
}

impl From<GithubHostError> for HostingServiceError {
    fn from(github: GithubHostError) -> Self {
        HostingServiceError::host(github)
    }
}

#[derive(Debug, Error)]
pub enum GithubServiceError {
    #[error("unrecognized award {}", award)]
    UnrecognizedAward { award: String },
}

impl From<GithubServiceError> for HostingServiceError {
    fn from(github: GithubServiceError) -> Self {
        HostingServiceError::service(github)
    }
}

impl HostingServiceExt for GithubService {
    fn as_director_service(self: Arc<Self>) -> Option<Arc<dyn DirectorHostingService>> {
        Some(self as Arc<dyn DirectorHostingService>)
    }
}

impl From<queries::members::RepositoryPermission> for AccessLevel {
    fn from(perms: queries::members::RepositoryPermission) -> Self {
        use handlers::github::queries::members::RepositoryPermission;
        match perms {
            RepositoryPermission::ADMIN => AccessLevel::Owner,
            RepositoryPermission::WRITE | RepositoryPermission::MAINTAIN => AccessLevel::Maintainer,
            RepositoryPermission::READ => AccessLevel::Developer,
            RepositoryPermission::TRIAGE | RepositoryPermission::Other(_) => {
                AccessLevel::Contributor
            },
        }
    }
}

impl From<queries::members::MembersRepositoryCollaboratorsMembersUser> for host::User {
    fn from(user: queries::members::MembersRepositoryCollaboratorsMembersUser) -> Self {
        let queries::members::MembersRepositoryCollaboratorsMembersUser {
            name,
            login,
            // email,
        } = user;

        Self {
            name: name.unwrap_or_else(|| login.clone()),
            // TODO(github-enterprise): What email to use here?
            email: format!("{}@users.noreply.github.com", login),
            handle: login,
        }
    }
}

impl DirectorHostingService for GithubService {
    fn as_hosting_service(self: Arc<Self>) -> Arc<dyn HostingService> {
        self as Arc<dyn HostingService>
    }

    fn members(&self, project: &str) -> Result<Vec<Membership>, HostingServiceError> {
        let (owner, name) = Self::split_project(project)?;

        let mut vars = queries::members::Variables {
            owner: owner.into(),
            name: name.into(),
            cursor: None,
        };

        let mut members = Vec::new();
        loop {
            let query = queries::Members::build_query(vars.clone());
            let page_collaborators = self
                .github()
                .send::<queries::Members>(owner, &query)
                .map_err(HostingServiceError::host)
                .and_then(|rsp| {
                    Self::check_rate_limits(
                        &rsp.rate_limit_info.rate_limit,
                        queries::Members::name(),
                    );
                    Ok(rsp
                        .repository
                        .ok_or_else(|| GithubHostError::no_repository(project.into()))?)
                })
                .and_then(|rsp| {
                    Ok(rsp
                        .collaborators
                        .ok_or_else(|| GithubHostError::no_collaborators(project.into()))?)
                })?;
            let (collaborators, page_info) = (
                page_collaborators
                    .members
                    .ok_or_else(|| GithubHostError::no_collaborator_edges(project.into()))?,
                page_collaborators.page_info,
            );

            members.extend(collaborators.into_iter().filter_map(|collaborator| {
                collaborator.map(|collaborator| {
                    Membership {
                        user: collaborator.user.into(),
                        access_level: collaborator.permission.into(),
                        expiration: None,
                    }
                })
            }));

            if page_info.has_next_page {
                // XXX: We are assuming that if `has_next_page` is `true` that we'll have an
                // `end_cursor`.
                assert!(
                    page_info.end_cursor.is_some(),
                    "GitHub gave us a new page without a cursor to follow.",
                );
                vars.cursor = page_info.end_cursor;
            } else {
                break;
            }
        }

        Ok(members)
    }

    fn add_member(
        &self,
        _: &str,
        _: &host::User,
        _: AccessLevel,
    ) -> Result<(), HostingServiceError> {
        // XXX(github): Github only supports inviting users.
        Err(GithubHostError::cannot_add_member_to_project().into())
    }

    fn add_hook(&self, _: &str, _: &str) -> Result<(), HostingServiceError> {
        // XXX(github): Github only supports "integrations".
        Err(GithubHostError::cannot_add_hook_to_project().into())
    }

    fn post_commit_comment(
        &self,
        commit: &host::Commit,
        content: &str,
    ) -> Result<(), HostingServiceError> {
        let project = &commit.repo.name;
        let oid = &commit.id;
        let (owner, name) = Self::split_project(project)?;

        let vars = queries::commit_id::Variables {
            owner: owner.into(),
            name: name.into(),
            commit: oid.as_str().into(),
        };
        let query = queries::CommitID::build_query(vars);
        let comment_id = self
            .github()
            .send::<queries::CommitID>(owner, &query)
            .map_err(HostingServiceError::host)
            .and_then(|rsp| {
                Self::check_rate_limits(&rsp.rate_limit_info.rate_limit, queries::CommitID::name());
                Ok(rsp
                    .repository
                    .ok_or_else(|| GithubHostError::no_repository(project.clone()))?)
            })
            .and_then(|rsp| {
                Ok(rsp
                    .object
                    .ok_or_else(|| GithubHostError::no_object(oid.clone(), project.clone()))?)
            })
            .and_then(|rsp| {
                use handlers::github::queries::commit_id::CommitIdRepositoryObjectOn;
                if let CommitIdRepositoryObjectOn::Commit(commit) = rsp.on {
                    Ok(commit.id)
                } else {
                    Err(GithubHostError::not_commit(oid.clone(), project.clone()).into())
                }
            })?;

        self.post_comment(owner, comment_id, content)
    }

    fn get_mr_comment_awards(
        &self,
        _: &host::MergeRequest,
        _: &host::Comment,
    ) -> Result<Vec<host::Award>, HostingServiceError> {
        // XXX(github): Github Apps need user permissions to crate a reaction.
        // Because of this, there's no other reason to fetch comment awards, so just do nothing
        // instead.

        Ok(Vec::new())
    }

    fn award_mr_comment(
        &self,
        _: &host::MergeRequest,
        _: &host::Comment,
        _: &str,
    ) -> Result<(), HostingServiceError> {
        // XXX(github): Github Apps need user permissions to crate a reaction.

        Ok(())
    }

    fn comment_award_name(&self) -> &str {
        "hooray"
    }
}
