// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Job data
//!
//! These structures are used to support common jobs that all hosts may use.

use std::collections::hash_map::{self, HashMap};
use std::vec;

use crates::ghostflow::actions::stage::TagStagePolicy;
use crates::serde::Deserialize;

fn default_stage_nightly() -> String {
    "%Y/%m/%d".into()
}

fn default_follow_name() -> String {
    "nightly".into()
}

#[derive(Deserialize, Debug, Clone, Copy, PartialEq, Eq)]
#[doc(hidden)]
pub enum TagStagePolicyIo {
    #[serde(rename = "keep_topics")]
    KeepTopics,
    #[serde(rename = "clear_stage")]
    ClearStage,
}

impl Default for TagStagePolicyIo {
    fn default() -> Self {
        TagStagePolicyIo::ClearStage
    }
}

impl From<TagStagePolicyIo> for TagStagePolicy {
    fn from(policy: TagStagePolicyIo) -> Self {
        match policy {
            TagStagePolicyIo::KeepTopics => TagStagePolicy::KeepTopics,
            TagStagePolicyIo::ClearStage => TagStagePolicy::ClearStage,
        }
    }
}

/// Data required to tag a staging branch.
#[derive(Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct TagStage {
    /// A datetime format string to use for the refname of the tagged stage.
    #[serde(default = "default_stage_nightly")]
    pub ref_date_format: String,
    /// The reason we are tagging the stage.
    pub reason: String,
    /// If false, the stage will be cleared every time the stage branch is published.
    #[serde(default)]
    pub policy: TagStagePolicyIo,
}

/// Data required to clear test refs for a project.
#[derive(Deserialize, Debug, Default, Clone, Copy, PartialEq, Eq)]
pub struct ClearTestRefs {}

/// Update any follow refs.
#[derive(Deserialize, Debug, Clone, PartialEq, Eq)]
pub struct UpdateFollowRefs {
    /// A datetime format string to use for the refname of the tagged stage.
    #[serde(default = "default_follow_name")]
    pub name: String,
}

/// Data required to reset failed projects.
#[derive(Deserialize, Debug, Default, Clone, Copy, PartialEq, Eq)]
pub struct ResetFailedProjects {}

/// Data required to run batch jobs on branches.
#[derive(Deserialize, Debug, Clone)]
struct BatchProject<T>(HashMap<String, T>);

/// Data required to run batch jobs on many branches of projects.
#[derive(Deserialize, Debug, Clone)]
pub struct BatchBranchJob<T>(HashMap<String, BatchProject<T>>);

// TODO: Use `impl Iterator<Item=(String, String, T)>` here.
impl<T> BatchBranchJob<T> {
    /// Iterate over all of the branches.
    pub fn into_iter_branch(self) -> vec::IntoIter<(String, String, T)> {
        self.0
            .into_iter()
            .map(|(project_name, batch_project)| {
                batch_project
                    .0
                    .into_iter()
                    .map(|(branch_name, data)| (project_name.clone(), branch_name, data))
                    .collect::<Vec<_>>()
                    .into_iter()
            })
            .flatten()
            .collect::<Vec<_>>()
            .into_iter()
    }
}

/// Data required to run batch jobs on projects.
#[derive(Deserialize, Debug, Clone)]
pub struct BatchProjectJob<T>(HashMap<String, T>);

// TODO: Use `impl Iterator<Item=(String, T)>` here.
impl<T> BatchProjectJob<T> {
    /// Iterate over all of the projects.
    pub fn into_iter_project(self) -> hash_map::IntoIter<String, T> {
        self.0.into_iter()
    }
}

#[cfg(test)]
mod test {
    use crates::serde_json;

    use handlers::common::jobs::{BatchBranchJob, BatchProjectJob};
    use handlers::common::jobs::{
        ClearTestRefs, ResetFailedProjects, TagStage, TagStagePolicyIo, UpdateFollowRefs,
    };

    #[test]
    fn test_deserialize_tag_stage() {
        let reason = "testing";
        let data = json!({
            "reason": reason,
        });
        let deser = serde_json::from_value::<TagStage>(data).unwrap();
        assert_eq!(deser.ref_date_format, "%Y/%m/%d");
        assert_eq!(deser.reason, reason);
        assert_eq!(deser.policy, TagStagePolicyIo::ClearStage);

        let data = json!({
            "reason": reason,
            "policy": "keep_topics",
        });
        let deser = serde_json::from_value::<TagStage>(data).unwrap();
        assert_eq!(deser.ref_date_format, "%Y/%m/%d");
        assert_eq!(deser.reason, reason);
        assert_eq!(deser.policy, TagStagePolicyIo::KeepTopics);

        let data = json!({
            "reason": reason,
            "policy": "clear_stage",
        });
        let deser = serde_json::from_value::<TagStage>(data).unwrap();
        assert_eq!(deser.ref_date_format, "%Y/%m/%d");
        assert_eq!(deser.reason, reason);
        assert_eq!(deser.policy, TagStagePolicyIo::ClearStage);

        let data = json!({
            "ref_date_format": "custom",
            "reason": reason,
        });
        let deser = serde_json::from_value::<TagStage>(data).unwrap();
        assert_eq!(deser.ref_date_format, "custom");
        assert_eq!(deser.reason, reason);
        assert_eq!(deser.policy, TagStagePolicyIo::ClearStage);
    }

    #[test]
    fn test_deserialize_update_follow_refs() {
        let data = json!({});
        let deser = serde_json::from_value::<UpdateFollowRefs>(data).unwrap();
        assert_eq!(deser.name, "nightly");

        let data = json!({
            "name": "follow",
        });
        let deser = serde_json::from_value::<UpdateFollowRefs>(data).unwrap();
        assert_eq!(deser.name, "follow");
    }

    #[test]
    fn test_deserialize_clear_test_refs() {
        let data = json!({});
        serde_json::from_value::<ClearTestRefs>(data).unwrap();
    }

    #[test]
    fn test_deserialize_reset_failed_projects() {
        let data = json!({});
        serde_json::from_value::<ResetFailedProjects>(data).unwrap();
    }

    #[test]
    fn test_deserialize_batch_project_job() {
        let data = json!({});
        let deser = serde_json::from_value::<BatchProjectJob<ClearTestRefs>>(data)
            .unwrap()
            .0;
        assert!(deser.is_empty());

        let data = json!({
            "project1": {},
            "project2": {},
        });
        let deser = serde_json::from_value::<BatchProjectJob<ClearTestRefs>>(data)
            .unwrap()
            .0;
        assert_eq!(deser.len(), 2);
        deser.get("project1").unwrap();
        deser.get("project2").unwrap();
    }

    #[test]
    fn test_deserialize_batch_branch_job() {
        let data = json!({});
        let deser = serde_json::from_value::<BatchBranchJob<ClearTestRefs>>(data)
            .unwrap()
            .0;
        assert!(deser.is_empty());

        let data = json!({
            "project": {
                "branch1": {},
                "branch2": {},
            },
        });
        let deser = serde_json::from_value::<BatchBranchJob<ClearTestRefs>>(data)
            .unwrap()
            .0;
        assert_eq!(deser.len(), 1);
        let project = &deser.get("project").unwrap().0;
        assert_eq!(project.len(), 2);
        project.get("branch1").unwrap();
        project.get("branch2").unwrap();
    }
}
