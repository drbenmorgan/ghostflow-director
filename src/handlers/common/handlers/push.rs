// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::json_job_dispatch::HandlerResult;
use crates::serde_json::Value;

use config::{Host, TestBackend};
use handlers::common::data::PushInfo;
use handlers::common::handlers::utils;

struct PushResult;

impl PushResult {
    fn non_branch(refname: &str) -> HandlerResult {
        HandlerResult::reject(format!("non-branch ref push {}", refname))
    }

    fn unknown_ref(refname: &Option<String>) -> HandlerResult {
        HandlerResult::reject(format!("unknown refname {:?}", refname))
    }
}

/// Handle a push to a repository.
pub fn handle_push(data: &Value, host: &Host, push: &PushInfo) -> HandlerResult {
    let mut errors = Vec::new();

    if let Ok(root_project) = utils::get_project(host, &push.commit.repo.fork_root().name) {
        if let Some(ref data) = root_project.data {
            if let Err(err) = data.fetch_data(&push.commit.repo) {
                errors.push(format!(
                    "failed to fetch data from {}: {}",
                    push.commit.repo.url, err,
                ));
                error!(
                    target: "ghostflow-director/handler",
                    "failed to fetch data from {}: {:?}",
                    push.commit.repo.url, err,
                );
            }
        }
    }

    let refs_heads_prefix = "refs/heads/";
    let refname = if let Some(ref name) = push.commit.refname {
        if name.starts_with(refs_heads_prefix) {
            name
        } else {
            return PushResult::non_branch(name);
        }
    } else {
        return PushResult::unknown_ref(&push.commit.refname);
    };

    let branch_name = &refname[refs_heads_prefix.len()..];
    let (project, branch) =
        try_action!(utils::get_branch(host, &push.commit.repo.name, branch_name));

    // Garbage collect the repository.
    let gc = project.context.git().arg("gc").arg("--auto").output();
    match gc {
        Ok(gc) => {
            if !gc.status.success() {
                error!(
                    target: "ghostflow-director/handler",
                    "failed to perform garbage collection for {}: {:?}",
                    push.commit.repo.name, String::from_utf8_lossy(&gc.stderr),
                );
            }
        },
        Err(err) => {
            error!(
                target: "ghostflow-director/handler",
                "failed to construct gc command: {:?}",
                err,
            );
        },
    }

    if let Some(test_action) = branch.test() {
        if test_action.test_updates {
            if let TestBackend::Jobs {
                action: ref test_jobs,
                ..
            } = *test_action.test()
            {
                let job_data = utils::test_job("branch_push", data);
                if let Err(err) = test_jobs.test_update(job_data) {
                    error!(
                        target: "ghostflow-director/handler",
                        "failed to drop a job file for an update to the {} branch on {}: {:?}",
                        branch.name, push.commit.repo.name, err,
                    );
                }
            }
        }
    }

    let fetch_res = project.context.force_fetch_into("origin", refname, refname);
    if let Err(err) = fetch_res {
        error!(
            target: "ghostflow-director/handler",
            "failed to fetch from the {} repository: {:?}",
            push.commit.repo.url, err,
        );

        errors.push(format!("Failed to fetch from the repository: {}.", err));
    }

    if let Some(stage_action) = branch.stage() {
        let mut stage = stage_action.stage();
        let res = stage.base_branch_update(&push.commit, &push.author.identity(), push.date);

        if let Err(err) = res {
            error!(
                target: "ghostflow-director/handler",
                "failed update the base branch for the {} stage in the {} repository: {:?}",
                branch.name, push.commit.repo.url, err,
            );

            errors.push(format!(
                "Error occurred when updating the base branch (@{}): {}.",
                host.maintainers.join(" @"),
                err,
            ));
        }
    }

    let dashboards = project.commit_dashboards();
    if !dashboards.is_empty() {
        // Check that the referenced commit is actually a valid commit.
        let is_commit = {
            let cat_file_res = project
                .context
                .git()
                .arg("cat-file")
                .arg("-t")
                .arg(push.commit.id.as_str())
                .output();
            match cat_file_res {
                Ok(output) => {
                    output.status.success()
                        && String::from_utf8_lossy(&output.stdout).trim() == "commit"
                },
                Err(err) => {
                    error!(
                        target: "ghostflow-director/handler",
                        "failed to determine if {} is a commit in the {} repository: {:?}",
                        push.commit.id, push.commit.repo.url, err,
                    );

                    // Just assume it isn't; dashboard statuses aren't vital.
                    false
                },
            }
        };

        if is_commit {
            dashboards.iter().for_each(|dashboard| {
                if let Err(err) = dashboard.post_for_commit(&push.commit) {
                    error!(
                        target: "ghostflow-director/handler",
                        "failed to post dashboard status for the {} repository on commit {}: {:?}",
                        push.commit.repo.url, push.commit.id, err,
                    );

                    // Not an error worth posting about.
                }
            });
        }
    }

    utils::handle_result(&[], &errors, false, |comment| {
        host.service.post_commit_comment(&push.commit, comment)
    })
}

#[cfg(test)]
mod tests {
    use crates::git_workarea::CommitId;
    use crates::serde_json::Value;

    use handlers::test::*;

    const BRANCH_NEXT: &str = "27485b5013bf65f5bccc17de921f67c069d43423";

    fn test_dashboard_config() -> Value {
        json!({
            "ghostflow/example": {
                "commit_dashboards": [
                    {
                        "status_name": "dashboard-for-{branch_name}",
                        "description": "Dashboard for {branch_name}",
                        "url": "https://example.com/{commit}",
                    },
                    {
                        "status_name": "other-dashboard-for-{branch_name}",
                        "description": "Other dashboard for {branch_name}",
                        "url": "https://example.org/{commit}",
                    },
                ],
                "branches": {
                    "master": {},
                    "deleted-ref": {},
                    "next": {},
                },
            },
        })
    }

    #[test]
    fn test_push_dashboard_status() {
        let mut service = TestService::new(
            "test_push_dashboard_status",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
            ],
        )
        .unwrap();
        let config = test_dashboard_config();
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(BRANCH_NEXT),
            name: "dashboard-for-next".into(),
            state: CommitStatusState::Success,
            description: Some("Dashboard for next".into()),
            target_url: Some(format!("https://example.com/{}", BRANCH_NEXT)),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_push_dashboard_status_all() {
        let mut service = TestService::new(
            "test_push_dashboard_status_all",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
            ],
        )
        .unwrap();
        let config = test_dashboard_config();
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(BRANCH_NEXT),
            name: "other-dashboard-for-next".into(),
            state: CommitStatusState::Success,
            description: Some("Other dashboard for next".into()),
            target_url: Some(format!("https://example.org/{}", BRANCH_NEXT)),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_push_dashboard_status_delete_ref() {
        let mut service = TestService::new(
            "test_push_dashboard_status_delete_ref",
            vec![
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("ghostflow", "example", "deleted-ref", BRANCH_NEXT),
                Action::delete_ref("ghostflow", "example", "deleted-ref"),
                // This is just used as a signal to end the test.
                Action::push("ghostflow", "example", "next", BRANCH_NEXT),
            ],
        )
        .unwrap();
        let config = test_dashboard_config();
        let end = EndSignal::CommitStatus {
            commit: CommitId::new(BRANCH_NEXT),
            name: "dashboard-for-next".into(),
            state: CommitStatusState::Success,
            description: Some("Dashboard for next".into()),
            target_url: Some(format!("https://example.com/{}", BRANCH_NEXT)),
        };

        service.launch(config, end).unwrap();
    }
}
