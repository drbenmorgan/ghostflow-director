// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::json_job_dispatch::HandlerResult;
use crates::serde_json::Value;
use crates::thiserror::Error;

use config::{Branch, Host, TestBackend};
use handlers::common::handlers::utils;
use handlers::common::jobs::{BatchBranchJob, ClearTestRefs, TagStage, UpdateFollowRefs};

struct ScannerResult;

impl ScannerResult {
    fn no_action(project: &str, branch: &str, action: &'static str) -> HandlerResult {
        HandlerResult::reject(format!("no action {} for {}/{}", action, project, branch))
    }
}

/// Scan all branches detecting which support an action.
fn branch_job_scanner<F, T>(
    host: &Host,
    data: BatchBranchJob<T>,
    action_name: &'static str,
    handler: F,
) -> HandlerResult
where
    F: Fn(T, &Branch) -> Option<HandlerResult>,
{
    data.into_iter_branch()
        .map(|(project_name, branch_name, job_data)| {
            utils::get_branch(host, &project_name, &branch_name)
                .map(|(_, branch)| (project_name, branch_name, handler(job_data, branch)))
        })
        .fold(HandlerResult::Accept, |result, branch_result| {
            let branch_handler_result = match branch_result {
                Ok((project, branch, action_result)) => {
                    action_result
                        .unwrap_or_else(|| ScannerResult::no_action(&project, &branch, action_name))
                },
                Err(err) => err,
            };

            result.combine(branch_handler_result)
        })
}

/// Handle requests to tag staging branches.
pub fn handle_stage_tag(_: &Value, host: &Host, data: BatchBranchJob<TagStage>) -> HandlerResult {
    branch_job_scanner(host, data, "stage", |tag_stage, branch| {
        branch.stage().map(|stage| {
            stage
                .stage()
                .tag_stage(
                    &tag_stage.reason,
                    &tag_stage.ref_date_format,
                    tag_stage.policy.into(),
                )
                .map(|()| HandlerResult::Accept)
                .unwrap_or_else(HandlerResult::fail)
        })
    })
}

#[derive(Debug, Error)]
enum HandleClearTestRefsError {
    #[error("non-refs test backend")]
    NonRefsTestBackend {},
}

struct HandleClearTestRefsResult;

impl HandleClearTestRefsResult {
    fn non_refs_test_backend() -> HandlerResult {
        HandlerResult::fail(HandleClearTestRefsError::NonRefsTestBackend {})
    }
}

/// Handle requests to clear test refs.
pub fn handle_clear_test_refs(
    _: &Value,
    host: &Host,
    data: BatchBranchJob<ClearTestRefs>,
) -> HandlerResult {
    branch_job_scanner(host, data, "test", |_, branch| {
        branch.test().map(|test| {
            if let TestBackend::Refs(ref test_refs) = *test.test() {
                test_refs
                    .clear_all_mrs()
                    .map(|()| HandlerResult::Accept)
                    .unwrap_or_else(HandlerResult::fail)
            } else {
                HandleClearTestRefsResult::non_refs_test_backend()
            }
        })
    })
}

/// Handle requests to update follow refs.
pub fn handle_update_follow_refs(
    _: &Value,
    host: &Host,
    data: BatchBranchJob<UpdateFollowRefs>,
) -> HandlerResult {
    branch_job_scanner(host, data, "follow", |update_follow, branch| {
        branch.follow().map(|follow| {
            follow
                .follow()
                .update(&update_follow.name)
                .map(|()| HandlerResult::Accept)
                .unwrap_or_else(HandlerResult::fail)
        })
    })
}
