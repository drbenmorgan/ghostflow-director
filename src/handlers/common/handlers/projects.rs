// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::ghostflow::host::{HostingServiceError, Repo};
use crates::json_job_dispatch::HandlerResult;
use crates::serde_json::Value;
use crates::thiserror::Error;

use config::Host;

#[derive(Debug, Error)]
enum ProjectCreationError {
    #[error("failed to add webhook to {}: {}", project, source)]
    AddWebHook {
        project: String,
        #[source]
        source: HostingServiceError,
    },
    #[error("failed to add member to {}: {}", project, source)]
    AddMember {
        project: String,
        #[source]
        source: HostingServiceError,
    },
}

struct ProjectCreationResult;

impl ProjectCreationResult {
    fn add_webhook(project: String, source: HostingServiceError) -> HandlerResult {
        HandlerResult::fail(ProjectCreationError::AddWebHook {
            project,
            source,
        })
    }

    fn add_member(project: String, source: HostingServiceError) -> HandlerResult {
        HandlerResult::fail(ProjectCreationError::AddMember {
            project,
            source,
        })
    }

    fn nothing_to_do() -> HandlerResult {
        HandlerResult::reject("nothing to do")
    }
}

/// Handle the creation of a project on the service.
pub fn handle_project_creation(_: &Value, host: &Host, project: &Repo) -> HandlerResult {
    let webhook_result = host.webhook_url.as_ref().map(|url| {
        host.service
            .add_hook(&project.name, url)
            .map(|()| HandlerResult::Accept)
            .unwrap_or_else(|err| ProjectCreationResult::add_webhook(project.name.clone(), err))
    });

    let membership_result = host.global_user.as_ref().map(|(user, access_level)| {
        host.service
            .add_member(&project.name, user, *access_level)
            .map(|()| HandlerResult::Accept)
            .unwrap_or_else(|err| ProjectCreationResult::add_member(project.name.clone(), err))
    });

    let all_results = vec![webhook_result, membership_result];

    all_results
        .into_iter()
        .flatten()
        .reduce(HandlerResult::combine)
        .unwrap_or_else(ProjectCreationResult::nothing_to_do)
}
