// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

pub struct Pairings<I>
where
    I: Iterator,
{
    first: I,
    second: I,
    store: Option<I::Item>,
}

impl<I> Pairings<I>
where
    I: Iterator + Clone,
{
    pub fn new(mut inner: I) -> Self {
        let store = inner.next();
        Self {
            first: inner.clone(),
            second: inner,
            store,
        }
    }
}

impl<I> Iterator for Pairings<I>
where
    I: Iterator + Clone,
    I::Item: Clone,
{
    type Item = (I::Item, I::Item);

    fn size_hint(&self) -> (usize, Option<usize>) {
        // Get the size hints from each iterator.
        let first_size = self.first.size_hint();
        let second_size = self.second.size_hint();

        // Calculate the lower bound.
        let lower = {
            // Amount remaining on the current stored item.
            let remaining_cur = if self.store.is_some() {
                second_size.0
            } else {
                0
            };
            // Amount remaining for the rest of the first.
            let remaining_next = {
                let n = first_size.0;
                n.saturating_sub(1) * n / 2
            };
            remaining_cur + remaining_next
        };

        // Calculate the upper bound.
        let upper = {
            // Amount remaining on the current stored item.
            let remaining_cur = if self.store.is_some() {
                second_size.1
            } else {
                second_size.1.map(|_| 0)
            };
            // Amount remaining for the rest of the first.
            let remaining_next = first_size.1.map(|n| n.saturating_sub(1) * n / 2);
            remaining_cur.and_then(|c| remaining_next.map(|n| c + n))
        };

        (lower, upper)
    }

    fn next(&mut self) -> Option<Self::Item> {
        // Update the store if needed.
        if self.store.is_none() {
            self.store = self.first.next();
        }
        // Bail if we have nothing stored.
        let _ = self.store.as_ref()?;

        // Get the second item.
        let snd = if let Some(snd) = self.second.next() {
            Some(snd)
        } else {
            // Store the next item in the main iterator for the first component.
            self.store = self.first.next();
            // Clone the first iterator for the second element.
            self.second = self.first.clone();
            // Get the next element.
            self.second.next()
        };

        // Return what we have.
        snd.and_then(|snd| self.store.clone().map(|store| (store, snd)))
    }
}

impl<I> ExactSizeIterator for Pairings<I>
where
    I: ExactSizeIterator + Clone,
    I::Item: Clone,
{
}

#[cfg(test)]
mod tests {
    use handlers::common::handlers::notes::pairings::Pairings;

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    enum Pair {
        First,
        Second,
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    enum Triad {
        First,
        Second,
        Third,
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    enum Erase {
        Lower,
        Upper,
        Both,
    }

    #[derive(Debug, Clone, Copy)]
    struct EraseSizeHintIterator<I> {
        inner: I,
        what: Erase,
    }

    impl<I> EraseSizeHintIterator<I> {
        fn erase_lower(inner: I) -> Self {
            Self {
                inner,
                what: Erase::Lower,
            }
        }

        fn erase_upper(inner: I) -> Self {
            Self {
                inner,
                what: Erase::Upper,
            }
        }

        fn erase_both(inner: I) -> Self {
            Self {
                inner,
                what: Erase::Both,
            }
        }
    }

    impl<I> Iterator for EraseSizeHintIterator<I>
    where
        I: Iterator,
    {
        type Item = I::Item;

        fn size_hint(&self) -> (usize, Option<usize>) {
            let size_hint = self.inner.size_hint();

            match self.what {
                Erase::Lower => (0, size_hint.1),
                Erase::Upper => (size_hint.0, None),
                Erase::Both => (0, None),
            }
        }

        fn next(&mut self) -> Option<Self::Item> {
            self.inner.next()
        }
    }

    #[test]
    fn test_pairings_empty() {
        let empty: &[()] = &[];
        let mut empty_pairings = Pairings::new(empty.iter());
        assert_eq!(empty_pairings.size_hint(), (0, Some(0)));
        assert_eq!(empty_pairings.len(), 0);
        assert_eq!(empty_pairings.next(), None);
        assert_eq!(empty_pairings.size_hint(), (0, Some(0)));
        assert_eq!(empty_pairings.len(), 0);
    }

    #[test]
    fn test_pairings_single() {
        let single = &[()];
        let mut single_pairings = Pairings::new(single.iter());
        assert_eq!(single_pairings.size_hint(), (0, Some(0)));
        assert_eq!(single_pairings.len(), 0);
        assert_eq!(single_pairings.next(), None);
        assert_eq!(single_pairings.size_hint(), (0, Some(0)));
        assert_eq!(single_pairings.len(), 0);
    }

    #[test]
    fn test_pairings_two() {
        let pair = &[Pair::First, Pair::Second];
        let mut pair_pairings = Pairings::new(pair.iter());
        assert_eq!(pair_pairings.size_hint(), (1, Some(1)));
        assert_eq!(pair_pairings.len(), 1);
        assert_eq!(pair_pairings.next(), Some((&Pair::First, &Pair::Second)));
        assert_eq!(pair_pairings.size_hint(), (0, Some(0)));
        assert_eq!(pair_pairings.len(), 0);
        assert_eq!(pair_pairings.next(), None);
        assert_eq!(pair_pairings.size_hint(), (0, Some(0)));
        assert_eq!(pair_pairings.len(), 0);
    }

    #[test]
    fn test_pairings_three() {
        let triad = &[Triad::First, Triad::Second, Triad::Third];
        let mut triad_pairings = Pairings::new(triad.iter());
        assert_eq!(triad_pairings.size_hint(), (3, Some(3)));
        assert_eq!(triad_pairings.len(), 3);
        assert_eq!(triad_pairings.next(), Some((&Triad::First, &Triad::Second)));
        assert_eq!(triad_pairings.size_hint(), (2, Some(2)));
        assert_eq!(triad_pairings.len(), 2);
        assert_eq!(triad_pairings.next(), Some((&Triad::First, &Triad::Third)));
        assert_eq!(triad_pairings.size_hint(), (1, Some(1)));
        assert_eq!(triad_pairings.len(), 1);
        assert_eq!(triad_pairings.next(), Some((&Triad::Second, &Triad::Third)));
        assert_eq!(triad_pairings.size_hint(), (0, Some(0)));
        assert_eq!(triad_pairings.len(), 0);
        assert_eq!(triad_pairings.next(), None);
        assert_eq!(triad_pairings.size_hint(), (0, Some(0)));
        assert_eq!(triad_pairings.len(), 0);
    }

    #[test]
    fn test_unknown_length() {
        let single = &[()];
        let mut single_pairings = Pairings::new(EraseSizeHintIterator::erase_both(single.iter()));
        assert_eq!(single_pairings.size_hint(), (0, None));
        assert_eq!(single_pairings.next(), None);
        assert_eq!(single_pairings.size_hint(), (0, None));

        let pair = &[Pair::First, Pair::Second];
        let mut pair_pairings = Pairings::new(EraseSizeHintIterator::erase_both(pair.iter()));
        assert_eq!(pair_pairings.size_hint(), (0, None));
        assert_eq!(pair_pairings.next(), Some((&Pair::First, &Pair::Second)));
        assert_eq!(pair_pairings.size_hint(), (0, None));
        assert_eq!(pair_pairings.next(), None);
        assert_eq!(pair_pairings.size_hint(), (0, None));
    }

    #[test]
    fn test_unknown_lower_length() {
        let single = &[()];
        let mut single_pairings = Pairings::new(EraseSizeHintIterator::erase_lower(single.iter()));
        assert_eq!(single_pairings.size_hint(), (0, Some(0)));
        assert_eq!(single_pairings.next(), None);
        assert_eq!(single_pairings.size_hint(), (0, Some(0)));

        let pair = &[Pair::First, Pair::Second];
        let mut pair_pairings = Pairings::new(EraseSizeHintIterator::erase_lower(pair.iter()));
        assert_eq!(pair_pairings.size_hint(), (0, Some(1)));
        assert_eq!(pair_pairings.next(), Some((&Pair::First, &Pair::Second)));
        assert_eq!(pair_pairings.size_hint(), (0, Some(0)));
        assert_eq!(pair_pairings.next(), None);
        assert_eq!(pair_pairings.size_hint(), (0, Some(0)));
    }

    #[test]
    fn test_unknown_upper_length() {
        let single = &[()];
        let mut single_pairings = Pairings::new(EraseSizeHintIterator::erase_upper(single.iter()));
        assert_eq!(single_pairings.size_hint(), (0, None));
        assert_eq!(single_pairings.next(), None);
        assert_eq!(single_pairings.size_hint(), (0, None));

        let pair = &[Pair::First, Pair::Second];
        let mut pair_pairings = Pairings::new(EraseSizeHintIterator::erase_upper(pair.iter()));
        assert_eq!(pair_pairings.size_hint(), (1, None));
        assert_eq!(pair_pairings.next(), Some((&Pair::First, &Pair::Second)));
        assert_eq!(pair_pairings.size_hint(), (0, None));
        assert_eq!(pair_pairings.next(), None);
        assert_eq!(pair_pairings.size_hint(), (0, None));
    }
}
