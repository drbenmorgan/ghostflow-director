// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::ghostflow::host::HostingServiceError;
use crates::json_job_dispatch::HandlerResult;
use crates::serde_json::Value;
use crates::thiserror::Error;

use config::{Host, ProjectStatus};
use handlers::common::handlers::utils;

#[derive(Debug, Error)]
enum MembershipError {
    #[error("failed to update membership for {}: {}", repo, source)]
    UpdateMembership {
        repo: String,
        #[source]
        source: HostingServiceError,
    },
    #[error("failed to refresh membership for {}: {}", repo, source)]
    RefreshMembership {
        repo: String,
        #[source]
        source: HostingServiceError,
    },
    #[error("uninitialized project: {}", project)]
    UninitializedProject { project: String },
    #[error("unknown project: {}", project)]
    UnknownProject { project: String },
    #[error(
        "failed to update {} repository memberships ({} succeeded)",
        failed,
        succeeded
    )]
    PartialSuccess { failed: usize, succeeded: usize },
}

struct MembershipResult;

impl MembershipResult {
    fn no_update_membership(repo: String, source: HostingServiceError) -> HandlerResult {
        HandlerResult::fail(MembershipError::UpdateMembership {
            repo,
            source,
        })
    }

    fn no_refresh_membership(repo: String, source: HostingServiceError) -> HandlerResult {
        HandlerResult::fail(MembershipError::RefreshMembership {
            repo,
            source,
        })
    }

    fn uninitialized_project(project: String) -> HandlerResult {
        HandlerResult::fail(MembershipError::UninitializedProject {
            project,
        })
    }

    fn unknown_project(project: String) -> HandlerResult {
        HandlerResult::fail(MembershipError::UnknownProject {
            project,
        })
    }

    fn non_group_project(project: &str) -> HandlerResult {
        HandlerResult::reject(format!("skipping non-group project {}", project))
    }

    fn partial_success(failed: usize, succeeded: usize) -> HandlerResult {
        HandlerResult::fail(MembershipError::PartialSuccess {
            failed,
            succeeded,
        })
    }
}

/// Handle a project's membership needing completely refreshed.
pub fn handle_project_membership_refresh(_: &Value, host: &Host, repo: &str) -> HandlerResult {
    let project = try_action!(utils::get_project(host, repo));

    project
        .refresh_membership()
        .map(|()| HandlerResult::Accept)
        .unwrap_or_else(|err| MembershipResult::no_update_membership(repo.into(), err))
}

/// Handle a group's membership needing completely refreshed.
pub fn handle_group_membership_refresh(_: &Value, host: &Host, group: &str) -> HandlerResult {
    let prefix = format!("{}/", group);

    let (succeeded, failed) = host
        .project_names()
        .map(|name| {
            if name.starts_with(&prefix) {
                match host.project(name) {
                    ProjectStatus::Initialized(project) => {
                        project
                            .refresh_membership()
                            .map(|()| HandlerResult::Accept)
                            .unwrap_or_else(|err| {
                                MembershipResult::no_refresh_membership(name.clone(), err)
                            })
                    },
                    ProjectStatus::FailedToInitialize => {
                        MembershipResult::uninitialized_project(name.clone())
                    },
                    ProjectStatus::Unknown => MembershipResult::unknown_project(name.clone()),
                }
            } else {
                MembershipResult::non_group_project(name)
            }
        })
        .fold((0, 0), |(succeeded, failed), res| {
            match res {
                HandlerResult::Accept => (succeeded + 1, failed),
                HandlerResult::Fail(_) => (succeeded, failed + 1),
                _ => (succeeded, failed),
            }
        });

    if failed == 0 {
        HandlerResult::Accept
    } else {
        MembershipResult::partial_success(failed, succeeded)
    }
}
