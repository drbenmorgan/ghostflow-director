// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Setup actions for the test service

use crates::ghostflow::host::{CommitStatusState, PipelineState};
use crates::git_workarea::CommitId;

use ghostflow_ext::AccessLevel;

/// Actions to perform while creating a test service.
#[derive(Debug)]
pub enum Action {
    /// Create a user on the service.
    CreateUser {
        /// The handle of the user.
        handle: String,
        /// The name of the user.
        name: String,
        /// The email of the user.
        email: String,
    },
    /// Create a new project on the service.
    NewProject {
        /// The name of the project.
        name: String,
        /// The branches to create on the project.
        branches: Vec<(String, CommitId)>,
        /// The owner of the project.
        owner: String,
    },
    /// Fork a project.
    ForkProject {
        /// The source project.
        from: String,
        /// The project to create.
        into: String,
        /// The owner of the fork.
        owner: String,
    },
    /// Push code into a project.
    PushRef {
        /// The project to push into.
        project: String,
        /// The refname to create or update.
        ///
        /// The reference is updated forcefully.
        refname: String,
        /// The commit to push.
        commit: CommitId,
        /// The user who pushed.
        user: String,
    },
    /// Delete a ref in a project.
    DeleteRef {
        /// The project to push into.
        project: String,
        /// The refname to create or update.
        ///
        /// The reference is updated forcefully.
        refname: String,
        /// The user who pushed.
        user: String,
    },
    /// Add a user to a project.
    AddTeamMember {
        /// The project to add the user to.
        project: String,
        /// The user to add to the project.
        user: String,
        /// The access level to give the user.
        access: AccessLevel,
    },
    /// Create a merge request.
    CreateMergeRequest {
        /// The project hosting the source branch.
        source_project: String,
        /// The branch which should be merged.
        source_branch: String,
        /// The project hosting the target branch.
        target_project: String,
        /// The branch which should be merged into.
        target_branch: String,
        /// The description of the merge request.
        description: String,
        /// The author of the merge request.
        author: String,
        /// Whether the merge request is a work-in-progress or not.
        work_in_progress: bool,
        /// Whether the source branch should be deleted or not.
        remove_source_branch: bool,
    },
    /// Create an issue.
    CreateIssue {
        /// The project to add the issue to.
        project: String,
    },
    /// Create a commit status for a commit.
    CreateStatus {
        /// The author of the status.
        author: String,
        /// The name of the status.
        name: String,
        /// The description of the status.
        description: String,
        /// The state of the status.
        state: CommitStatusState,
        /// The target URL for the status.
        target_url: Option<String>,
    },
    /// Create a pipeline for a commit.
    CreatePipeline {
        /// The project which owns the pipeline.
        project: String,
        /// The commit the pipeline is for.
        commit: CommitId,
    },
    /// Create a comment on the newest merge request.
    MergeRequestComment {
        /// The author of the comment.
        author: String,
        /// The content of the comment.
        content: String,
    },
    /// Create an award on the newest merge request.
    MergeRequestAward {
        /// The name of the award.
        name: String,
        /// The author of the award.
        author: String,
    },
    /// Create an award on the newest comment.
    CommentAward {
        /// The name of the award.
        name: String,
        /// The author of the award.
        author: String,
    },
    /// Enable pipeline support on the newest project.
    EnablePipelines,
    /// Create a job on the latest pipeline.
    CreateJob {
        /// The state of the job.
        state: PipelineState,
        /// The stage of the job.
        stage: Option<String>,
        /// The name of the job.
        name: String,
    },
    /// Delay the action sequence for a time.
    Delay {
        /// How long to wait for.
        millis: u64,
    },
    /// Delay the director for up to a number of milliseconds waiting for a file.
    DelayDirector {
        /// The path to wait for.
        path: String,
        /// How long to wait for.
        millis: u64,
    },
    /// Signal that the director got a job.
    Signal {
        /// The path to signal.
        path: String,
    },
    /// Force all projects to be cloned.
    CloneProjects,
    /// Reset any projects which failed to initialize.
    ResetFailedProjects,
    /// A no-op action.
    Ignore,
}

impl Action {
    pub fn create_user(name: &str) -> Self {
        Action::CreateUser {
            handle: name.into(),
            name: name.into(),
            email: format!("{}@example.org", name),
        }
    }

    pub fn new_project(owner: &str, name: &str) -> Self {
        Action::NewProject {
            name: format!("{}/{}", owner, name),
            branches: vec![(
                "master".into(),
                CommitId::new("6f561266fa69ead81dde5a0df5dad0eb952827c9"),
            )],
            owner: owner.into(),
        }
    }

    pub fn fork_project(origin: &str, name: &str, owner: &str) -> Self {
        Action::ForkProject {
            from: format!("{}/{}", origin, name),
            into: format!("{}/{}", owner, name),
            owner: owner.into(),
        }
    }

    pub fn push(owner: &str, name: &str, ref_: &str, commit: &str) -> Self {
        Action::PushRef {
            project: format!("{}/{}", owner, name),
            refname: ref_.into(),
            commit: CommitId::new(commit),
            user: owner.into(),
        }
    }

    pub fn delete_ref(owner: &str, name: &str, ref_: &str) -> Self {
        Action::DeleteRef {
            project: format!("{}/{}", owner, name),
            refname: ref_.into(),
            user: owner.into(),
        }
    }

    pub fn add_team_member(project: &str, user: &str, level: AccessLevel) -> Self {
        Action::AddTeamMember {
            project: project.into(),
            user: user.into(),
            access: level,
        }
    }

    pub fn create_mr(
        origin: &str,
        name: &str,
        user: &str,
        description: &str,
        remove_branch: bool,
    ) -> Self {
        Action::CreateMergeRequest {
            source_project: format!("{}/{}", user, name),
            source_branch: "mr-source".into(),
            target_project: format!("{}/{}", origin, name),
            target_branch: "master".into(),
            description: description.into(),
            author: user.into(),
            work_in_progress: description.starts_with("WIP"),
            remove_source_branch: remove_branch,
        }
    }

    pub fn mr_comment(user: &str, content: &str) -> Self {
        Action::MergeRequestComment {
            author: user.into(),
            content: content.into(),
        }
    }

    pub fn create_status<T>(
        user: &str,
        name: &str,
        description: &str,
        target_url: T,
        state: CommitStatusState,
    ) -> Self
    where
        T: Into<Option<String>>,
    {
        Action::CreateStatus {
            author: user.into(),
            name: name.into(),
            description: description.into(),
            target_url: target_url.into(),
            state,
        }
    }

    pub fn create_pipeline(owner: &str, name: &str, commit: &str) -> Self {
        Action::CreatePipeline {
            project: format!("{}/{}", owner, name),
            commit: CommitId::new(commit),
        }
    }

    pub fn create_job<S>(state: PipelineState, stage: S, name: &str) -> Self
    where
        S: Into<Option<&'static str>>,
    {
        Action::CreateJob {
            state,
            stage: stage.into().map(Into::into),
            name: name.into(),
        }
    }

    pub fn delay(millis: u64) -> Self {
        Action::Delay {
            millis,
        }
    }

    pub fn delay_director(path: &str, millis: u64) -> Self {
        Action::DelayDirector {
            path: path.into(),
            millis,
        }
    }

    pub fn signal(path: &str) -> Self {
        Action::Signal {
            path: path.into(),
        }
    }

    /// A description of the action.
    pub fn description(&self) -> String {
        format!("{:?}", self)
    }
}
