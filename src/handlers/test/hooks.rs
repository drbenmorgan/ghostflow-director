// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;
use std::ops::Deref;

use crates::chrono::{DateTime, Utc};
use crates::ghostflow::host;
use crates::git_workarea::CommitId;
use crates::serde::{Deserialize, Serialize};

use actions::merge_requests::Info;
use ghostflow_ext;
use handlers::common::data::*;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct User {
    pub id: u64,
    pub handle: String,
    pub name: String,
    pub email: String,
}

impl From<User> for host::User {
    fn from(user: User) -> Self {
        host::User {
            handle: user.handle,
            name: user.name,
            email: user.email,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Note {
    pub id: u64,
    pub is_branch_update: bool,
    pub created_at: DateTime<Utc>,
    pub author: User,
    pub content: String,
}

impl From<Note> for host::Comment {
    fn from(note: Note) -> Self {
        host::Comment {
            id: format!("{}", note.id),
            is_system: note.is_branch_update,
            is_branch_update: note.is_branch_update,
            created_at: note.created_at,
            author: note.author.into(),
            content: note.content,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum MergeRequestState {
    #[serde(rename = "open")]
    Open,
    #[serde(rename = "closed")]
    Closed,
    #[serde(rename = "merged")]
    Merged,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Repo {
    pub name: String,
    pub url: String,
    pub id: u64,
    pub forked_from: Option<Box<Repo>>,
}

impl From<Repo> for host::Repo {
    fn from(repo: Repo) -> Self {
        host::Repo {
            name: repo.name,
            url: repo.url,
            forked_from: repo
                .forked_from
                .map(|upstream| Box::new(upstream.deref().clone().into())),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct MergeRequest {
    pub source_repo: Repo,
    pub source_branch: String,
    pub target_repo: Repo,
    pub target_branch: String,
    pub id: u64,
    pub url: String,
    pub work_in_progress: bool,
    pub description: String,
    pub old_commit: Option<Commit>,
    pub commit: Commit,
    pub author: User,
    pub reference: String,
    pub remove_source_branch: bool,
}

impl From<MergeRequest> for host::MergeRequest {
    fn from(mr: MergeRequest) -> Self {
        host::MergeRequest {
            source_repo: Some(mr.source_repo.into()),
            source_branch: mr.source_branch,
            target_repo: mr.target_repo.into(),
            target_branch: mr.target_branch,
            id: mr.id,
            url: mr.url,
            work_in_progress: mr.work_in_progress,
            description: mr.description,
            old_commit: mr.old_commit.map(Into::into),
            commit: mr.commit.into(),
            author: mr.author.into(),
            reference: mr.reference,
            remove_source_branch: mr.remove_source_branch,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum MergeRequestAction {
    #[serde(rename = "comment")]
    Comment,
    #[serde(rename = "update")]
    Update,
    #[serde(rename = "open")]
    Open,
    #[serde(rename = "close")]
    Close,
    #[serde(rename = "merge")]
    Merge,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct MergeRequestHook {
    pub state: MergeRequestState,
    pub action: MergeRequestAction,
    pub updated_at: DateTime<Utc>,
    pub merge_request: MergeRequest,
    pub note: Option<Note>,
}

impl From<MergeRequestHook> for Info<'static> {
    fn from(hook: MergeRequestHook) -> Self {
        Info {
            merge_request: Cow::Owned(hook.merge_request.into()),
            was_opened: hook.action == MergeRequestAction::Open,
            was_merged: hook.action == MergeRequestAction::Merge,
            is_open: hook.state == MergeRequestState::Open,
            date: hook.updated_at,
        }
    }
}

impl From<MergeRequestHook> for MergeRequestNoteInfo<'static> {
    fn from(hook: MergeRequestHook) -> Self {
        MergeRequestNoteInfo {
            note: hook.note.clone().unwrap().into(),
            merge_request: hook.into(),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Commit {
    pub repo: Repo,
    pub refname: Option<String>,
    pub id: String,
    pub last_pipeline: Option<u64>,
}

impl From<Commit> for host::Commit {
    fn from(commit: Commit) -> Self {
        host::Commit {
            repo: commit.repo.into(),
            refname: commit.refname,
            id: CommitId::new(commit.id),
            last_pipeline: commit.last_pipeline,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct PushHook {
    pub commit: Commit,
    pub author: User,
    pub date: DateTime<Utc>,
}

impl From<PushHook> for PushInfo {
    fn from(hook: PushHook) -> Self {
        PushInfo {
            commit: hook.commit.into(),
            author: hook.author.into(),
            date: hook.date,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct ProjectHook {
    pub project: Repo,
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum MembershipAction {
    #[serde(rename = "added")]
    Added,
    #[serde(rename = "removed")]
    Removed,
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum AccessLevel {
    #[serde(rename = "contributor")]
    Contributor,
    #[serde(rename = "developer")]
    Developer,
    #[serde(rename = "maintainer")]
    Maintainer,
    #[serde(rename = "owner")]
    Owner,
}

impl From<AccessLevel> for ghostflow_ext::AccessLevel {
    fn from(access: AccessLevel) -> Self {
        match access {
            AccessLevel::Contributor => ghostflow_ext::AccessLevel::Contributor,
            AccessLevel::Developer => ghostflow_ext::AccessLevel::Developer,
            AccessLevel::Maintainer => ghostflow_ext::AccessLevel::Maintainer,
            AccessLevel::Owner => ghostflow_ext::AccessLevel::Owner,
        }
    }
}

impl From<ghostflow_ext::AccessLevel> for AccessLevel {
    fn from(access: ghostflow_ext::AccessLevel) -> Self {
        match access {
            ghostflow_ext::AccessLevel::Contributor => AccessLevel::Contributor,
            ghostflow_ext::AccessLevel::Developer => AccessLevel::Developer,
            ghostflow_ext::AccessLevel::Maintainer => AccessLevel::Maintainer,
            ghostflow_ext::AccessLevel::Owner => AccessLevel::Owner,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct MembershipHook {
    pub action: MembershipAction,
    pub repo: Repo,
    pub user: User,
    pub access: AccessLevel,
}

impl From<MembershipHook> for MembershipAdditionInfo {
    fn from(hook: MembershipHook) -> Self {
        MembershipAdditionInfo {
            repo: hook.repo.into(),
            membership: ghostflow_ext::Membership {
                user: hook.user.into(),
                access_level: hook.access.into(),
                expiration: None,
            },
        }
    }
}

impl From<MembershipHook> for MembershipRemovalInfo {
    fn from(hook: MembershipHook) -> Self {
        MembershipRemovalInfo {
            repo: hook.repo.into(),
            user: hook.user.into(),
        }
    }
}
