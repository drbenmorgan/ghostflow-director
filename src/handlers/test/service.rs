// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![allow(deprecated)]

use std::any::Any;
use std::cell::Cell;
use std::env;
use std::error::Error;
use std::ffi::{OsStr, OsString};
use std::fmt;
use std::fs;
use std::io::{self, BufReader};
use std::iter;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::sync::{self, Arc, RwLock};
use std::thread::{self, JoinHandle};
use std::time::Duration;

use crates::futures::sync::mpsc::{self, Receiver, Sender};
use crates::futures::{Future, IntoFuture, Sink, Stream};
use crates::ghostflow::host::CommitStatusState;
use crates::git_workarea::{CommitId, GitContext, GitError};
use crates::itertools::Itertools;
use crates::json_job_dispatch::{Director, DirectorWatchdog, Handler, RunResult};
use crates::serde::Deserialize;
use crates::serde_json::{self, Value};
use crates::tempdir::TempDir;
use crates::thiserror::Error;
use crates::tokio;
use crates::tokio::runtime::current_thread::Runtime;
use crates::tokio::timer::Timeout;
// XXX: https://github.com/tokio-rs/tokio/pull/573
// use crates::tokio::util::StreamExt;
use crates::tokio_uds::UnixListener;

use config;
use ghostflow_ext::AccessLevel;
use handlers::test::action::Action;
use handlers::test::data::{self, Data, RefUpdate, User};
use handlers::test::ghostflow::TestHost;
use handlers::test::handler::TestHandler;
use tests::log;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Dir {
    Webhook,
    Delay,
    Signal,
}

impl fmt::Display for Dir {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let desc = match self {
            Dir::Webhook => "webhook",
            Dir::Delay => "delay",
            Dir::Signal => "signal",
        };

        write!(f, "{}", desc)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum InitStep {
    Init,
    Fetch,
    Branch(String),
}

impl fmt::Display for InitStep {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InitStep::Init => write!(f, "init"),
            InitStep::Fetch => write!(f, "fetch"),
            InitStep::Branch(ref b) => write!(f, "create branch {}", b),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DataOp {
    AddProject,
    HandlePush,
    AddMember,
    CreateMergeRequest,
    AddStatus,
    AddMergeRequestComment,
    AddMergeRequestAward,
    AddCommentAward,
    EnablePipelines,
    CreateJob,
    AddHook,
    AddIssueComment,
    AddIssueLabels,
}

impl fmt::Display for DataOp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let desc = match self {
            DataOp::AddProject => "add project",
            DataOp::AddIssueLabels => "add issue labels",
            DataOp::HandlePush => "handle push",
            DataOp::AddMember => "add member",
            DataOp::CreateMergeRequest => "create merge request",
            DataOp::AddStatus => "add status",
            DataOp::AddMergeRequestComment => "add merge request comment",
            DataOp::AddMergeRequestAward => "add merge request award",
            DataOp::AddCommentAward => "add comment award",
            DataOp::EnablePipelines => "enable pipelines",
            DataOp::CreateJob => "create job",
            DataOp::AddHook => "add hook",
            DataOp::AddIssueComment => "add issue comment",
        };

        write!(f, "{}", desc)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SocketStep {
    Bind,
    Accept,
    Read,
}

impl fmt::Display for SocketStep {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let desc = match self {
            SocketStep::Bind => "bind",
            SocketStep::Accept => "accept",
            SocketStep::Read => "read",
        };

        write!(f, "{}", desc)
    }
}

#[derive(Debug, Error)]
pub enum ServiceError {
    #[error("a timeout occurred")]
    TimedOut {},
    #[error("failed to create temporary directory named {}: {}", name, source)]
    CreateTemporaryDirectory {
        name: String,
        #[source]
        source: io::Error,
    },
    #[error("failed to create {} directory {}: {}", dir, path.display(), source)]
    CreateDirectory {
        dir: Dir,
        path: PathBuf,
        #[source]
        source: io::Error,
    },
    #[error("failed to set git config {} = {:?}: {}", key, value, output)]
    GitConfig {
        key: String,
        value: OsString,
        output: String,
    },
    #[error("failed to obtain a lock on the hosting data: {}", reason)]
    DataLock { reason: String },
    #[error("failed to create {} directory for {}: {}", path.display(), project, source)]
    ProjectCreate {
        path: PathBuf,
        project: String,
        #[source]
        source: io::Error,
    },
    #[error(
        "failed to initialize the {} repository at the {} step: {}",
        project,
        step,
        output
    )]
    ProjectInit {
        project: String,
        step: InitStep,
        output: String,
    },
    #[error("failed to {}: {}", op, source)]
    UpdateData {
        op: DataOp,
        #[source]
        source: data::HookError,
    },
    #[error(
        "failed to initialize the {} repository from {}: {}",
        project,
        upstream,
        output
    )]
    ProjectFork {
        upstream: String,
        project: String,
        output: String,
    },
    #[error(
        "failed to update {} in {} to {}: {}",
        refname,
        project,
        commit,
        output
    )]
    UpdatePush {
        project: String,
        refname: String,
        commit: CommitId,
        output: String,
    },
    #[error("failed to delete {} in {}: {}", refname, project, output)]
    DeleteRef {
        project: String,
        refname: String,
        output: String,
    },
    #[error("failed to parse {} in {}: {}", refname, project, output)]
    RevParse {
        project: String,
        refname: String,
        output: String,
    },
    #[error("failed to read server channel")]
    ReadChannel {},
    #[error("a service may only be run once")]
    DuplicateRun {},
    #[error("failed to {} the hook socket: {}", step, source)]
    HookSocket {
        step: SocketStep,
        #[source]
        source: io::Error,
    },
    #[error("failed to parse hook into JSON: {}", source)]
    ParseHook {
        #[source]
        source: serde_json::Error,
    },
    #[error("failed to deserialize hook: {}", source)]
    DeserializeHook {
        #[source]
        source: serde_json::Error,
    },
    #[error("unexpected exit status: {:?}", exit)]
    ExitStatus { exit: ServiceExit },
    #[error("failed to join director thread: {:?}", err)]
    JoinDirector { err: Box<dyn Any + Send + 'static> },
    #[error("director did not exit successfully: {:?}", result)]
    DirectorFailure { result: RunResult },
    #[error("failed to deserialize configuration: {}", source)]
    DeserializeConfiguration {
        #[source]
        source: serde_json::Error,
    },
    #[error("failed to construct host: {}", source)]
    ConstructHost {
        #[source]
        source: config::ConfigError,
    },
    #[error("failed to write event to the service channel: {}", source)]
    WriteEvent {
        #[source]
        source: mpsc::SendError<ServiceEvent>,
    },
    #[error("tokio error: {}", source)]
    Tokio {
        #[source]
        source: io::Error,
    },
    #[error("timer error: {}", source)]
    Timer {
        #[from]
        source: tokio::timer::Error,
    },
    #[error("data error: {}", source)]
    Data {
        #[from]
        source: data::DataError,
    },
    #[error("hook error: {}", source)]
    Hook {
        #[from]
        source: data::HookError,
    },
    #[error("git error: {}", source)]
    Git {
        #[from]
        source: GitError,
    },
    #[error("director error: {}", source)]
    Director {
        #[from]
        source: json_job_dispatch::DirectorError,
    },
    #[error("add watchdog error: {}", source)]
    AddWatchdog {
        #[source]
        source: Box<dyn Error + Send + Sync>,
    },
    #[error("add hander error: {}", source)]
    AddHandler {
        #[source]
        source: Box<dyn Error + Send + Sync>,
    },
}

impl ServiceError {
    fn create_temporary_directory(name: String, source: io::Error) -> Self {
        ServiceError::CreateTemporaryDirectory {
            name,
            source,
        }
    }

    fn create_directory(dir: Dir, path: PathBuf, source: io::Error) -> Self {
        ServiceError::CreateDirectory {
            dir,
            path,
            source,
        }
    }

    fn git_config(key: String, value: OsString, output: &[u8]) -> Self {
        ServiceError::GitConfig {
            key,
            value,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn data_lock<T>(source: sync::PoisonError<T>) -> Self {
        ServiceError::DataLock {
            reason: format!("{}", source),
        }
    }

    fn project_create(path: PathBuf, project: String, source: io::Error) -> Self {
        ServiceError::ProjectCreate {
            path,
            project,
            source,
        }
    }

    fn project_init(project: String, step: InitStep, output: &[u8]) -> Self {
        ServiceError::ProjectInit {
            project,
            step,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn update_data<E>(op: DataOp, source: E) -> Self
    where
        E: Into<data::HookError>,
    {
        ServiceError::UpdateData {
            op,
            source: source.into(),
        }
    }

    fn project_fork(upstream: String, project: String, output: &[u8]) -> Self {
        ServiceError::ProjectFork {
            upstream,
            project,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn update_push(project: String, refname: String, commit: CommitId, output: &[u8]) -> Self {
        ServiceError::UpdatePush {
            project,
            refname,
            commit,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn delete_ref(project: String, refname: String, output: &[u8]) -> Self {
        ServiceError::DeleteRef {
            project,
            refname,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn rev_parse(project: String, refname: String, output: &[u8]) -> Self {
        ServiceError::RevParse {
            project,
            refname,
            output: String::from_utf8_lossy(output).into(),
        }
    }

    fn read_channel(_: ()) -> Self {
        ServiceError::ReadChannel {}
    }

    fn duplicate_run() -> Self {
        ServiceError::DuplicateRun {}
    }

    fn hook_socket(step: SocketStep, source: io::Error) -> Self {
        ServiceError::HookSocket {
            step,
            source,
        }
    }

    fn parse_hook(source: serde_json::Error) -> Self {
        ServiceError::ParseHook {
            source,
        }
    }

    fn deserialize_hook(source: serde_json::Error) -> Self {
        ServiceError::DeserializeHook {
            source,
        }
    }

    fn exit_status(exit: ServiceExit) -> Self {
        ServiceError::ExitStatus {
            exit,
        }
    }

    fn join_director(err: Box<dyn Any + Send + 'static>) -> Self {
        ServiceError::JoinDirector {
            err,
        }
    }

    fn director_failure(result: RunResult) -> Self {
        ServiceError::DirectorFailure {
            result,
        }
    }

    fn deserialize_configuration(source: serde_json::Error) -> Self {
        ServiceError::DeserializeConfiguration {
            source,
        }
    }

    fn construct_host(source: config::ConfigError) -> Self {
        ServiceError::ConstructHost {
            source,
        }
    }

    fn write_event(source: mpsc::SendError<ServiceEvent>) -> Self {
        ServiceError::WriteEvent {
            source,
        }
    }

    fn tokio(source: io::Error) -> Self {
        ServiceError::Tokio {
            source,
        }
    }

    fn add_watchdog(source: Box<dyn Error + Send + Sync>) -> Self {
        ServiceError::AddWatchdog {
            source,
        }
    }

    fn add_handler(source: Box<dyn Error + Send + Sync>) -> Self {
        ServiceError::AddHandler {
            source,
        }
    }
}

impl From<tokio::timer::timeout::Error<ServiceError>> for ServiceError {
    fn from(err: tokio::timer::timeout::Error<Self>) -> Self {
        // See https://github.com/tokio-rs/tokio/issues/581.
        if err.is_elapsed() {
            ServiceError::TimedOut {}
        } else if err.is_inner() {
            err.into_inner().expect("timeout error lied to us")
        } else if err.is_timer() {
            err.into_timer().expect("timeout error lied to us").into()
        } else {
            unreachable!("new timer error implementation?")
        }
    }
}

/// Git hook events.
#[derive(Deserialize, Debug, Clone)]
#[serde(tag = "hook")]
pub enum GitHookEvent {
    #[serde(rename = "post-receive")]
    PostReceive {
        project: String,
        updates: Vec<RefUpdate>,
    },
}

impl GitHookEvent {
    /// A description of a git hook event.
    fn description(&self) -> String {
        match *self {
            GitHookEvent::PostReceive {
                ref project,
                ref updates,
            } => {
                format!(
                    "refs pushed into {}: {}",
                    project,
                    updates.iter().format(", "),
                )
            },
        }
    }
}

/// An event from the `HostingService`.
///
/// All actions through this are performed as the administrative user.
#[derive(Debug)]
pub enum HostEvent {
    /// Add a user to a project.
    AddMember {
        /// The project to add the user to.
        project: u64,
        /// The user to add.
        user: String,
        /// The access level of the user.
        level: AccessLevel,
    },
    /// Add a hook to a project.
    AddHook {
        /// The project to add the hook to.
        project: u64,
        /// The URL to contact.
        url: String,
    },
    /// Create a comment on a merge request.
    MergeRequestComment {
        /// The merge request to comment on.
        mr: u64,
        /// The content of the comment.
        content: String,
    },
    /// Add a commit status to a commit.
    CommitStatus {
        /// The project associated with the status.
        project: u64,
        /// The commit associated with the status.
        commit: CommitId,
        /// The name of the status.
        name: String,
        /// The description of the status.
        description: String,
        /// The state of the status.
        state: CommitStatusState,
        /// The target URL for the status.
        target_url: Option<String>,
    },
    /// Award a comment.
    AwardComment {
        /// The comment to award.
        comment: u64,
        /// The award to give.
        award: String,
    },
    /// Add labels to an issue.
    AddIssueLabels {
        /// The issue to add labels to.
        issue: u64,
        /// The labels to add.
        labels: Vec<String>,
    },
    /// A job was triggered.
    TriggerJob {
        /// The project the job belongs to.
        project: String,
        /// The name of the triggered job.
        job: String,
        /// The user to associate with the trigger.
        user: Option<String>,
    },
}

impl HostEvent {
    /// A description of the hosting request.
    fn description(&self) -> String {
        format!("{:?}", self)
    }
}

/// Events which prompt action from the service.
#[derive(Debug)]
pub enum ServiceEvent {
    /// An event from the `HostingService`.
    Host(HostEvent),
    /// An event from git hooks.
    GitHook(GitHookEvent),
    /// Quit the service.
    Quit,
}

impl ServiceEvent {
    /// A description of the service event.
    fn description(&self) -> String {
        match *self {
            ServiceEvent::Host(ref host) => format!("host: {}", host.description()),
            ServiceEvent::GitHook(ref hook) => format!("hook: {}", hook.description()),
            ServiceEvent::Quit => "quit".into(),
        }
    }
}

/// The signal which indicates that the test is over.
#[derive(Debug, Clone)]
pub enum EndSignal {
    MergeRequestComment {
        content: String,
    },
    CommitStatus {
        commit: CommitId,
        name: String,
        state: CommitStatusState,
        description: Option<String>,
        target_url: Option<String>,
    },
    CommitStatusRepeat {
        count: Cell<usize>,
        commit: CommitId,
        name: String,
        state: CommitStatusState,
        description: Option<String>,
        target_url: Option<String>,
    },
    Push {
        project: String,
        ref_: String,
        sha1: CommitId,
    },
    TriggerJob {
        project: String,
        job: String,
        user: Option<String>,
    },
    /// Never exit.
    ///
    /// Using this guarantees a timeout since nothing else will cause the service to exit.
    Ignore,
}

impl EndSignal {
    /// Whether an event matches the signal or not.
    fn matches(&self, event: &ServiceEvent) -> bool {
        match *event {
            ServiceEvent::Host(ref host) => {
                match (self, host) {
                    (
                        &EndSignal::MergeRequestComment {
                            content: ref expected,
                        },
                        &HostEvent::MergeRequestComment {
                            ref content, ..
                        },
                    ) => expected == content,
                    (
                        &EndSignal::CommitStatus {
                            commit: ref expected_commit,
                            name: ref expected_name,
                            state: expected_state,
                            description: ref expected_description,
                            target_url: ref expected_target_url,
                        },
                        &HostEvent::CommitStatus {
                            ref commit,
                            ref name,
                            state,
                            ref description,
                            ref target_url,
                            ..
                        },
                    ) => {
                        expected_commit == commit
                            && expected_name == name
                            && expected_state == state
                            && expected_description
                                .as_ref()
                                .map(|ed| ed == description)
                                .unwrap_or(true)
                            && expected_target_url == target_url
                    },
                    (
                        &EndSignal::CommitStatusRepeat {
                            ref count,
                            commit: ref expected_commit,
                            name: ref expected_name,
                            state: expected_state,
                            description: ref expected_description,
                            target_url: ref expected_target_url,
                        },
                        &HostEvent::CommitStatus {
                            ref commit,
                            ref name,
                            state,
                            ref description,
                            ref target_url,
                            ..
                        },
                    ) => {
                        expected_commit == commit
                            && expected_name == name
                            && expected_state == state
                            && expected_description
                                .as_ref()
                                .map(|ed| ed == description)
                                .unwrap_or(true)
                            && expected_target_url == target_url
                            // The commit status matches; check if we've gotten it the requisite
                            // number of times.
                            && {
                                // https://github.com/rust-lang/rust/issues/50186
                                // #![feature(cell_update)]
                                // count.update(|x| x - 1) == 0
                                let new_count = count.get() - 1;
                                count.set(new_count);
                                new_count == 0
                            }
                    },
                    (
                        &EndSignal::TriggerJob {
                            project: ref expected_project,
                            job: ref expected_job,
                            user: ref expected_user,
                        },
                        &HostEvent::TriggerJob {
                            ref project,
                            ref job,
                            ref user,
                        },
                    ) => {
                        expected_project == project && expected_job == job && expected_user == user
                    },
                    _ => false,
                }
            },
            ServiceEvent::GitHook(ref hook) => {
                match (self, hook) {
                    (
                        &EndSignal::Push {
                            project: ref expected_project,
                            ref_: ref expected_ref,
                            sha1: ref expected_sha1,
                        },
                        &GitHookEvent::PostReceive {
                            ref project,
                            ref updates,
                        },
                    ) => {
                        expected_project == project
                            && updates.iter().any(|ref_update| {
                                expected_ref == &ref_update.ref_
                                    && expected_sha1.as_str() == ref_update.new
                            })
                    },
                    _ => false,
                }
            },
            ServiceEvent::Quit => true,
        }
    }
}

/// Why the service exited.
#[derive(Deserialize, Debug, Clone, Copy, PartialEq, Eq)]
pub enum ServiceExit {
    /// The service completed successfully.
    Complete,
    /// The service timed out.
    TimedOut,
}

pub struct TestService {
    root: TempDir,
    reader: Option<Receiver<ServiceEvent>>,
    writer: Sender<ServiceEvent>,
    hook_socket: PathBuf,

    // Feature flags
    with_pipelines: bool,

    // Service data
    data: Arc<RwLock<Data>>,
    admin: User,
    pub log: Vec<String>,
}

lazy_static! {
    static ref SERVICE_TIMEOUT: Duration = {
        let timeout = env::var("GHOSTFLOW_TEST_SERVICE_TIMEOUT")
            .map_err(|err| {
                if let env::VarError::NotUnicode(val) = err {
                    error!("non-unicode GHOSTFLOW_TEST_SERVICE_TIMEOUT given; ignoring: {:?}", val);
                }
            })
            // Ignore environment variable errors.
            .ok()
            .and_then(|val| {
                val.parse()
                    .map_err(|err| {
                        error!("failed to parse GHOSTFLOW_TEST_SERVICE_TIMEOUT; ignoring: {}", err);
                    })
                    .ok()
            })
            .unwrap_or(5);

        Duration::from_secs(timeout)
    };
}

impl TestService {
    pub fn new(name: &str, actions: Vec<Action>) -> Result<Self, ServiceError> {
        let mut service = Self::new_impl(name)?;
        service.prepare(actions)?;
        Ok(service)
    }

    fn new_impl(name: &str) -> Result<Self, ServiceError> {
        log::setup_logging();

        // `new_in` cannot be used because the socket path needs to be less than `SUN_LEN`, which
        // is generally around 100 bytes.
        let root = TempDir::new(name)
            .map_err(|err| ServiceError::create_temporary_directory(name.into(), err))?;

        let webhook_dir = root.path().join("queue");
        fs::create_dir_all(&webhook_dir).map_err(|err| {
            ServiceError::create_directory(Dir::Webhook, webhook_dir.clone(), err)
        })?;

        let (writer, reader) = mpsc::channel(2);
        Ok(TestService {
            hook_socket: root.path().join("socket"),
            root,
            reader: Some(reader),
            writer,

            with_pipelines: false,

            admin: User {
                id: 0,
                handle: "ghostflow".into(),
                name: "Ghostflow Director".into(),
                email: "ghostflow-director-testing@example.com".into(),
            },
            data: Arc::new(RwLock::new(Data::new(webhook_dir))),
            log: Vec::new(),
        })
    }

    pub fn root(&self) -> &Path {
        self.root.path()
    }

    fn prepare<A>(&mut self, actions: A) -> Result<(), ServiceError>
    where
        A: IntoIterator<Item = Action>,
    {
        let create_admin = Action::CreateUser {
            handle: self.admin.handle.clone(),
            name: self.admin.name.clone(),
            email: self.admin.email.clone(),
        };

        iter::once(create_admin)
            .chain(actions.into_iter())
            .map(|action| self.apply(action))
            .collect::<Result<Vec<_>, ServiceError>>()?;

        Ok(())
    }

    fn set_config<N, V>(ctx: &GitContext, name: N, value: V) -> Result<(), ServiceError>
    where
        N: AsRef<str>,
        V: AsRef<OsStr>,
    {
        let config = ctx
            .git()
            .arg("config")
            .arg(name.as_ref())
            .arg(value.as_ref())
            .output()
            .map_err(|err| GitError::subcommand("config", err))?;
        if !config.status.success() {
            return Err(ServiceError::git_config(
                name.as_ref().into(),
                value.as_ref().into(),
                &config.stderr,
            ));
        }

        Ok(())
    }

    fn init_repo<N>(&self, ctx: GitContext, name: N) -> Result<(), ServiceError>
    where
        N: AsRef<str>,
    {
        Self::set_config(&ctx, "service.project", name.as_ref())?;
        Self::set_config(&ctx, "service.socket", &self.hook_socket)?;
        Self::set_config(
            &ctx,
            "core.hooksPath",
            concat!(env!("CARGO_MANIFEST_DIR"), "/test/hooks"),
        )?;
        Ok(())
    }

    fn apply(&mut self, action: Action) -> Result<(), ServiceError> {
        info!(
            target: "test-service/action",
            "action: {:?}",
            action,
        );

        let mut data = self.data.write().map_err(ServiceError::data_lock)?;

        match action {
            Action::CreateUser {
                handle,
                name,
                email,
            } => {
                data.add_user(handle, name, email);
            },
            Action::NewProject {
                name,
                branches,
                owner,
            } => {
                let owner = data.user(owner)?.id;

                let project_dir = self.root().join("projects").join(format!("{}.git", name));
                fs::create_dir_all(&project_dir).map_err(|err| {
                    ServiceError::project_create(project_dir.clone(), name.clone(), err)
                })?;
                let ctx = GitContext::new(&project_dir);
                let init = ctx
                    .git()
                    .arg("init")
                    .arg("--bare")
                    .output()
                    .map_err(|err| GitError::subcommand("init", err))?;
                if !init.status.success() {
                    return Err(ServiceError::project_init(
                        name,
                        InitStep::Init,
                        &init.stderr,
                    ));
                }
                let fetch = ctx
                    .git()
                    .arg("fetch")
                    .arg(env!("CARGO_MANIFEST_DIR"))
                    .output()
                    .map_err(|err| GitError::subcommand("fetch", err))?;
                if !fetch.status.success() {
                    return Err(ServiceError::project_init(
                        name,
                        InitStep::Fetch,
                        &fetch.stderr,
                    ));
                }

                for (refname, commit) in branches {
                    let update_ref = ctx
                        .git()
                        .arg("update-ref")
                        .arg(format!("refs/heads/{}", refname))
                        .arg(commit.as_str())
                        .output()
                        .map_err(|err| GitError::subcommand("update-ref", err))?;
                    if !update_ref.status.success() {
                        return Err(ServiceError::project_init(
                            name,
                            InitStep::Branch(refname),
                            &update_ref.stderr,
                        ));
                    }
                }
                self.init_repo(ctx, &name)?;

                data.add_project(name, project_dir, owner, None)
                    .map_err(|err| ServiceError::update_data(DataOp::AddProject, err))?;
            },
            Action::ForkProject {
                from,
                into,
                owner,
            } => {
                let owner = data.user(owner)?.id;
                let (origin, origin_dir) = {
                    let project = data.project(&from)?;
                    (project.id, project.url.clone())
                };

                let project_dir = self.root().join("projects").join(format!("{}.git", into));
                fs::create_dir_all(&project_dir).map_err(|err| {
                    ServiceError::project_create(project_dir.clone(), into.clone(), err)
                })?;
                let clone = Command::new("git")
                    .arg("clone")
                    .arg("--bare")
                    .arg(origin_dir)
                    .arg(&project_dir)
                    .output()
                    .map_err(|err| GitError::subcommand("clone", err))?;
                if !clone.status.success() {
                    return Err(ServiceError::project_fork(from, into, &clone.stderr));
                }
                let ctx = GitContext::new(&project_dir);
                let fetch = ctx
                    .git()
                    .arg("fetch")
                    .arg(env!("CARGO_MANIFEST_DIR"))
                    .output()
                    .map_err(|err| GitError::subcommand("fetch", err))?;
                if !fetch.status.success() {
                    return Err(ServiceError::project_init(
                        into,
                        InitStep::Fetch,
                        &fetch.stderr,
                    ));
                }
                self.init_repo(ctx, &into)?;

                data.add_project(into, project_dir, owner, Some(origin))
                    .map_err(|err| ServiceError::update_data(DataOp::AddProject, err))?;
            },
            Action::PushRef {
                project,
                refname,
                commit,
                user,
            } => {
                let url = {
                    let project = data.project(&project)?;
                    project.url.clone()
                };
                let user_id = data.user(user)?.id;
                let ctx = GitContext::new(url);

                let refname = if refname.starts_with("refs/") {
                    refname
                } else {
                    format!("refs/heads/{}", refname)
                };
                let rev_parse = ctx
                    .git()
                    .arg("rev-parse")
                    .arg(&refname)
                    .output()
                    .map_err(|err| GitError::subcommand("rev-parse", err))?;
                let old = if rev_parse.status.success() {
                    String::from_utf8_lossy(&rev_parse.stdout).trim().into()
                } else {
                    "0000000000000000000000000000000000000000".into()
                };
                let update_ref = ctx
                    .git()
                    .arg("update-ref")
                    .arg(&refname)
                    .arg(commit.as_str())
                    .output()
                    .map_err(|err| GitError::subcommand("update-ref", err))?;
                if !update_ref.status.success() {
                    return Err(ServiceError::update_push(
                        project,
                        refname,
                        commit,
                        &update_ref.stderr,
                    ));
                }

                let update = RefUpdate {
                    ref_: refname,
                    old,
                    new: commit.as_str().into(),
                };
                data.handle_push(user_id, &project, &update)
                    .map_err(|err| ServiceError::update_data(DataOp::HandlePush, err))?;
            },
            Action::DeleteRef {
                project,
                refname,
                user,
            } => {
                let url = {
                    let project = data.project(&project)?;
                    project.url.clone()
                };
                let user_id = data.user(user)?.id;
                let ctx = GitContext::new(url);

                let refname = if refname.starts_with("refs/") {
                    refname
                } else {
                    format!("refs/heads/{}", refname)
                };
                let rev_parse = ctx
                    .git()
                    .arg("rev-parse")
                    .arg(&refname)
                    .output()
                    .map_err(|err| GitError::subcommand("rev-parse", err))?;
                let old = if rev_parse.status.success() {
                    String::from_utf8_lossy(&rev_parse.stdout).trim().into()
                } else {
                    "0000000000000000000000000000000000000000".into()
                };
                let update_ref = ctx
                    .git()
                    .arg("update-ref")
                    .arg("-d")
                    .arg(&refname)
                    .output()
                    .map_err(|err| GitError::subcommand("update-ref", err))?;
                if !update_ref.status.success() {
                    return Err(ServiceError::delete_ref(
                        project,
                        refname,
                        &update_ref.stderr,
                    ));
                }

                let update = RefUpdate {
                    ref_: refname,
                    old,
                    new: "0000000000000000000000000000000000000000".into(),
                };
                data.handle_push(user_id, &project, &update)
                    .map_err(|err| ServiceError::update_data(DataOp::HandlePush, err))?;
            },
            Action::AddTeamMember {
                project,
                user,
                access,
            } => {
                let project_id = data.project(&project)?.id;
                let user_handle = data.user(&user)?.handle.clone();
                data.add_to_project(project_id, user_handle, access)
                    .map_err(|err| ServiceError::update_data(DataOp::AddMember, err))?;
            },
            Action::CreateMergeRequest {
                source_project,
                source_branch,
                target_project,
                target_branch,
                description,
                author,
                work_in_progress,
                remove_source_branch,
            } => {
                let user_id = data.user(author)?.id;
                let (source_id, commit) = {
                    let project = data.project(&source_project)?;

                    let ctx = GitContext::new(&project.url);
                    let rev_parse = ctx
                        .git()
                        .arg("rev-parse")
                        .arg(&source_branch)
                        .output()
                        .map_err(|err| GitError::subcommand("rev-parse", err))?;
                    if !rev_parse.status.success() {
                        return Err(ServiceError::rev_parse(
                            source_project,
                            source_branch,
                            &rev_parse.stderr,
                        ));
                    }
                    let parsed = String::from_utf8_lossy(&rev_parse.stdout);

                    (project.id, CommitId::new(parsed.trim()))
                };
                let target_id = data.project(&target_project)?.id;

                data.add_mr(
                    source_id,
                    source_branch,
                    target_id,
                    target_branch,
                    description,
                    commit,
                    user_id,
                    work_in_progress,
                    remove_source_branch,
                )
                .map_err(|err| ServiceError::update_data(DataOp::CreateMergeRequest, err))?;
            },
            Action::CreateIssue {
                project,
            } => {
                let project_id = data.project(&project)?.id;

                data.add_issue(project_id);
            },
            Action::CreateStatus {
                author,
                name,
                description,
                target_url,
                state,
            } => {
                let user_id = data.user(author)?.id;

                data.add_mr_status(user_id, name, description, target_url, state)
                    .map_err(|err| ServiceError::update_data(DataOp::AddStatus, err))?;
            },
            Action::CreatePipeline {
                project,
                commit,
            } => {
                let project_id = data.project(&project)?.id;

                data.add_pipeline(project_id, commit);
            },
            Action::MergeRequestComment {
                author,
                content,
            } => {
                let user_id = data.user(author)?.id;

                let id = data.add_comment(user_id, content);
                data.add_mr_comment(None, id).map_err(|err| {
                    ServiceError::update_data(DataOp::AddMergeRequestComment, err)
                })?;
            },
            Action::MergeRequestAward {
                name,
                author,
            } => {
                let user_id = data.user(&author)?.id;

                let id = data.add_award(user_id, name);
                data.add_mr_award(None, id)
                    .map_err(|err| ServiceError::update_data(DataOp::AddMergeRequestAward, err))?;
            },
            Action::CommentAward {
                name,
                author,
            } => {
                let user_id = data.user(&author)?.id;

                let id = data.add_award(user_id, name);
                data.add_comment_award(None, id)
                    .map_err(|err| ServiceError::update_data(DataOp::AddCommentAward, err))?;
            },
            Action::EnablePipelines => {
                self.with_pipelines = true;
                data.enable_pipelines()
                    .map_err(|err| ServiceError::update_data(DataOp::EnablePipelines, err))?;
            },
            Action::CreateJob {
                state,
                stage,
                name,
            } => {
                data.add_job(state, stage, name)
                    .map_err(|err| ServiceError::update_data(DataOp::CreateJob, err))?;
            },
            Action::Delay {
                millis,
            } => {
                thread::sleep(Duration::from_millis(millis));
            },
            Action::DelayDirector {
                path,
                millis,
            } => {
                let delay_dir = self.root().join("delay");
                fs::create_dir_all(&delay_dir).map_err(|err| {
                    ServiceError::create_directory(Dir::Delay, delay_dir.clone(), err)
                })?;
                let path = delay_dir.join(path);
                data.queue_job(
                    "delay",
                    json!({
                        "path": path.to_string_lossy(),
                        "millis": millis,
                    }),
                )?;
            },
            Action::Signal {
                path,
            } => {
                let signal_dir = self.root().join("signal");
                fs::create_dir_all(&signal_dir).map_err(|err| {
                    ServiceError::create_directory(Dir::Signal, signal_dir.clone(), err)
                })?;
                let path = signal_dir.join(path);
                data.queue_job(
                    "signal",
                    json!({
                        "path": path.to_string_lossy(),
                    }),
                )?;
            },
            Action::CloneProjects => {
                data.queue_job("clone_projects", json!(null))?;
            },
            Action::ResetFailedProjects => {
                data.queue_job("reset_failed_projects", json!(null))?;
            },
            Action::Ignore => (),
        }

        Ok(())
    }

    fn handle_event(
        &mut self,
        event: &ServiceEvent,
        end_signal: &EndSignal,
    ) -> Result<bool, ServiceError> {
        info!(
            target: "test-service/event",
            "event: {:?}",
            event,
        );
        self.log.push(event.description());

        match *event {
            ServiceEvent::Host(ref host) => {
                let mut data = self.data.write().map_err(ServiceError::data_lock)?;

                match *host {
                    HostEvent::AddMember {
                        project,
                        ref user,
                        level,
                    } => {
                        data.add_to_project(project, user, level)
                            .map_err(|err| ServiceError::update_data(DataOp::AddMember, err))?;
                    },
                    HostEvent::AddHook {
                        project,
                        ref url,
                    } => {
                        data.add_hook(project, url)
                            .map_err(|err| ServiceError::update_data(DataOp::AddHook, err))?;
                    },
                    HostEvent::MergeRequestComment {
                        mr,
                        ref content,
                    } => {
                        let id = data.add_comment(self.admin.id, content);
                        data.add_mr_comment(Some(mr), id).map_err(|err| {
                            ServiceError::update_data(DataOp::AddMergeRequestComment, err)
                        })?;
                    },
                    HostEvent::CommitStatus {
                        project,
                        ref commit,
                        ref name,
                        ref description,
                        ref target_url,
                        state,
                    } => {
                        data.add_status(
                            project,
                            commit,
                            self.admin.id,
                            name,
                            description,
                            target_url.clone(),
                            state,
                        )
                    },
                    HostEvent::AwardComment {
                        comment,
                        ref award,
                    } => {
                        let id = data.add_award(self.admin.id, award);
                        data.add_comment_award(Some(comment), id).map_err(|err| {
                            ServiceError::update_data(DataOp::AddCommentAward, err)
                        })?;
                    },
                    HostEvent::AddIssueLabels {
                        issue,
                        ref labels,
                    } => {
                        data.add_issue_labels(issue, labels.iter().cloned())
                            .map_err(|err| {
                                ServiceError::update_data(DataOp::AddIssueLabels, err)
                            })?;
                    },
                    HostEvent::TriggerJob {
                        ..
                    } => {
                        // Nothing to do right now.
                    },
                }
            },
            ServiceEvent::GitHook(ref hook) => {
                match *hook {
                    GitHookEvent::PostReceive {
                        ref project,
                        ref updates,
                    } => {
                        let mut data = self.data.write().map_err(ServiceError::data_lock)?;

                        updates
                            .iter()
                            .map(|update| data.handle_push(self.admin.id, project, update))
                            .collect::<Result<Vec<_>, data::HookError>>()?;
                    },
                }
            },
            ServiceEvent::Quit => (),
        }

        Ok(!end_signal.matches(event))
    }

    pub fn run(
        &mut self,
        end_signal: EndSignal,
        time_limit: Duration,
    ) -> Result<ServiceExit, ServiceError> {
        let res = {
            let reader = match self.reader.take() {
                Some(reader) => reader.map_err(ServiceError::read_channel),
                None => return Err(ServiceError::duplicate_run()),
            };
            let socket = UnixListener::bind(&self.hook_socket)
                .map_err(|err| ServiceError::hook_socket(SocketStep::Bind, err))?
                .incoming()
                .map_err(|err| ServiceError::hook_socket(SocketStep::Accept, err))
                .and_then(|reader| {
                    let buf_reader = BufReader::new(reader);
                    tokio::io::read_until(buf_reader, 0, Vec::new())
                        .map_err(|err| ServiceError::hook_socket(SocketStep::Read, err))
                        .and_then(|(_, mut buffer)| {
                            // Remove the trailing NUL.
                            buffer.pop();
                            serde_json::from_slice(&buffer)
                                .map_err(ServiceError::parse_hook)
                                .and_then(|json| {
                                    serde_json::from_value(json)
                                        .map_err(ServiceError::deserialize_hook)
                                })
                                .map(ServiceEvent::GitHook)
                                .into_future()
                        })
                });

            let runner = Timeout::new(reader.select(socket), time_limit)
                // XXX: https://github.com/tokio-rs/tokio/pull/573
                // .timeout(time_limit)
                .map_err(Into::into)
                .take_while(|event| self.handle_event(event, &end_signal))
                .for_each(|_| Ok(()));

            let mut rt = Runtime::new().map_err(ServiceError::tokio)?;
            rt.block_on(runner)
        };

        self.data.read().map_err(ServiceError::data_lock)?.exit()?;

        match res {
            Ok(_) => Ok(ServiceExit::Complete),
            Err(err) => {
                if let ServiceError::TimedOut {} = err {
                    Ok(ServiceExit::TimedOut)
                } else {
                    Err(err)
                }
            },
        }
    }

    pub fn launch(&mut self, config: Value, end: EndSignal) -> Result<(), ServiceError> {
        self.launch_impl(config, end, ServiceExit::Complete)
    }

    pub fn launch_timeout(&mut self, config: Value, end: EndSignal) -> Result<(), ServiceError> {
        self.launch_impl(config, end, ServiceExit::TimedOut)
    }

    fn launch_impl(
        &mut self,
        config: Value,
        end: EndSignal,
        expect: ServiceExit,
    ) -> Result<(), ServiceError> {
        let director_thread = self.director_thread(config);

        let res = self.run(end, *SERVICE_TIMEOUT)?;
        let director_res = director_thread
            .join()
            .map_err(ServiceError::join_director)??;
        if res != expect {
            return Err(ServiceError::exit_status(res));
        }
        if director_res != RunResult::Done {
            return Err(ServiceError::director_failure(director_res));
        }

        Ok(())
    }

    fn director_thread(&self, config: Value) -> JoinHandle<Result<RunResult, ServiceError>> {
        let webhook_dir = self.root().join("queue");
        let director_dir = self.root().join("director");
        let mut hosting_service = TestHost::new(Arc::clone(&self.data), self.writer.clone());
        hosting_service.with_pipelines(self.with_pipelines);

        thread::spawn(move || {
            let host_json = json!({
                "host_api": "test-service",
                "maintainers": ["ghostflow"],
                "secrets_path": "/dev/null",
                "projects": config,
            });
            let host_config = serde_json::from_value(host_json)
                .map_err(ServiceError::deserialize_configuration)?;
            let host = config::Host::new(host_config, Arc::new(hosting_service), director_dir)
                .map_err(ServiceError::construct_host)?;
            env::set_var("HOME", concat!(env!("CARGO_MANIFEST_DIR"), "/test/home"));
            env::set_var(
                "XDG_CONFIG_HOME",
                concat!(env!("CARGO_MANIFEST_DIR"), "/test/home/config"),
            );

            let watchdog = DirectorWatchdog;
            let handler = TestHandler::new(host);

            let mut director = Director::new(&webhook_dir)?;
            watchdog
                .add_to_director(&mut director)
                .map_err(ServiceError::add_watchdog)?;
            handler
                .add_to_director(&mut director)
                .map_err(ServiceError::add_handler)?;

            Ok(director.watch_directory(&webhook_dir)?)
        })
    }

    fn send(&self, event: ServiceEvent) -> Result<(), ServiceError> {
        let sender = self.writer.clone();
        sender
            .send(event)
            .wait()
            .map_err(ServiceError::write_event)
            .map(|_| ())
    }
}

mod tests {
    use std::thread;
    use std::time::Duration;

    use crates::git_workarea::{CommitId, GitContext};

    use handlers::test::action::Action;
    use handlers::test::service::{
        EndSignal, ServiceEvent, ServiceExit, TestService, SERVICE_TIMEOUT,
    };

    #[test]
    fn test_service_complete() {
        let mut service = TestService::new_impl("test_service_timeout").unwrap();
        service.send(ServiceEvent::Quit).unwrap();
        let res = service
            .run(EndSignal::Ignore, Duration::from_secs(1))
            .unwrap();
        assert_eq!(res, ServiceExit::Complete);
    }

    #[test]
    fn test_service_timeout() {
        let mut service = TestService::new_impl("test_service_timeout").unwrap();
        let res = service
            .run(EndSignal::Ignore, Duration::from_secs(1))
            .unwrap();
        assert_eq!(res, ServiceExit::TimedOut);
    }

    #[test]
    fn test_service_complete_push() {
        let mut service = TestService::new(
            "test_service_complete_push",
            vec![Action::new_project("ghostflow", "example")],
        )
        .unwrap();
        let project = "ghostflow/example";
        let refname = "refs/heads/test_ref";
        let commit = "f2a1015e0ddb02c7483035aa2c72ff858d6159ee";

        let end = EndSignal::Push {
            project: project.into(),
            ref_: refname.into(),
            sha1: CommitId::new(commit),
        };

        let project_dir = service.root().join(format!("projects/{}.git", project));
        let push_thread = thread::spawn(move || {
            let ctx = GitContext::new(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"));

            thread::sleep(*SERVICE_TIMEOUT);

            let push = ctx
                .git()
                .arg("push")
                .arg(project_dir)
                .arg(format!("{}:{}", commit, refname))
                .output()
                .expect("failed to create push command");
            if !push.status.success() {
                panic!(
                    "failed to push {} as {} in {}: {}",
                    commit,
                    refname,
                    project,
                    String::from_utf8_lossy(&push.stderr),
                );
            }
        });
        let res = service.run(end, 2 * (*SERVICE_TIMEOUT)).unwrap();
        assert_eq!(res, ServiceExit::Complete);
        push_thread.join().unwrap();
    }

    #[test]
    fn test_service_complete_director() {
        let mut service = TestService::new("test_service_complete_director", Vec::new()).unwrap();
        service.send(ServiceEvent::Quit).unwrap();
        let config = json!({});
        service.launch(config, EndSignal::Ignore).unwrap();
    }
}
