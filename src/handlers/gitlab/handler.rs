// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::cmp::Ordering;
use std::error::Error;
use std::sync::{Arc, RwLock};

use crates::boxfnonce::BoxFnOnce;
use crates::either::Either;
use crates::ghostflow::host::HostingServiceError;
use crates::ghostflow_gitlab::gitlab::systemhooks::{
    GroupMemberSystemHook, ProjectMemberSystemHook, ProjectSystemHook,
};
use crates::ghostflow_gitlab::gitlab::types::NoteType;
use crates::ghostflow_gitlab::gitlab::webhooks::{MergeRequestHook, NoteHook, PushHook};
use crates::ghostflow_gitlab::gitlab::{Gitlab, GitlabError};
use crates::ghostflow_gitlab::GitlabService;
use crates::json_job_dispatch::{Director, Handler, HandlerResult};
use crates::serde::de::DeserializeOwned;
use crates::serde::Deserialize;
use crates::serde_json::{self, Value};
use crates::thiserror::Error;

use actions::merge_requests::Info;
use config::defaults;
use config::Host;
use handlers::common::handlers::*;
use handlers::common::jobs::{
    BatchBranchJob, ClearTestRefs, ResetFailedProjects, TagStage, UpdateFollowRefs,
};
use handlers::gitlab::support;
use handlers::gitlab::traits::*;
use handlers::HostHandler;

#[derive(Debug, Error)]
pub enum GitlabHostError {
    #[error("failed to deserialize GitLab secrets for host {}: {}", host, source)]
    DeserializeSecrets {
        host: String,
        #[source]
        source: serde_json::Error,
    },
    #[error("failed to construct client for host {}: {}", host, source)]
    ConstructClient {
        host: String,
        #[source]
        source: GitlabError,
    },
    #[error("failed to construct service for host {}: {}", host, source)]
    ConstructService {
        host: String,
        #[source]
        source: HostingServiceError,
    },
}

impl GitlabHostError {
    fn deserialize_secrets(host: String, source: serde_json::Error) -> Self {
        GitlabHostError::DeserializeSecrets {
            host,
            source,
        }
    }

    fn construct_client(host: String, source: GitlabError) -> Self {
        GitlabHostError::ConstructClient {
            host,
            source,
        }
    }

    fn construct_service(host: String, source: HostingServiceError) -> Self {
        GitlabHostError::ConstructService {
            host,
            source,
        }
    }
}

#[derive(Debug, Deserialize)]
struct GitlabSecrets {
    token: String,
    #[serde(default = "defaults::default_true")]
    use_ssl: bool,
}

/// Connect to a Gitlab from its configuration block.
pub fn host_handler(
    url: &Option<String>,
    secrets: &Value,
    name: String,
) -> Result<HostHandler, GitlabHostError> {
    let host = url.as_ref().map_or("gitlab.com", |u| u.as_str());
    let secrets: GitlabSecrets = serde_json::from_value(secrets.clone())
        .map_err(|err| GitlabHostError::deserialize_secrets(host.into(), err))?;

    let ctor = if secrets.use_ssl {
        Gitlab::new
    } else {
        Gitlab::new_insecure
    };

    let gitlab = ctor(host, secrets.token)
        .map_err(|err| GitlabHostError::construct_client(host.into(), err))?;
    let service = GitlabService::new(gitlab.clone())
        .map(Arc::new)
        .map_err(|err| GitlabHostError::construct_service(host.into(), err))?;

    Ok(HostHandler {
        service,
        handler: BoxFnOnce::new(|host| {
            Box::new(GitlabHandler::new(gitlab, host, name)) as Box<dyn Handler>
        }),
    })
}

/// The handler for Gitlab events.
struct GitlabHandler {
    /// Handle to a private client.
    gitlab: Gitlab,
    /// The host block for this handler.
    host: RwLock<Host>,
    /// The name to use for this handler.
    name: String,
}

const HOST_LOCK_POISONED: &str = "host lock poisoned";

impl GitlabHandler {
    /// Create a new handler.
    fn new(gitlab: Gitlab, host: Host, name: String) -> Self {
        GitlabHandler {
            gitlab,
            host: RwLock::new(host),
            name,
        }
    }

    /// Verify that the kind is valid.
    fn verify_kind<'a>(&self, kind: &'a str) -> Either<&'a str, HandlerResult> {
        let mut split = kind.split(':');

        if let Some(level) = split.next() {
            if level != self.name {
                return Either::Right(HandlerResult::reject(format!(
                    "handler mismatch: {}",
                    level,
                )));
            }
        } else {
            return Either::Right(HandlerResult::reject("handler mismatch"));
        }

        if let Some(kind) = split.next() {
            Either::Left(kind)
        } else {
            Either::Right(HandlerResult::reject("missing kind"))
        }
    }

    /// Parse an object into a type.
    fn parse_object<F, T>(object: &Value, callback: F) -> HandlerResult
    where
        T: DeserializeOwned,
        F: Fn(T) -> HandlerResult,
    {
        match serde_json::from_value::<T>(object.clone()) {
            Ok(hook) => callback(hook),
            Err(err) => HandlerResult::fail(err),
        }
    }

    /// Handle a job.
    fn handle_kind(&self, kind: &str, object: &Value, can_defer: bool) -> HandlerResult {
        match kind {
            "merge_request" => {
                Self::parse_object(object, |hook: MergeRequestHook| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    let info = GitlabMergeRequestInfo::from_web_hook(
                        host.service.as_ref(),
                        &hook.object_attributes,
                    );

                    // XXX(gitlab): Only needed until something like this feature has landed:
                    // https://gitlab.com/gitlab-org/gitlab/merge_requests/18213
                    if let Ok(mr) = info.as_ref() {
                        self.unprotect_source_branch(&host, mr);
                    }

                    info.map(|mr| handle_merge_request_update(object, &host, &mr))
                        .unwrap_or_else(HandlerResult::fail)
                })
            },
            "note" => {
                Self::parse_object(object, |hook: NoteHook| {
                    let note_type = hook.object_attributes.noteable_type;
                    if let NoteType::MergeRequest = note_type {
                        let host = self.host.read().expect(HOST_LOCK_POISONED);
                        GitlabMergeRequestNoteInfo::from_web_hook(host.service.as_ref(), &hook)
                            .map(|note| handle_merge_request_note(object, &host, &note, can_defer))
                            .unwrap_or_else(HandlerResult::fail)
                    } else {
                        HandlerResult::reject(format!("unhandled noteable type: {:?}", note_type))
                    }
                })
            },
            "push" => {
                Self::parse_object(object, |hook: PushHook| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    GitlabPushInfo::from_web_hook(host.service.as_ref(), hook)
                        .map(|push| handle_push(object, &host, &push))
                        .unwrap_or_else(HandlerResult::fail)
                })
            },
            "project_create" => {
                Self::parse_object(object, |hook: ProjectSystemHook| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    host.service
                        .repo(&hook.path_with_namespace)
                        .map(|project| handle_project_creation(object, &host, &project))
                        .unwrap_or_else(HandlerResult::fail)
                })
            },
            "user_add_to_team" | "user_remove_from_team" => {
                Self::parse_object(object, |hook: ProjectMemberSystemHook| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    handle_project_membership_refresh(
                        object,
                        &host,
                        &hook.project_path_with_namespace,
                    )
                })
            },
            "user_add_to_group" | "user_remove_from_group" => {
                Self::parse_object(object, |hook: GroupMemberSystemHook| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    handle_group_membership_refresh(object, &host, &hook.group_name)
                })
            },
            "clear_test_refs" => {
                Self::parse_object(object, |data: BatchBranchJob<ClearTestRefs>| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    handle_clear_test_refs(object, &host, data)
                })
            },
            "tag_stage" => {
                Self::parse_object(object, |data: BatchBranchJob<TagStage>| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    handle_stage_tag(object, &host, data)
                })
            },
            "update_follow_refs" => {
                Self::parse_object(object, |data: BatchBranchJob<UpdateFollowRefs>| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    handle_update_follow_refs(object, &host, data)
                })
            },
            "ensure_project_state" => {
                Self::parse_object(object, |ensure: support::EnsureProjectState| {
                    let host = self.host.read().expect(HOST_LOCK_POISONED);
                    ensure.check_all_projects(&self.gitlab, &host)
                })
            },
            "reset_failed_projects" => {
                Self::parse_object(object, |_: ResetFailedProjects| {
                    self.host
                        .write()
                        .expect(HOST_LOCK_POISONED)
                        .reset_failed_projects();
                    HandlerResult::Accept
                })
            },
            _ => HandlerResult::reject(format!("unhandled kind: {}", kind)),
        }
    }

    fn unprotect_source_branch(&self, host: &Host, mr: &Info) {
        let project =
            if let Ok(project) = utils::get_project(host, &mr.merge_request.target_repo.name) {
                project
            } else {
                return;
            };

        if !project.unprotect_source_branches() {
            return;
        }

        support::unprotect_source_branch(&self.gitlab, host, mr)
    }
}

impl Handler for GitlabHandler {
    fn add_to_director<'a>(
        &'a self,
        director: &mut Director<'a>,
    ) -> Result<(), Box<dyn Error + Send + Sync>> {
        let mut add_handler = |kind| director.add_handler(&format!("{}:{}", self.name, kind), self);

        add_handler("merge_request")?;
        add_handler("note")?;
        add_handler("push")?;
        add_handler("project_create")?;
        add_handler("user_add_to_team")?;
        add_handler("user_remove_from_team")?;
        add_handler("user_add_to_group")?;
        add_handler("user_remove_from_group")?;

        add_handler("clear_test_refs")?;
        add_handler("tag_stage")?;
        add_handler("update_follow_refs")?;
        add_handler("ensure_project_state")?;

        add_handler("reset_failed_projects")?;

        Ok(())
    }

    fn handle(
        &self,
        kind: &str,
        object: &Value,
    ) -> Result<HandlerResult, Box<dyn Error + Send + Sync>> {
        let kind = match self.verify_kind(kind) {
            Either::Left(kind) => kind,
            Either::Right(res) => return Ok(res),
        };

        Ok(self.handle_kind(kind, object, true))
    }

    fn handle_retry(
        &self,
        kind: &str,
        object: &Value,
        reasons: Vec<String>,
    ) -> Result<HandlerResult, Box<dyn Error + Send + Sync>> {
        let kind = match self.verify_kind(kind) {
            Either::Left(kind) => kind,
            Either::Right(res) => return Ok(res),
        };

        match reasons.len().cmp(&self.retry_limit(kind)) {
            Ordering::Greater => {
                Ok(HandlerResult::reject(format!(
                    "retry limit ({}) reached for {}",
                    reasons.len(),
                    kind,
                )))
            },
            Ordering::Equal => Ok(self.handle_kind(kind, object, false)),
            Ordering::Less => Ok(self.handle_kind(kind, object, true)),
        }
    }
}
