// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::num::ParseIntError;
use std::sync::Arc;

use crates::chrono::{DateTime, NaiveDate, Utc};
use crates::ghostflow::host::{self, HostingService, HostingServiceError};
use crates::ghostflow_gitlab::gitlab;
use crates::ghostflow_gitlab::gitlab::api;
use crates::ghostflow_gitlab::GitlabService;
use crates::itertools::Itertools;
use crates::serde::de::DeserializeOwned;
use crates::thiserror::Error;

use ghostflow_ext::{AccessLevel, DirectorHostingService, HostingServiceExt, Membership};
use handlers::gitlab::types;

#[derive(Debug, Error)]
pub enum GitlabServiceError {
    #[error("failed to parse commetn ID: {}", source)]
    CommentIdParse {
        #[source]
        source: ParseIntError,
    },
    #[error("failed to find a user named '{}'", name)]
    NoSuchUser { name: String },
}

impl GitlabServiceError {
    fn comment_id_parse(source: ParseIntError) -> Self {
        Self::CommentIdParse {
            source,
        }
    }

    fn no_such_user(name: String) -> Self {
        GitlabServiceError::NoSuchUser {
            name,
        }
    }
}

impl From<GitlabServiceError> for HostingServiceError {
    fn from(gitlab: GitlabServiceError) -> Self {
        HostingServiceError::service(gitlab)
    }
}

fn ghostflow_access_level(level: u64) -> AccessLevel {
    if level >= 50 {
        AccessLevel::Owner
    } else if level >= 40 {
        AccessLevel::Maintainer
    } else if level >= 30 {
        AccessLevel::Developer
    } else {
        AccessLevel::Contributor
    }
}

fn api_access_level(level: AccessLevel) -> api::common::AccessLevel {
    match level {
        AccessLevel::Owner => api::common::AccessLevel::Owner,
        AccessLevel::Maintainer => api::common::AccessLevel::Maintainer,
        AccessLevel::Developer => api::common::AccessLevel::Developer,
        AccessLevel::Contributor => api::common::AccessLevel::Guest,
    }
}

fn naivedate_to_datetime(naive_date: NaiveDate) -> DateTime<Utc> {
    DateTime::from_utc(naive_date.and_hms(0, 0, 0), Utc)
}

fn query<Q, T>(gitlab: &gitlab::Gitlab, query: &Q) -> Result<T, HostingServiceError>
where
    Q: api::Query<T, gitlab::Gitlab>,
    T: DeserializeOwned,
{
    query.query(gitlab).map_err(HostingServiceError::host)
}

impl HostingServiceExt for GitlabService {
    fn as_director_service(self: Arc<Self>) -> Option<Arc<dyn DirectorHostingService>> {
        Some(self as Arc<dyn DirectorHostingService>)
    }
}

impl DirectorHostingService for GitlabService {
    fn as_hosting_service(self: Arc<Self>) -> Arc<dyn HostingService> {
        self as Arc<dyn HostingService>
    }

    fn members(&self, project: &str) -> Result<Vec<Membership>, HostingServiceError> {
        let endpoint = api::projects::Project::builder()
            .project(project)
            .build()
            .unwrap();
        let gitlab_project: types::Project = query(self.gitlab(), &endpoint)?;

        let endpoint = api::projects::members::ProjectMembers::builder()
            .project(gitlab_project.id)
            .build()
            .unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);
        let project_members: Vec<types::Member> = query(self.gitlab(), &endpoint)?;
        let group_members: Vec<types::Member> =
            if let types::NamespaceKind::Group = gitlab_project.namespace.kind {
                let mut cur_group_id = Some(gitlab_project.namespace.id);
                let mut group_members = Vec::new();
                while let Some(group_id) = cur_group_id {
                    let endpoint = api::groups::members::GroupMembers::builder()
                        .group(group_id)
                        .build()
                        .unwrap();
                    let endpoint = api::paged(endpoint, api::Pagination::All);
                    let cur_group_members: Vec<types::Member> = query(self.gitlab(), &endpoint)?;

                    // Accumulate and iterate.
                    group_members.extend(cur_group_members.into_iter());
                    let endpoint = api::groups::Group::builder()
                        .group(group_id)
                        .build()
                        .unwrap();
                    let parent_group: types::Group = query(self.gitlab(), &endpoint)?;
                    cur_group_id = parent_group.parent_id;
                }
                group_members
            } else {
                Vec::new()
            };

        project_members
            .into_iter()
            // Chain with the group membership list.
            .chain(group_members.into_iter())
            // Sort by the user IDs.
            .sorted_by_key(|a| a.id)
            // Group all members in the list by the ID.
            .group_by(|member| member.id)
            .into_iter()
            // Create Membership structs.
            .map(|(_, members)| {
                // Find the maximum access level from any membership.
                let max_access = members
                    .max_by_key(|member| member.access_level)
                    .expect("expected there to be at least one member in each group");

                Ok(Membership {
                    user: self.user(project, &max_access.username)?,
                    access_level: ghostflow_access_level(max_access.access_level),
                    expiration: max_access.expires_at.map(naivedate_to_datetime),
                })
            })
            .collect()
    }

    fn add_member(
        &self,
        project: &str,
        user: &host::User,
        level: AccessLevel,
    ) -> Result<(), HostingServiceError> {
        let endpoint = api::users::Users::builder()
            .username(&user.handle)
            .build()
            .unwrap();
        // We're assuming the search by username shows up on the first page.
        let users: Vec<types::User> = query(self.gitlab(), &endpoint)?;

        let user = users
            .into_iter()
            .find(|gitlab_user: &types::User| gitlab_user.username == user.handle)
            .ok_or_else(|| GitlabServiceError::no_such_user(user.handle.clone()))?;

        let access_level = api_access_level(level);
        let endpoint = api::projects::members::AddProjectMember::builder()
            .project(project)
            .user(user.id)
            .access_level(access_level)
            .build()
            .unwrap();
        let endpoint = api::ignore(endpoint);
        query(self.gitlab(), &endpoint)
    }

    fn add_hook(&self, project: &str, url: &str) -> Result<(), HostingServiceError> {
        // XXX: Keep in sync with `src/handlers/gitlab/support.rs@check_all_projects`.
        let endpoint = api::projects::hooks::CreateHook::builder()
            .project(project)
            .url(url)
            .issues_events(true)
            .job_events(true)
            .merge_requests_events(true)
            .note_events(true)
            .pipeline_events(true)
            .push_events(true)
            .build()
            .unwrap();
        let endpoint = api::ignore(endpoint);
        query(self.gitlab(), &endpoint)
    }

    fn post_commit_comment(
        &self,
        commit: &host::Commit,
        content: &str,
    ) -> Result<(), HostingServiceError> {
        let endpoint = api::projects::repository::commits::CommentOnCommit::builder()
            .project(commit.repo.name.as_str())
            .commit(commit.id.as_str())
            .note(content)
            .build()
            .unwrap();
        let endpoint = api::ignore(endpoint);
        query(self.gitlab(), &endpoint)
    }

    fn get_mr_comment_awards(
        &self,
        mr: &host::MergeRequest,
        comment: &host::Comment,
    ) -> Result<Vec<host::Award>, HostingServiceError> {
        let note_id = comment
            .id
            .parse()
            .map_err(GitlabServiceError::comment_id_parse)?;
        let endpoint =
            api::projects::merge_requests::notes::awards::MergeRequestNoteAwards::builder()
                .project(mr.target_repo.name.as_ref())
                .merge_request(mr.id)
                .note(note_id)
                .build()
                .unwrap();
        let endpoint = api::paged(endpoint, api::Pagination::All);

        query(self.gitlab(), &endpoint)?
            .into_iter()
            .map(|award: types::AwardEmoji| {
                let author = self.user(&mr.target_repo.name, &award.user.username)?;

                Ok(host::Award {
                    name: award.name,
                    author,
                })
            })
            .collect()
    }

    fn award_mr_comment(
        &self,
        mr: &host::MergeRequest,
        comment: &host::Comment,
        award: &str,
    ) -> Result<(), HostingServiceError> {
        let note_id = comment
            .id
            .parse()
            .map_err(GitlabServiceError::comment_id_parse)?;
        let endpoint =
            api::projects::merge_requests::notes::awards::CreateMergeRequestNoteAward::builder()
                .project(mr.target_repo.name.as_ref())
                .merge_request(mr.id)
                .note(note_id)
                .name(award)
                .build()
                .unwrap();
        let endpoint = api::ignore(endpoint);
        query(self.gitlab(), &endpoint)
    }

    fn comment_award_name(&self) -> &str {
        "robot"
    }
}
