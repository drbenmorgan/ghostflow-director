// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::sync::Arc;

use crates::chrono::{DateTime, Utc};
use crates::ghostflow::host::{
    Award, Comment, Commit, HostingService, HostingServiceError, MergeRequest, User,
};

/// Access level to a project.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum AccessLevel {
    /// A user external to the project.
    Contributor,
    /// A user with permission to develop on the project directly.
    Developer,
    /// A user with permission to maintain integration branches on the project.
    Maintainer,
    /// A user with permission to administrate the project.
    Owner,
}

/// A membership of a user to a project, team, or group.
#[derive(Debug, Clone)]
pub struct Membership {
    /// The user with access.
    pub user: User,

    /// The access level of the user.
    pub access_level: AccessLevel,

    /// When the membership expires (if at all).
    pub expiration: Option<DateTime<Utc>>,
}

pub trait HostingServiceExt: HostingService {
    fn as_director_service(self: Arc<Self>) -> Option<Arc<dyn DirectorHostingService>> {
        None
    }
}

pub trait DirectorHostingService: HostingService {
    /// Downcast into a plain `HostingService`.
    fn as_hosting_service(self: Arc<Self>) -> Arc<dyn HostingService>;

    /// Return the list of memberships to a project.
    ///
    /// This should include *all* memberships which provide a permission to the project above that
    /// of an anonymous user.
    fn members(&self, project: &str) -> Result<Vec<Membership>, HostingServiceError>;

    /// Add a member to a project.
    fn add_member(
        &self,
        project: &str,
        user: &User,
        access_level: AccessLevel,
    ) -> Result<(), HostingServiceError>;

    /// Register a webhook for a project.
    ///
    /// The webhook should listen for all events that matter for the workflow (e.g., pushes,
    /// updates to merge requests, etc.).
    fn add_hook(&self, project: &str, url: &str) -> Result<(), HostingServiceError>;

    /// Add a comment to a commit.
    fn post_commit_comment(
        &self,
        commit: &Commit,
        content: &str,
    ) -> Result<(), HostingServiceError>;

    /// Get awards on a merge request comment.
    fn get_mr_comment_awards(
        &self,
        mr: &MergeRequest,
        comment: &Comment,
    ) -> Result<Vec<Award>, HostingServiceError>;
    /// Award an emoji to a merge request comment.
    fn award_mr_comment(
        &self,
        mr: &MergeRequest,
        comment: &Comment,
        award: &str,
    ) -> Result<(), HostingServiceError>;
    /// The award to give comments which were acted upon.
    fn comment_award_name(&self) -> &str;
}
